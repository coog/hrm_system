var pb_data=echarts.init(document.getElementById("pb_data"));
var data = [
    {"value": 26, "name": "论 坛"}, {"value": 16.7, "name": "博 览 会"}, {
        "value": 12.4,
        "name": "研 讨 会"
    }, {"value": 12.2, "name": "峰 会"}, {"value": 8.4, "name": "座 谈 会"}, {
        "value": 7.6,
        "name": "展 览"
    }
];
data_name = [];
for (var n  in data){
    data[n]['name'] = data[n]['name'] + ' '+data[n]['value'] +'%';
    data_name.push(data[n]['name'])
}

option = {
    backgroundColor: "#fff",
	
    title: {
        text: '员工基本数据统计',
        left: '50%',
        link:'http://www.baidu.com',
        target:'_blank',
        textAlign: 'center',
        textStyle: {
            color: "#000",
            fontWeight:'normal',
            fontFamily:'Microsoft YaHei',
            fontSize:'22'
        }
    },
    tooltip: {
        trigger: 'item',
        formatter: "{b}"
    },
    legend: {
        orient: 'horizontal',
//      top: 'center',
		left:'center',
        bottom: 30,
        data:data_name,
        textStyle: {
            color: "#666666",
            fontWeight:'normal',
            fontFamily:'Microsoft YaHei'
        }
    },
    series: [
        {
            name:'',
            type:'pie',
            radius: ['40%', '55%'],
            data:data,
            stillShowZeroSum:true,
            textStyle:{
            fontSize:'20'
            }
        }
    ]
};
pb_data.setOption(option);


window.onresize = function(){
	this.chart1 = echarts.getInstanceByDom(document.getElementById('pb_data'));
          this.chart1.resize();
          console.log(this.chart1);
          
}

