$(function() {
	$('#formverification').bootstrapValidator({

		excluded:[":disabled"],　　　　　　　　
		message: 'This value is not valid',
		feedbackIcons: {　　　　　　　　
			valid: 'glyphicon glyphicon-ok',
			　　　　　　　　invalid: 'glyphicon glyphicon-remove',
			　　　　　　　　validating: 'glyphicon glyphicon-refresh'　　　　　　　　
		},
		fields: {
			hrName: {
				message: '用户名验证失败',
				validators: {
					notEmpty: {
						message: '名字不能为空'
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '名字长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '姓名只能为中文'
					}
				},
			},
			hrNativePlace: {
				validators: {
					notEmpty: {
						message: '请输入籍贯 '
					},
					stringLength: {
						min: 2,
						max: 5,
						message: '2~5个汉字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrWeight: {
				validators: {
					notEmpty: {
						message: '请输入体重,单位KG '
					},
					stringLength: {
						min: 2,
						max: 5,
						message: ' '
					},
					regexp: {
						regexp: /^[0-9]*$/,
						message: '请输入数字'
					}
				}
			},
			hrTel: {
				validators: {
					notEmpty: {
						message: '联系电话不能为空'
					},
					//                      stringLength: {
					//                          min: 8,
					//                          max: 11,
					//                          message: '请输入正确的号码'
					//                      },
					regexp: {
						regexp: /^1[3|5|7|8][0-9]\d{4,8}$/,
						message: '请输入正确的号码'
					}
				}
			},
			hrSalary: {
				validators: {
					notEmpty: {
						message: '此项不能为空'
					},
					stringLength: {
						min: 0,
						max: 5,
						message: ' '
					},
					regexp: {
						regexp: /^[0-9]*$/,
						message: '请输入正确的号码'
					}
				}
			},
			hrMultiracial: {
				validators: {
					notEmpty: {
						message: ' '
					},
					stringLength: {
						min: 0,
						max: 6,
						message: ' '
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请填写中文'
					}
				}
			},
			hrPosition: {
				validators: {
					notEmpty: {
						message: '应聘岗位不能为空'
					}
				}
			},
			hrCity: {
				validators: {
					notEmpty: {
						message: '住址不能为空'
					},
				}
			},
			hrEmail: {
				validators: {
					notEmpty: {
						message: '应聘岗位不能为空'
					},
					regexp: {
						regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
						message: '请输入正确的邮箱'
					}
				}
			},
			hrIDCard: {
				validators: {
					notEmpty: {
						message: '身份证不能为空'
					},
					regexp: {
						regexp: /(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}[0-9Xx]$)/,
						message: '请输入正确的身份证号码'
					}
				}
			},
			hrPoliticalIandscape: {
				validators: {
					notEmpty: {
						message: '政治面貌不能为空'
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrWorkArea: {
				validators: {
					notEmpty: {
						message: '地区'
					},
				}
			},
			hrRecruitment: {
				validators: {
					notEmpty: {
						message: '请选择渠道'
					},
				}
			},
			hrBirthdate: {
				trigger:"change", 
				validators: {
					notEmpty: {
						message: '生日不能为空'
					},
				}
			},
			hrEntryDate: {
				trigger:"change", 
				validators: {
					notEmpty: {
						message: '日期不能为空'
					},
				}
			},
			hrHeight: {
				validators: {
					notEmpty: {
						message: '此项不能为空'
					},
				}
			},

			hrContactName: {
				validators: {
					notEmpty: {
						message: '紧急联系人姓名不能为空'
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrContactRelationship: {
				validators: {
					notEmpty: {
						message: '关系不能为空'
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrContactTel: {
				validators: {
					notEmpty: {
						message: '紧急联系电话不能为空'
					},
					regexp: {
						regexp: /^1[3|5|7|8][0-9]\d{4,8}$/,
						message: '请输入正确的号码'
					}
				}
			},
			hrFamilyMember: {
				validators: {
					notEmpty: {
						message: '姓名不能为空'
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrFamilyRelationship: {
				validators: {
					notEmpty: {
						message: '关系不能为空'
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrFamilyMember: {
				validators: {
					//              		notEmpty:{
					//              			message: '姓名不能为空'
					//              		},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrFamilyMember: {
				validators: {
					notEmpty: {
						message: '请填写职业'
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '请输入中文'
					}
				}
			},
			hrFamilyTel: {
				validators: {
					notEmpty: {
						message: '紧急联系电话不能为空'
					},
					regexp: {
						regexp: /^1[3|5|7|8][0-9]\d{4,8}$/,
						message: '请输入正确的号码'
					}
				}
			},
			hrEduDate: {
				validators: {
					notEmpty: {
						message: ' '
					},
					regexp: {
						regexp: /^[1-2]\d{3}\.(0[1-9]|1[0-2])\.(0[1-9]|[1-2][0-9]|3[0-1])-[1-2]\d{3}\.(0[1-9]|1[0-2])\.(0[1-9]|[1-2][0-9]|3[0-1])$/,
						message: '日期格式不正确，正确格式为：2015.09.01-2018.07.01'
					}
				}
			},
			hrEduSchool: {
				validators: {
					notEmpty: {
						message: '请输入学校名字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '',
					}
				}
			},
			hrEduProfession: {
				validators: {
					notEmpty: {
						message: '请输入专业名字'
					},
					stringLength: {
						min: 2,
						max: 10,
						message: ' '
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: '',
					}
				}
			},
			hrEduEducation: {
				validators: {
					notEmpty: {
						message: ' '
					},
					stringLength: {
						min: 2,
						max: 4,
						message: '长度为2~4个字'
					},
					regexp: {
						regexp: /^[\u0391-\uFFE5]+$/,
						message: ' ',
					}
				}
			},
		}
	});

});

/**
						 *
						 方法说明
						 *
						 @method add_input() 
						 *
						 @param {string} name1  添加的input的name属性
						 @param {string} idname  添加的input的父级ID
						 @param {string} cname  添加input按钮的class名
						 @param {number} dil  添加的input的个数
						 @param {string} secname  包着input父级盒子的盒子class名（input-->父级ID-->父级class）
						 @param {string} pjn  secname的父盒子class名
						 *
						 @return {Object}  返回一个对象，该对象包含所添加的input个数，input的外层盒子，与外层盒子平级的div，div的文本为part+（N+1） 							  N为添加input组的次数
						 */
function add_input(idname, cname, dil, secname, pjn) {
	var seclen = document.getElementsByClassName(secname).length;
	var pj = document.getElementsByClassName(pjn)[0];
	var ib = document.getElementById(idname);
	var pn = document.getElementsByClassName("p1")[0];
	var btj = document.getElementsByClassName(cname)[0];
	var ibl = document.getElementById(idname).getElementsByTagName("input");
	var il = ib.childNodes.length;
	var arr1 = [];
	var p_arr = [];
	var n_arr = [];
	for(var i = 0; i < dil; i++) {
		var input_file = document.createElement("input");
		input_file.setAttribute("type", "text");
		input_file.setAttribute("class", 'form-control mb3');
		var name1 = ibl[i].getAttribute("name" + (idname + seclen));
		n_arr.push(ibl[i].getAttribute("name"));
		p_arr.push(ibl[i].getAttribute("placeholder"));
		arr1.push(input_file);
	}

	var ibc = document.createElement("div");
	var sec = document.createElement("div");
	var pnc = document.createElement("div");
	pnc.innerText = 'Part' + (seclen + 1);
	pnc.setAttribute("class", "col-sm-1 p1 pl0");
	ibc.setAttribute("id", (idname + seclen));
	ibc.setAttribute("class", "col-sm-11 p0");
	sec.setAttribute("class", (secname) + " " + "col-sm-11 p0");
	sec.append(pnc);
	for(var j = 0; j < arr1.length; j++) {
		ibc.append(arr1[j])
		arr1[j].setAttribute("placeholder", p_arr[j]);
		arr1[j].setAttribute("name", (n_arr[j] + seclen));
	}
	sec.append(ibc);
	pj.append(sec);
	if(seclen == 2) {
		// btj.setAttribute("disabled","disabled")
		btj.style.display = "none";
	}
}

/**
 *
 方法说明
 *
 @method showText()
 *
 @param {string} idname  触发该点击事件的ID名
 @param {string} changebox  需要显示input所在位置的盒子class名
 @param {string} changeval  显示内容的元素class名

 *
 @return {string}   需要显示的内容
 */
function showText(idname, changebox, changeval,key) {
	var onch = document.getElementById(idname);
	var chbox = document.getElementsByClassName(changebox)[0];
	var chval = document.getElementsByClassName(changeval)[0];
	var valin = chval.getElementsByTagName("input")[0];
	if(onch.options[onch.selectedIndex].className == "newEnt") {
		chbox.style.display = "inline-block";
		chval.innerHTML = "<input name='" + key + onch.options[onch.selectedIndex].index + "'  placeholder='" + onch.options[onch.selectedIndex].value + "' type='text' class='form-control'/>";
		//									valin.name=onch.options[onch.selectedIndex].getAttribute("name")+1;
		//									valin.placeholder=onch.options[onch.selectedIndex].value;

	} else {
		chbox.style.display = "none";
	}
}
			

function tnext() {
	var carts = document.getElementsByClassName('tabcart');
	var tab1 = document.getElementsByClassName('tab1');
	for(var j = 0; j < carts.length; j++) {
		if(carts[j].style.display === "block") {
			carts[j].style.display = "none";
			tab1[j].style.color = "#c8c9c9";
			if(j + 1 <= carts.length) {
				carts[j + 1].style.display = "block";
				tab1[j + 1].style.color = "#3390cd";
				return;
			}
		}

	}

}

function tprev() {
	var carts = document.getElementsByClassName('tabcart');
	var tab1 = document.getElementsByClassName('tab1');
	for(var j = 0; j < carts.length; j++) {
		if(carts[j].style.display === "block") {
			carts[j].style.display = "none";
			tab1[j].style.color = "#c8c9c9";
			if(j - 1 >= 0) {
				carts[j - 1].style.display = "block";
				tab1[j - 1].style.color = "#3390cd";
				return;
			}
		}

	}

}
