<?php
namespace Personnelsystem\Controller;

use Think\Controller;
header("content-type:text/html;charset=utf-8");

class StatisticsController extends LoginTrueController
{

    public function Charts()
    {
        $this->LoginTrue();
        $admin = M("admin");
        $rs_admin = $admin->count();
        $this->assign("rs_admin", $rs_admin);
        $admin_role = M("admin_role");
        $rs_role = $admin_role->count();
        $this->assign("rs_role", $rs_role);
        $department = M("department");
        $rs_department = $department->count();
        $this->assign("rs_department", $rs_department);
        $staff = M("staff");
        $rs_staff = $staff->count();
        $this->assign("rs_staff", $rs_staff);
        //人才库的统计
        $hrreserve=M("hrreserve");
        $rs_hrreserve=$hrreserve->count();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        $this->display();
    }
    
    public function ChartsHr()
    {
        $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $rs_hrA=$hrreserve->where("hrState=0")->count();
        $this->assign("rs_hrA",$rs_hrA);
        $rs_hrB=$hrreserve->where("hrState=1 AND hrStateSuccess=0")->count();
        $this->assign("rs_hrB",$rs_hrB);
        $rs_hrC=$hrreserve->where("hrState=1 AND hrStateSuccess=1 AND hrStateSuccessJob=0")->count();
        $this->assign("rs_hrC",$rs_hrC);
        $rs_hrD=$hrreserve->where("hrState=1 AND hrStateSuccess=1 AND hrStateSuccessJob=1")->count();
        $this->assign("rs_hrD",$rs_hrD);
        $this->display();
    }

    public function ChartsDepartmentNum()
    {
        $this->LoginTrue();
        $department = M("department");
        $staff = M("staff");
        $this->assign("staff", $staff);
        $rs_department = $department->select();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function ChartsBasic()
    {
        $this->LoginTrue();
        $aDid = $_GET["aDid"];
        $staff = M("staff");
        if ($aDid == 0) {
            $rs_staff = $staff->count();
            $rs_staff_work = $staff->where("stBlacklist=0")->count();
            $rs_staff_working = $staff->where("stBlacklist=0 AND stJobState=1")->count();
            $rs_staff_worked = $staff->where("stBlacklist=0 AND stJobState=0")->count();
            $rs_staff_black = $staff->where("stBlacklist=1")->count();
        } else {
            $rs_staff = $staff->where("stDid={$aDid}")->count();
            $rs_staff_work = $staff->where("stBlacklist=0 AND stDid={$aDid}")->count();
            $rs_staff_working = $staff->where("stBlacklist=0 AND stJobState=1 AND stDid={$aDid}")->count();
            $rs_staff_worked = $staff->where("stBlacklist=0 AND stJobState=0 AND stDid={$aDid}")->count();
            $rs_staff_black = $staff->where("stBlacklist=1 AND stDid={$aDid}")->count();
        }
        $this->assign("rs_staff", $rs_staff);
        $this->assign("rs_staff_work", $rs_staff_work);
        $this->assign("rs_staff_working", $rs_staff_working);
        $this->assign("rs_staff_worked", $rs_staff_worked);
        $this->assign("rs_staff_black", $rs_staff_black);
        $this->display();
    }

    public function ChartsWork()
    {
        $this->LoginTrue();
        $aDid = $_GET["aDid"];
        $staff = M("staff");
        if ($aDid == 0) {
            $rs_staff_working = $staff->where("stBlacklist=0 AND stJobState=1")->count();
            $rs_staff_worked = $staff->where("stBlacklist=0 AND stJobState=0")->count();
        } else {
            $rs_staff_working = $staff->where("stBlacklist=0 AND stJobState=1 AND stDid={$aDid}")->count();
            $rs_staff_worked = $staff->where("stBlacklist=0 AND stJobState=0  AND stDid={$aDid}")->count();
        }
        $this->assign("rs_staff_working", $rs_staff_working);
        $this->assign("rs_staff_worked", $rs_staff_worked);
        $this->display();
    }

    public function ChartsBlack()
    {
        $this->LoginTrue();
        $staff = M("staff");
        $aDid = $_GET["aDid"];
        if ($aDid == 0) {
            $rs_staff_work = $staff->where("stBlacklist=0")->count();
            $rs_staff_black = $staff->where("stBlacklist=1")->count();
        } else {
            $rs_staff_work = $staff->where("stBlacklist=0 AND stDid={$aDid}")->count();
            $rs_staff_black = $staff->where("stBlacklist=1 AND stDid={$aDid}")->count();
        }
        $this->assign("rs_staff_work", $rs_staff_work);
        $this->assign("rs_staff_black", $rs_staff_black);
        $this->display();
    }

    public function ChartsSex()
    {
        $this->LoginTrue();
        $staff = M("staff");
        $aDid = $_GET["aDid"];
        if ($aDid == 0) {
            $rs_staff_sexboy = $staff->where("stBlacklist=0 AND stJobState=1 AND stSex=1")->count();
            $rs_staff_sexgirl = $staff->where("stBlacklist=0 AND stJobState=1 AND stSex=2")->count();
        } else {
            $rs_staff_sexboy = $staff->where("stBlacklist=0 AND stJobState=1 AND stSex=1 AND stDid={$aDid}")->count();
            $rs_staff_sexgirl = $staff->where("stBlacklist=0 AND stJobState=1 AND stSex=2 AND stDid={$aDid}")->count();
        }
        $this->assign("rs_staff_sexboy", $rs_staff_sexboy);
        $this->assign("rs_staff_sexgirl", $rs_staff_sexgirl);
        $this->display();
    }

    public function ChartsStaffMonth()
    {
        $this->LoginTrue();
        $year = date("Y");
        $month = date("m");
        $this->assign("year", $year);
        $staff = M("staff");
        $aDid = $_GET["aDid"];
        // 计算 $year 年的入职人数
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-01%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_01 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-02%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_02 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-03%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_03 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-04%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_04 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-05%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_05 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-06%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_06 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-07%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_07 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-08%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_08 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-09%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_09 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-10%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_10 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-11%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_11 = $staff->where($map)->count();
        $map['stEntryDate'] = array(
            'like',
            "%" . $year . "-12%"
        );
        $map['stJobState'] = 1;
        $map['stBlacklist'] = 0;
        if ($aDid != 0) {
            $map['stDid'] = $aDid;
        }
        $working_12 = $staff->where($map)->count();
        
        // 离职员工$year年度的月份统计
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-01%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_01 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-02%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_02 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-03%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_03 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-04%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_04 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-05%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_05 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-06%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_06 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-07%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_07 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-08%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_08 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-09%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_09 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-10%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_10 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-11%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_11 = $staff->where($maped)->count();
        $maped['stDepartureDate'] = array(
            'like',
            "%" . $year . "-12%"
        );
        $maped['stJobState'] = 0;
        $maped['stBlacklist'] = 0;
        if ($aDid != 0) {
            $maped['stDid'] = $aDid;
        }
        $worked_12 = $staff->where($maped)->count();
        if ($month == 1) {
            switch (1) {
                case 1:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
            }
        } elseif ($month == 2) {
            switch (2) {
                case 2:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
            }
        } elseif ($month == 3) {
            switch (3) {
                case 3:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
            }
        } elseif ($month == 4) {
            switch (4) {
                case 4:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
            }
        } elseif ($month == 5) {
            switch (5) {
                case 5:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
            }
        } elseif ($month == 6) {
            switch (6) {
                case 6:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
                    $this->assign("working_06", $working_06);
                    $this->assign("worked_06", $worked_06);
            }
        } elseif ($month == 7) {
            switch (7) {
                case 7:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
                    $this->assign("working_06", $working_06);
                    $this->assign("worked_06", $worked_06);
                    $this->assign("working_07", $working_07);
                    $this->assign("worked_07", $worked_07);
            }
        } elseif ($month == 8) {
            switch (8) {
                case 8:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
                    $this->assign("working_06", $working_06);
                    $this->assign("worked_06", $worked_06);
                    $this->assign("working_07", $working_07);
                    $this->assign("worked_07", $worked_07);
                    $this->assign("working_08", $working_08);
                    $this->assign("worked_08", $worked_08);
            }
        } elseif ($month == 9) {
            switch (9) {
                case 9:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
                    $this->assign("working_06", $working_06);
                    $this->assign("worked_06", $worked_06);
                    $this->assign("working_07", $working_07);
                    $this->assign("worked_07", $worked_07);
                    $this->assign("working_08", $working_08);
                    $this->assign("worked_08", $worked_08);
                    $this->assign("working_09", $working_09);
                    $this->assign("worked_09", $worked_09);
            }
        } elseif ($month == 10) {
            switch (10) {
                case 10:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
                    $this->assign("working_06", $working_06);
                    $this->assign("worked_06", $worked_06);
                    $this->assign("working_07", $working_07);
                    $this->assign("worked_07", $worked_07);
                    $this->assign("working_08", $working_08);
                    $this->assign("worked_08", $worked_08);
                    $this->assign("working_09", $working_09);
                    $this->assign("worked_09", $worked_09);
                    $this->assign("working_10", $working_10);
                    $this->assign("worked_10", $worked_10);
            }
        } elseif ($month == 11) {
            switch (11) {
                case 11:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
                    $this->assign("working_06", $working_06);
                    $this->assign("worked_06", $worked_06);
                    $this->assign("working_07", $working_07);
                    $this->assign("worked_07", $worked_07);
                    $this->assign("working_08", $working_08);
                    $this->assign("worked_08", $worked_08);
                    $this->assign("working_09", $working_09);
                    $this->assign("worked_09", $worked_09);
                    $this->assign("working_10", $working_10);
                    $this->assign("worked_10", $worked_10);
                    $this->assign("working_11", $working_11);
                    $this->assign("worked_11", $worked_11);
            }
        } elseif ($month == 12) {
            switch (12) {
                case 12:
                    $this->assign("working_01", $working_01);
                    $this->assign("worked_01", $worked_01);
                    $this->assign("working_02", $working_02);
                    $this->assign("worked_02", $worked_02);
                    $this->assign("working_03", $working_03);
                    $this->assign("worked_03", $worked_03);
                    $this->assign("working_04", $working_04);
                    $this->assign("worked_04", $worked_04);
                    $this->assign("working_05", $working_05);
                    $this->assign("worked_05", $worked_05);
                    $this->assign("working_06", $working_06);
                    $this->assign("worked_06", $worked_06);
                    $this->assign("working_07", $working_07);
                    $this->assign("worked_07", $worked_07);
                    $this->assign("working_08", $working_08);
                    $this->assign("worked_08", $worked_08);
                    $this->assign("working_09", $working_09);
                    $this->assign("worked_09", $worked_09);
                    $this->assign("working_10", $working_10);
                    $this->assign("worked_10", $worked_10);
                    $this->assign("working_11", $working_11);
                    $this->assign("worked_11", $worked_11);
                    $this->assign("working_12", $working_12);
                    $this->assign("worked_12", $worked_12);
            }
        }
        $this->display();
    }
}