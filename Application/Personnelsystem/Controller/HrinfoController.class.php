<?php
namespace Personnelsystem\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class HrinfoController extends LoginTrueController
{
    public function index()
    {
        $this->LoginTrue();
        $statesId=$_GET["statesId"];
        $hrreserve=M("hrreserve");
        if($statesId==0){
            $rs_hrreserves=$hrreserve->select();
        }
        if($statesId==1){
            $rs_hrreserves=$hrreserve->where("hrState=0")->select();
        }elseif($statesId==2){
            $rs_hrreserves=$hrreserve->where("hrState=1 AND hrStateSuccess=0")->select();
        }elseif($statesId==3){
            $rs_hrreserves=$hrreserve->where("hrState=1 AND hrStateSuccess=1 AND hrStateSuccessJob=0")->select();
        }elseif($statesId==4){
            $rs_hrreserves=$hrreserve->where("hrState=1 AND hrStateSuccess=1 AND hrStateSuccessJob=1")->select();
        }
        
        $this->assign("rs_hrreserves",$rs_hrreserves);
        if($statesId==0){
            $this->display("lists");
        }else{
            $this->display();
        }
        
    }
    
    public function main(){
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $this->assign("hrId",$hrId);
        $printId=$_GET["printId"];
        $this->assign("printId",$printId);
        $hrreserve=M("hrreserve");
        $rs_hrreserve=$hrreserve->where("hrId='{$hrId}'")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        //性别的处理
        if($rs_hrreserve["hrSex"]==1){
            $sex="男";
        }else{
            $sex="女";
        }
        $this->assign("sex",$sex);
        
        // 学历的处理
        $variables = M("variables");
        $rs_xueli = $variables->where("vId=1")->find();
        $xueli = explode("|", $rs_xueli["vVariablesVal"]);
        $newxueliId=$rs_hrreserve["hrDegrees"];
        $xueliInfo=$xueli[$newxueliId];
        $this->assign("xueliInfo", $xueliInfo);
        //职务/职位的处理
        $rs_zhiwu = $variables->where("vId=3")->find();
        $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]);
        $newzhiwuId=$rs_hrreserve["hrDuties"];
        $zhiwuInfo=$zhiwu[$newzhiwuId];
        $this->assign("zhiwuInfo", $zhiwuInfo);
        //算年龄
        $nowYear=date("Y");
        $age=explode("-",$rs_hrreserve["hrBirthdate"]);
        if($age[0]!=0000){
            $nowAge=$nowYear-$age[0];
            if($nowAge>120){
                $nowAge=0;
            }
        }else{
            $nowAge=0;
        }
        $this->assign("nowAge",$nowAge);
        
        //求简历分值：
        $shengri=$rs_hrreserve["hrBirthdate"];
        $jiguan=$rs_hrreserve["hrNativePlace"];
        $zhiwei=$rs_hrreserve["hrDuties"];
        $yuexin=$rs_hrreserve["hrSalary"];
        
        $email=$rs_hrreserve["hrEmail"];
        $listscoreE=0;
        if($email!=null){
            $listscoreE=5;
        }
        $tizhong=$rs_hrreserve["hrWeight"];
        $listscoreT=0;
        if($tizhong!=null){
            $listscoreT=5;
        }
        $IDCard=$rs_hrreserve["hrIDCard"];
        $listscoreID=0;
        if($IDCard!=null){
            $listscoreID=5;
        }
        $mianmao=$rs_hrreserve["hrPoliticalIandscape"];
        $listscoreM=0;
        if($mianmao!=null){
            $listscoreM=5;
        }
        $jiaoyubj=$rs_hrreserve["hrEdu"];
        $listscoreJ=0;
        if($jiaoyubj!=null){
            $listscoreJ=15;
        }
        $gongzuojl=$rs_hrreserve["hrWorks"];
        $listscoreG=0;
        if($gongzuojl!=null){
            $listscoreG=15;
        }
        $jineng=$rs_hrreserve["hrJineng"];
        $listscoreJN=0;
        if($jineng!=null){
            $listscoreJN=10;
        }
        $pinjia=$rs_hrreserve["hrPingjia"];
        $listscoreP=0;
        if($pinjia!=null){
            $listscoreP=10;
        }
        $beizhu=$rs_hrreserve["hrRemarks"];
        $listscoreBZ=0;
        if($beizhu!=null){
            $listscoreBZ=5;
        }
        
        $photo=$rs_hrreserve["hrPhoto"];
        $listscorePH=0;
        if($photo!=null){
            $listscorePH=15;
        }
        
        $jianli=$rs_hrreserve["hrEnclosure"];
        $listscoreJL=0;
        if($jianli!=null){
            $listscoreJL=5;
        }
        
        $IDimg=$rs_hrreserve["hrShenfenzhengImages"];
        $listscoreIDIMG=0;
        if($IDimg!=null){
            $listscoreIDIMG=5;
        }
        $zongfen=$listscoreE+$listscoreT+$listscoreID+$listscoreM+$listscoreJ+$listscoreG+$listscoreJN+$listscoreP+$listscoreBZ+$listscorePH+$listscoreJL+$listscoreIDIMG;
        $fenzhi="";
        if($shengri==null OR $jiguan==null OR $zhiwei==null OR $yuexin==null){
            $fenzhi=0;
        }else{
            $fenzhi=$zongfen;
        }
        $this->assign("fenzhi",$fenzhi);
        //减分计算
        $jianfenXL=0;
        if($rs_hrreserve["hrDegrees"]==0){
            $jianfenXL=10;
        }elseif($rs_hrreserve["hrDegrees"]==1){
            $jianfenXL=8;
        }elseif($rs_hrreserve["hrDegrees"]==2){
            $jianfenXL=6;
        }elseif($rs_hrreserve["hrDegrees"]==3){
            $jianfenXL=2;
        }
        $jianfenCT=0;
        if($rs_hrreserve["hrCity"]==null){
            $jianfenCT=3;
        }
        $jianfenHE=0;
        if($rs_hrreserve["hrSex"]==1){
            if($rs_hrreserve["hrHeight"]<165){
                $jianfenHE=4;
            }elseif($rs_hrreserve["hrHeight"]<170 && $rs_hrreserve["hrHeight"]>=165){
                $jianfenHE=3;
            }elseif($rs_hrreserve["hrHeight"]<175 && $rs_hrreserve["hrHeight"]>=170){
                $jianfenHE=1;
            }
        }else{
            if($rs_hrreserve["hrHeight"]<150){
                $jianfenHE=4;
            }elseif($rs_hrreserve["hrHeight"]<155 && $rs_hrreserve["hrHeight"]>=150){
                $jianfenHE=3;
            }elseif($rs_hrreserve["hrHeight"]<160 && $rs_hrreserve["hrHeight"]>=155){
                $jianfenHE=2;
            }elseif($rs_hrreserve["hrHeight"]<165 && $rs_hrreserve["hrHeight"]>=160){
                $jianfenHE=1;
            }
        }
        $jianfenYX=0;
        if($rs_hrreserve["hrSalary"]<2000){
            $jianfenYX=5;
        }elseif($rs_hrreserve["hrSalary"]<3000 && $rs_hrreserve["hrSalary"]>=2000){
            $jianfenYX=4;
        }elseif($rs_hrreserve["hrSalary"]<4000 && $rs_hrreserve["hrSalary"]>=3000){
            $jianfenYX=3;
        }elseif($rs_hrreserve["hrSalary"]<5000 && $rs_hrreserve["hrSalary"]>=4000){
            $jianfenYX=2;
        }elseif($rs_hrreserve["hrSalary"]<6000 && $rs_hrreserve["hrSalary"]>=5000){
            $jianfenYX=1;
        }
        $hotscore=$fenzhi-$jianfenHE-$jianfenCT-$jianfenXL-$jianfenYX;
        if($hotscore<=-10){
            $hotscore=$fenzhi-$jianfenHE-$jianfenCT-$jianfenXL-$jianfenYX+20;
        }elseif($hotscore<=0 && $hotscore>-10){
            $hotscore=$fenzhi-$jianfenHE-$jianfenCT-$jianfenXL-$jianfenYX+13;
        }elseif($hotscore<=20 && $hotscore>0){
            $hotscore=20;
        }elseif($hotscore>100){
            $hotscore=100;
        }
        $this->assign("hotscore",$hotscore);
        $this->display();
    }
    public function feedBack(){
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $rs_hrreserve=$hrreserve->where("hrId={$hrId}")->field("hrId,hrName,hrTel,hrSex")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        $this->display();
    }
    public function feedBackAction(){
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $data["hrState"]=1;
        $data["hrStateSuccess"]=$_POST["hrStateSuccess"];
        $data["hrStateInfo"]=$_POST["hrStateInfo"];
        $data["hrStateDate"]=date("Y-m-d H:i:s");
        $hrreserve=M("hrreserve");
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
            if($data["hrStateSuccess"]==0){
                $this->success("反馈成功",U("index?statesId=2"));
            }else{
                $this->success("反馈成功",U("index?statesId=3"));
            }
        }else{
            $this->error("反馈失败");
        }
        
    }
    public function successFeedBack(){
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $rs_hrreserve=$hrreserve->where("hrId={$hrId}")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        
        $successDate=date("Y-m-d");
        $this->assign("successDate",$successDate);
        // 部门的处理
        $department = M("department");
        $rs_department = $department->select();
        $this->assign("rs_department", $rs_department);
        
        //职称的处理
        $variables = M("variables");
        $rs_zhicheng = $variables->where("vId=2")->find();
        $zhicheng = explode("|", $rs_zhicheng["vVariablesVal"]);
        $this->assign("zhicheng", $zhicheng);
        
        //职务的处理【应聘岗位】
        $rs_zhiwu = $variables->where("vId=3")->find();
        $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]);
        $this->assign("zhiwu", $zhiwu);
        
        //入职编号的处理
        $staffNum = M("staff");
        $rs_staffNum = $staffNum->count();
        $number = $rs_staffNum + 1;
        $this->assign("number", $number);
        $this->display();
    }
    public function successFeedBackAction(){
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $data["stNum"]=$_POST["stNum"];
        $data["stEntryDate"]=$_POST["stEntryDate"];
        $data["stDid"]=$_POST["stDid"];
        $data["stPositionalTitles"]=$_POST["stPositionalTitles"];
        $data["stDuties"]=$_POST["stDuties"];
        $dataHR["hrEntryDate"]=$data["stEntryDate"];
        $dataHR["hrStateSuccessJob"]=1;
        $hrreserve=M("hrreserve");
        $rs_HRorStaff=$hrreserve->where("hrId={$hrId}")->find();

        $data["stName"]=$rs_HRorStaff["hrName"];
        $data["stTel"]=$rs_HRorStaff["hrTel"];
        $data["stSex"]=$rs_HRorStaff["hrSex"];
        $data["stBirthdateType"]=$rs_HRorStaff["hrBirthdateType"];
        $data["stBirthdate"]=$rs_HRorStaff["hrBirthdate"];
        $data["stMultiracial"]=$rs_HRorStaff["hrMultiracial"];
        $data["stQQ"]=$rs_HRorStaff["hrQQ"];
        $data["stEmail"]=$rs_HRorStaff["hrEmail"];
        $data["stDegrees"]=$rs_HRorStaff["hrDegrees"];
        $data["stNativePlace"]=$rs_HRorStaff["hrNativePlace"];
        $data["stCity"]=$rs_HRorStaff["hrCity"];
        $data["stJobState"]=1;
        $data["stPhoto"]=$rs_HRorStaff["hrPhoto"];
        $data["stHeight"]=$rs_HRorStaff["hrHeight"];
        $data["stWeight"]=$rs_HRorStaff["hrWeight"];
        $data["stIDCard"]=$rs_HRorStaff["hrIDCard"];
        $data["stPoliticalIandscape"]=$rs_HRorStaff["hrPoliticalIandscape"];
        $data["stEnclosure"]=$rs_HRorStaff["hrEnclosure"];
        $data["stShenfenzhengImages"]=$rs_HRorStaff["hrShenfenzhengImages"];
        $data["stJingyuan"]=$rs_HRorStaff["hrWorks"];
        $data["stJineng"]=$rs_HRorStaff["hrJineng"];
        $staffAdd = M("staff");
        $resultST=$staffAdd->add($data);
        $resultHR=$hrreserve->where("hrId={$hrId}")->save($dataHR);
        if($resultHR && $resultST){
                $this->success("办理入职手续成功",U("index?statesId=4"));
        }else{
            $this->error("办理入职手续失败");
        }
    
    }
}