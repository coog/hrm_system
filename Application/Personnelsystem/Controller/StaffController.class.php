<?php
namespace Personnelsystem\Controller;

use Think\Controller;
use Think\Upload;
ob_end_clean();
header("content-type:text/html;charset=utf-8");

class StaffController extends LoginTrueController
{

    public function Add()
    {
        $this->LoginTrue();
        $depId = $_GET["depId"];
        $staffNum = M("staff");
        $rs_staffNum = $staffNum->count();
        $number = $rs_staffNum + 1;
        $this->assign("number", $number);
        // 部门的处理
        $department = M("department");
        if ($depId != 0) {
            $rs_department = $department->field("dId,dPid,dPsid,dName")
                ->where("dId={$depId}")
                ->find();
        } else {
            $rs_department = $department->field("dId,dPid,dPsid,dName")->select();
        }
        $this->assign("depId", $depId);
        $this->assign("rs_department", $rs_department);
        // 下面对自定义变量的处理
        $variables = M("variables");
        $rs_xueli = $variables->where("vId=1")->find();
        $xueli = explode("|", $rs_xueli["vVariablesVal"]);
        $this->assign("xueli", $xueli);
        
        $rs_zhicheng = $variables->where("vId=2")->find();
        $zhicheng = explode("|", $rs_zhicheng["vVariablesVal"]);
        $this->assign("zhicheng", $zhicheng);
        
        $rs_zhiwu = $variables->where("vId=3")->find();
        $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]);
        $this->assign("zhiwu", $zhiwu);
        
        $rs_hunyin = $variables->where("vId=4")->find();
        $hunyin = explode("|", $rs_hunyin["vVariablesVal"]);
        $this->assign("hunyin", $hunyin);
        
        $rs_xuexing = $variables->where("vId=5")->find();
        $xuexing = explode("|", $rs_xuexing["vVariablesVal"]);
        $this->assign("xuexing", $xuexing);
        
        $rs_leibie = $variables->where("vId=6")->find();
        $leibie = explode("|", $rs_leibie["vVariablesVal"]);
        $this->assign("leibie", $leibie);
        
        $rs_minzu = $variables->where("vId=7")->find();
        $minzu = explode("|", $rs_minzu["vVariablesVal"]);
        $this->assign("minzu", $minzu);
        
        $this->display();
    }

    public function up()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Staff/photo/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }

    public function upfujian()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Staff/fujian/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }

    public function upsfz()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Staff/shenfenzheng/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }

    public function uplizhi()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Staff/lizhibiao/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }

    public function AddAction()
    {
        $this->LoginTrue();
        $staffAdd = M("staff");
        $yinsu = $_POST["yinsu"];
        $string = implode(",", $yinsu);
        $data["stYinsuyuan"] = $string;
        $data["stNum"] = $_POST["stNum"];
        $data["stName"] = $_POST["stName"];
        $data["stSex"] = $_POST["stSex"];
        $data["stBirthdate"] = $_POST["stBirthdate"];
        $data["stBirthdateType"] = $_POST["stBirthdateType"];
        $data["stTel"] = $_POST["stTel"];
        $data["stTelT"] = $_POST["stTelT"];
        $data["stMarital"] = $_POST["stMarital"];
        $data["stAddress"] = $_POST["stAddress"];
        $data["stDegrees"] = $_POST["stDegrees"];
        $data["stEntryDate"] = $_POST["stEntryDate"];
        $data["stBloodType"] = $_POST["stBloodType"];
        $data["stHealth"] = $_POST["stHealth"];
        $data["stPersonnelCategory"] = $_POST["stPersonnelCategory"];
        $data["stWinning"] = $_POST["stWinning"];
        $data["stAssessment"] = $_POST["stAssessment"];
        $data["stRemarks"] = $_POST["stRemarks"];
        $data["stDid"] = $_POST["stDid"];
        $data["stPositionalTitles"] = $_POST["stPositionalTitles"];
        if ($data["stPositionalTitles"] == "") {
            $data["stPositionalTitles"] = "普通员工";
        }
        $data["stDuties"] = $_POST["stDuties"];
        if ($data["stDuties"] == "") {
            $data["stDuties"] = "暂未录入";
        }
        $data["stHeight"] = $_POST["stHeight"];
        if ($data["stHeight"] == "") {
            $data["stHeight"] = "0";
        }
        $data["stWeight"] = $_POST["stWeight"];
        if ($data["stWeight"] == "") {
            $data["stWeight"] = "0";
        }
        $data["stMultiracial"] = $_POST["stMultiracial"];
        if ($data["stMultiracial"] == "") {
            $data["stMultiracial"] = "汉族";
        }
        $data["stNativePlace"] = $_POST["stNativePlace"];
        if ($data["stNativePlace"] == "") {
            $data["stNativePlace"] = "未录入";
        }
        $data["stCity"] = $_POST["stCity"];
        if ($data["stCity"] == "") {
            $data["stCity"] = "未录入";
        }
        $data["stPhoto"] = $_POST["stPhoto"];
        if ($data["stPhoto"] == "") {
            $data["stPhoto"] = "0";
        }
        $data["stPoliticalIandscape"] = $_POST["stPoliticalIandscape"];
        if ($data["stPoliticalIandscape"] == "") {
            $data["stPoliticalIandscape"] = "普通公民";
        }
        $data["stIDCard"] = $_POST["stIDCard"];
        if ($data["stIDCard"] == "") {
            $data["stIDCard"] = "";
        } else {
            $rs_stID = $staffAdd->select();
            foreach ($rs_stID as $val_stID) {
                if ($data["stIDCard"] == $val_stID["stIDCard"]) {
                    if ($data["stIDCard"] != "") {
                        $this->error("此身份证号码已经存在");
                    }
                }
            }
        }
        $data["stQQ"] = $_POST["stQQ"];
        if ($data["stQQ"] == "") {
            $data["stQQ"] = "未填写";
        }
        $data["stEmail"] = $_POST["stEmail"];
        if ($data["stEmail"] == "") {
            $data["stEmail"] = "未填写";
        }
        $data["stEnclosure"] = $_POST["stEnclosure"];
        $data["stShenfenzhengImages"] = $_POST["stShenfenzhengImages"];
        $data["stJingyuan"] = $_POST["stJingyuan"];
        if ($data["stJingyuan"] == "") {
            $data["stJingyuan"] = "未填写";
        }
        $data["stJineng"] = $_POST["stJineng"];
        if ($data["stJineng"] == "") {
            $data["stJineng"] = "未填写";
        }
        $data["stInputDate"] = date("Y-m-d");
        $result = $staffAdd->add($data);
        if ($result) {
            $this->success("录入员工信息成功");
        } else {
            $this->error("录入员工信息失败");
        }
    }

    public function Lists()
    {
        $this->LoginTrue();
        $staffLists = M("staff");
        $depId = $_GET["depId"];
        if ($depId != 0) {
            $rs_staffLists = $staffLists->where("stBlacklist=0 AND stDid={$depId}")->select();
        } else {
            $rs_staffLists = $staffLists->where("stBlacklist=0")->select();
        }
        $this->assign("rs_staffLists", $rs_staffLists);
        $year = date("Y");
        $this->assign("year", $year);
        $this->assign("depId", $depId);
        $this->display();
    }

    public function JobLists()
    {
        $this->LoginTrue();
        $staffLists = M("staff");
        $depId = $_GET["depId"];
        if ($depId != 0) {
            $rs_staffLists = $staffLists->where("stBlacklist=0 AND stJobState=1 AND stDid={$depId}")->select();
        } else {
            $rs_staffLists = $staffLists->where("stBlacklist=0 AND stJobState=1")->select();
        }
        $this->assign("rs_staffLists", $rs_staffLists);
        $this->assign("depId", $depId);
        $year = date("Y");
        $this->assign("year", $year);
        $this->display();
    }

    public function NoJobLists()
    {
        $this->LoginTrue();
        $staffLists = M("staff");
        $depId = $_GET["depId"];
        if ($depId != 0) {
            $rs_staffLists = $staffLists->where("stBlacklist=0  AND stJobState!=1  AND stDid={$depId}")->select();
        } else {
            $rs_staffLists = $staffLists->where("stBlacklist=0  AND stJobState!=1")->select();
        }
        $this->assign("rs_staffLists", $rs_staffLists);
        $this->assign("depId", $depId);
        $year = date("Y");
        $this->assign("year", $year);
        $this->display();
    }

    public function BackLists()
    {
        $this->LoginTrue();
        $staffLists = M("staff");
        $depId = $_GET["depId"];
        if ($depId != 0) {
            $rs_staffLists = $staffLists->where("stBlacklist=1 AND stDid={$depId}")->select();
        } else {
            $rs_staffLists = $staffLists->where("stBlacklist=1")->select();
        }
        $this->assign("rs_staffLists", $rs_staffLists);
        $this->assign("depId", $depId);
        $year = date("Y");
        $this->assign("year", $year);
        $this->display();
    }

    public function Show()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffShow", $rs_staffShow);
        // 算年龄
        $nowTime = time();
        $year = date("Y");
        $staffShow_stBirthdate = strtotime($rs_staffShow["stBirthdate"]);
        $nowAge = floor(((($nowTime - $staffShow_stBirthdate) / 86400) / 365));
        $this->assign("nowAge", $nowAge);
        // 算工龄
        $workingYear = strtotime($rs_staffShow["stEntryDate"]);
        $quiteYear = strtotime($rs_staffShow["stDepartureDate"]);
        if ($quiteYear != 0) {
            $nowWorkingYear = round(((($quiteYear - $workingYear) / 86400) / 365), 1);
        } else {
            $nowWorkingYear = round(((($nowTime - $workingYear) / 86400) / 365), 1);
        }
        $this->assign("nowWorkingYear", $nowWorkingYear);
        
        $department = M("department");
        $rs_department = $department->field("dId,dName")
            ->where("dId={$rs_staffShow["stDid"]}")
            ->find();
        $this->assign("rs_department", $rs_department);
        // 算黑名单间隔天数
        if ($rs_staffShow["stBlacklistEndDate"] != 0) {
            $startTime = strtotime($rs_staffShow["stBlacklistNowDate"]);
            $endTime = strtotime($rs_staffShow["stBlacklistEndDate"]);
            $days = ceil((abs($endTime - $startTime)) / 86400);
            $newTime = $days * 86400;
            $days = date("Y-m-d", $newTime);
            $days = explode("-", $days);
            $days = ($days[0] - 1970) . " 年 零 " . ($days[1] - 1) . " 个月 零 " . ($days[2] - 1) . " 天 ";
        } else {
            $days = "永久禁封";
        }
        $this->assign("days", $days);
        
        // 洗黑还剩天数
        if ($rs_staffShow["stBlacklistEndDate"] != 0) {
            $nBacklistTime = floor(($endTime - $nowTime) / 86400);
            if ($nBacklistTime <= 0) {
                $nBacklistTime = "已洗白";
            } else {
                $nBacklistTime = $nBacklistTime . " 天 ";
            }
        } else {
            $nBacklistTime = "永久黑名单";
        }
        $this->assign("nBacklistTime", $nBacklistTime);
        $this->display();
    }

    public function ShowImages()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffShow", $rs_staffShow);
        
        // 算年龄
        $nowTime = time();
        $year = date("Y");
        $staffShow_stBirthdate = strtotime($rs_staffShow["stBirthdate"]);
        $nowAge = floor(((($nowTime - $staffShow_stBirthdate) / 86400) / 365));
        $this->assign("nowAge", $nowAge);
        
        $this->display();
    }

    public function ShowFujian()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffShow", $rs_staffShow);
        
        // 算年龄
        $nowTime = time();
        $year = date("Y");
        $staffShow_stBirthdate = strtotime($rs_staffShow["stBirthdate"]);
        $nowAge = floor(((($nowTime - $staffShow_stBirthdate) / 86400) / 365));
        $this->assign("nowAge", $nowAge);
        
        $this->display();
    }

    public function ShowRuzhi()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $ruzhibiaofujian = $rs_staffShow["stEnclosure"];
        $ruzhilength = strlen($ruzhibiaofujian);
        $this->assign("ruzhilength", $ruzhilength);
        $this->assign("ruzhibiaofujian", $ruzhibiaofujian);
        $this->assign("rs_staffShow", $rs_staffShow);
        // 算年龄
        $nowTime = time();
        $year = date("Y");
        $staffShow_stBirthdate = strtotime($rs_staffShow["stBirthdate"]);
        $nowAge = floor(((($nowTime - $staffShow_stBirthdate) / 86400) / 365));
        $this->assign("nowAge", $nowAge);
        
        $this->display();
    }

    public function ShowShenfenzheng()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $shenfenzhengfujian = $rs_staffShow["stShenfenzhengImages"];
        $shenfenzhenglength = strlen($shenfenzhengfujian);
        $this->assign("shenfenzhenglength", $shenfenzhenglength);
        $this->assign("shenfenzhengfujian", $shenfenzhengfujian);
        $this->assign("rs_staffShow", $rs_staffShow);
        // 算年龄
        $nowTime = time();
        $year = date("Y");
        $staffShow_stBirthdate = strtotime($rs_staffShow["stBirthdate"]);
        $nowAge = floor(((($nowTime - $staffShow_stBirthdate) / 86400) / 365));
        $this->assign("nowAge", $nowAge);
        
        $this->display();
    }

    public function ShowLizhi()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $lizhifujian = $rs_staffShow["stLizhiImages"];
        $lizhilength = strlen($lizhifujian);
        $this->assign("lizhilength", $lizhilength);
        $this->assign("lizhifujian", $lizhifujian);
        $this->assign("rs_staffShow", $rs_staffShow);
        
        // 算年龄
        $nowTime = time();
        $year = date("Y");
        $staffShow_stBirthdate = strtotime($rs_staffShow["stBirthdate"]);
        $nowAge = floor(((($nowTime - $staffShow_stBirthdate) / 86400) / 365));
        $this->assign("nowAge", $nowAge);
        
        $this->display();
    }

    public function QuitAdd()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffShow", $rs_staffShow);
        
        $nowTime = time();
        $workingYear = strtotime($rs_staffShow["stEntryDate"]);
        $quiteYear = strtotime($rs_staffShow["stDepartureDate"]);
        if ($quiteYear != 0) {
            $nowWorkingYear = round(((($quiteYear - $workingYear) / 86400) / 365), 1);
        } else {
            $nowWorkingYear = round(((($nowTime - $workingYear) / 86400) / 365), 1);
        }
        $this->assign("nowWorkingYear", $nowWorkingYear);
        
        $nowDate = date("Y-m-d");
        $this->assign("nowDate", $nowDate);
        
        $department = M("department");
        $rs_department = $department->field("dId,dName")
            ->where("dId={$rs_staffShow["stDid"]}")
            ->find();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function QuitAddAction()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $data["stDepartureDate"] = $_POST["stDepartureDate"];
        $data["stLizhiImages"] = $_POST["stLizhiImages"];
        $data["stDepartureSo"] = $_POST["stDepartureSo"];
        if ($data["stDepartureSo"] == "") {
            $data["stDepartureSo"] = "未注明原因";
        }
        $data["stJobState"] = 0;
        $data["stBlacklist"] = $_POST["stBlacklist"];
        $staffSet = M("staff");
        $result = $staffSet->where("stId={$stId}")->save($data);
        if ($result) {
            if ($data["stBlacklist"] != 0) {
                $this->success("成功离职", U("Staff/BackListsAdd/stId/$stId"));
            } else {
                $this->success("成功离职", U("lists"));
            }
        } else {
            $this->error("离职失败", U("lists"));
        }
    }

    public function BackListsAdd()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffShow", $rs_staffShow);
        $nowTime = time();
        $workingYear = strtotime($rs_staffShow["stEntryDate"]);
        $quiteYear = strtotime($rs_staffShow["stDepartureDate"]);
        if ($quiteYear != 0) {
            $nowWorkingYear = round(((($quiteYear - $workingYear) / 86400) / 365), 1);
        } else {
            $nowWorkingYear = round(((($nowTime - $workingYear) / 86400) / 365), 1);
        }
        $this->assign("nowWorkingYear", $nowWorkingYear);
        
        $nowDate = date("Y-m-d");
        $this->assign("nowDate", $nowDate);
        
        $department = M("department");
        $rs_department = $department->field("dId,dName")
            ->where("dId={$rs_staffShow["stDid"]}")
            ->find();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function BackListsAddAction()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $data["stBlacklist"] = 1;
        $data["stBlacklistNowDate"] = $_POST["stBlacklistNowDate"];
        $data["stBlacklistEndDate"] = $_POST["stBlacklistEndDate"];
        if ($data["stBlacklistEndDate"] == "") {
            $data["stBlacklistEndDate"] = 0;
        }
        $data["stBlacklistSo"] = $_POST["stBlacklistSo"];
        if ($data["stBlacklistSo"] == "") {
            $data["stBlacklistSo"] = "未注明原因";
        }
        $staffSet = M("staff");
        $result = $staffSet->where("stId={$stId}")->save($data);
        if ($result) {
            $this->success("成功加入黑名单", U("lists"));
        } else {
            $this->error("加入黑名单失败");
        }
    }

    public function Del()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $rs_staffShow = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffShow", $rs_staffShow);
        $this->display();
    }

    public function DelAction()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $result = $staffShow->where("stId={$stId}")->delete();
        if ($result) {
            $this->success("删除成功", U("lists"));
        } else {
            $this->error("删除失败");
        }
    }

    public function DelAll()
    {
        $this->LoginTrue();
        $stId = $_POST["node"];
        $staff = M("staff");
        foreach ($stId as $val) {
            $result = $staff->where("stId={$val}")->delete();
        }
        $this->success("删除成功", U("lists"));
    }

    public function Update()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $depId = $_GET["depId"];
        $staffShow = M("staff");
        $rs_staffInfo = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffInfo", $rs_staffInfo);
        $department = M("department");
        if ($depId != 0) {
            $rs_department = $department->field("dId,dPid,dPsid,dName")
                ->where("dId={$depId}")
                ->find();
        } else {
            $rs_department = $department->field("dId,dPid,dPsid,dName")->select();
        }
        $this->assign("depId", $depId);
        // 学历的处理
        $variables = M("variables");
        $rs_xueli = $variables->where("vId=1")->find();
        $xueli = explode("|", $rs_xueli["vVariablesVal"]);
        $this->assign("xueli", $xueli);
        
        $rs_zhicheng = $variables->where("vId=2")->find();
        $zhicheng = explode("|", $rs_zhicheng["vVariablesVal"]);
        $this->assign("zhicheng", $zhicheng);
        
        $rs_zhiwu = $variables->where("vId=3")->find();
        $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]);
        $this->assign("zhiwu", $zhiwu);
        
        $rs_hunyin = $variables->where("vId=4")->find();
        $hunyin = explode("|", $rs_hunyin["vVariablesVal"]);
        $this->assign("hunyin", $hunyin);
        
        $rs_xuexing = $variables->where("vId=5")->find();
        $xuexing = explode("|", $rs_xuexing["vVariablesVal"]);
        $this->assign("xuexing", $xuexing);
        
        $rs_leibie = $variables->where("vId=6")->find();
        $leibie = explode("|", $rs_leibie["vVariablesVal"]);
        $this->assign("leibie", $leibie);
        
        $rs_minzu = $variables->where("vId=7")->find();
        $minzu = explode("|", $rs_minzu["vVariablesVal"]);
        $this->assign("minzu", $minzu);
        
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function UpdateAction()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffAdd = M("staff");
        $yinsu = $_POST["yinsu"];
        $yinsuyuan = array_unique($yinsu);
        $string = implode(",", $yinsuyuan);
        $data["stYinsuyuan"] = $string;
        $data["stNum"] = $_POST["stNum"];
        $data["stName"] = $_POST["stName"];
        $data["stSex"] = $_POST["stSex"];
        $data["stBirthdate"] = $_POST["stBirthdate"];
        $data["stBirthdateType"] = $_POST["stBirthdateType"];
        $data["stTel"] = $_POST["stTel"];
        $data["stTelT"] = $_POST["stTelT"];
        $data["stMarital"] = $_POST["stMarital"];
        $data["stAddress"] = $_POST["stAddress"];
        $data["stDegrees"] = $_POST["stDegrees"];
        $data["stEntryDate"] = $_POST["stEntryDate"];
        $data["stBloodType"] = $_POST["stBloodType"];
        $data["stHealth"] = $_POST["stHealth"];
        $data["stPersonnelCategory"] = $_POST["stPersonnelCategory"];
        $data["stWinning"] = $_POST["stWinning"];
        $data["stAssessment"] = $_POST["stAssessment"];
        $data["stRemarks"] = $_POST["stRemarks"];
        $data["stDid"] = $_POST["stDid"];
        $data["stPositionalTitles"] = $_POST["stPositionalTitles"];
        if ($data["stPositionalTitles"] == "") {
            $data["stPositionalTitles"] = "普通员工";
        }
        $data["stDuties"] = $_POST["stDuties"];
        if ($data["stDuties"] == "") {
            $data["stDuties"] = "暂未录入";
        }
        $data["stHeight"] = $_POST["stHeight"];
        if ($data["stHeight"] == "") {
            $data["stHeight"] = "0";
        }
        $data["stWeight"] = $_POST["stWeight"];
        if ($data["stWeight"] == "") {
            $data["stWeight"] = "0";
        }
        $data["stMultiracial"] = $_POST["stMultiracial"];
        if ($data["stMultiracial"] == "") {
            $data["stMultiracial"] = "汉族";
        }
        $data["stNativePlace"] = $_POST["stNativePlace"];
        if ($data["stNativePlace"] == "") {
            $data["stNativePlace"] = "未录入";
        }
        $data["stCity"] = $_POST["stCity"];
        if ($data["stCity"] == "") {
            $data["stCity"] = "未录入";
        }
        $data["stPhoto"] = $_POST["stPhoto"];
        if ($data["stPhoto"] == "") {
            $data["stPhoto"] = "0";
        }
        $data["stPoliticalIandscape"] = $_POST["stPoliticalIandscape"];
        if ($data["stPoliticalIandscape"] == "") {
            $data["stPoliticalIandscape"] = "普通公民";
        }
        $IdCard = $data["stIDCard"] = $_POST["stIDCard"];
        if ($data["stIDCard"] == "") {
            $data["stIDCard"] = "未填写";
        } else {
            $rs_stID = $staffAdd->where("stIDCard!='{$IdCard}'")->select();
            foreach ($rs_stID as $val_stID) {
                if ($data["stIDCard"] == $val_stID["stIDCard"]) {
                    $this->error("此身份证号码已经存在");
                }
            }
        }
        $data["stQQ"] = $_POST["stQQ"];
        if ($data["stQQ"] == "") {
            $data["stQQ"] = "未填写";
        }
        $data["stEmail"] = $_POST["stEmail"];
        if ($data["stEmail"] == "") {
            $data["stEmail"] = "未填写";
        }
        $data["stEnclosure"] = $_POST["stEnclosure"];
        $data["stShenfenzhengImages"] = $_POST["stShenfenzhengImages"];
        $data["stJingyuan"] = $_POST["stJingyuan"];
        if ($data["stJingyuan"] == "") {
            $data["stJingyuan"] = "未填写";
        }
        $data["stJineng"] = $_POST["stJineng"];
        if ($data["stJineng"] == "") {
            $data["stJineng"] = "未填写";
        }
        $result = $staffAdd->where("stId={$stId}")->save($data);
        if ($result) {
            $this->success("修改员工信息成功", U("lists"));
        } else {
            $this->error("修改员工信息失败");
        }
    }

    public function UpdateQuit()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $depId = $_GET["depId"];
        $staffShow = M("staff");
        $rs_staffInfo = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffInfo", $rs_staffInfo);
        $department = M("department");
        if ($depId != 0) {
            $rs_department = $department->field("dId,dName")
                ->where("dId={$depId}")
                ->find();
        } else {
            $rs_department = $department->field("dId,dName")->select();
        }
        $this->assign("depId", $depId);
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function UpdateQuitAction()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $data["stNum"] = $_POST["stNum"];
        $data["stDepartureDate"] = $_POST["stDepartureDate"];
        if ($data["stDepartureDate"] == "") {
            $data["stDepartureDate"] = "0";
        }
        $data["stDepartureSo"] = $_POST["stDepartureSo"];
        if ($data["stDepartureSo"] == "") {
            $data["stDepartureSo"] = "还未填写";
        }
        $data["stJobState"] = $_POST["stJobState"];
        $result = $staffShow->where("stId={$stId}")->save($data);
        if ($result) {
            $this->success("修改离职员工信息成功");
        } else {
            $this->error("修改离职员工信息失败");
        }
    }

    public function UpdateBackLists()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $depId = $_GET["depId"];
        $staffShow = M("staff");
        $rs_staffInfo = $staffShow->where("stId={$stId}")->find();
        $this->assign("rs_staffInfo", $rs_staffInfo);
        $department = M("department");
        if ($depId != 0) {
            $rs_department = $department->field("dId,dName")
                ->where("dId={$depId}")
                ->find();
        } else {
            $rs_department = $department->field("dId,dName")->select();
        }
        $this->assign("depId", $depId);
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function UpdateBackListsAction()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffShow = M("staff");
        $data["stBlacklistNowDate"] = $_POST["stBlacklistNowDate"];
        if ($data["stBlacklistNowDate"] == "") {
            $data["stBlacklistNowDate"] = "0";
        }
        $data["stBlacklistEndDate"] = $_POST["stBlacklistEndDate"];
        if ($data["stBlacklistEndDate"] == "") {
            $data["stBlacklistEndDate"] = "0";
        }
        $data["stBlacklistSo"] = $_POST["stBlacklistSo"];
        if ($data["stBlacklistSo"] == "") {
            $data["stBlacklistSo"] = "暂未说明";
        }
        $data["stBlacklist"] = $_POST["stBlacklist"];
        $result = $staffShow->where("stId={$stId}")->save($data);
        if ($result) {
            $this->success("修改黑名单员工信息成功");
        } else {
            $this->error("修改黑名单员工信息失败");
        }
    }

    public function ShutDown()
    {
        $this->LoginTrue();
        $this->display();
    }
    
    // 导出
    function export()
    {
        $this->LoginTrue();
        $product_menu = M("department");
        $rs_pmenu = $product_menu->select();
        $this->assign("rs_pmenu", $rs_pmenu);
        $pfexcel_view = M("pfexcel_view");
        $rs_excel_view = $pfexcel_view->field("pfevId,pfevName")->select();
        $this->assign("rs_excel_view", $rs_excel_view);
        $this->display();
    }
    
    public function exportExcel($excelView, $rspmName, $expCellName, $expTableData)
    {
        $pfexcel_view = M("pfexcel_view");
        $excelView = $pfexcel_view->where("pfevId=$excelView")->find();
        if ($rspmName != "") {
            $prm = "---(" . $rspmName . ")";
        } else {
            $prm = "";
        }
        $expTitle = $excelView["pfevTitle"] . $prm;
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle); // 文件名称
        $fileName = $excelView["pfevFileNameOne"] . date('_Y-m-d-His'); // or $xlsTitle 文件名称可根据自己情况设定
        $cellNum = count($expCellName);
        $dataNum = count($expTableData);
        import("Org.Util.PHPExcel");
        vendor("PHPExcel.PHPExcel");
    
        $objPHPExcel = new \PHPExcel();
        $cellName = array(
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z'

        );
        if ($excelView["pfevTitleSheetHeBing"] == 1) {
            $objPHPExcel->getActiveSheet(0)->mergeCells('A1:' . $cellName[$cellNum - 1] . '1'); // 合并单元格
        }
        if ($excelView["pfevDongjie"] == 1) {
            $objPHPExcel->getActiveSheet()->freezePane('A2'); // 冻结单元格
            $objPHPExcel->getActiveSheet()->freezePane('A3'); // 冻结单元格
        }
        if ($excelView["pfevTitleFontBold"] == 1) {
            $titleBold = false;
        } else {
            $titleBold = true;
        }
        if ($excelView["pfevTitleFontItalic"] == 1) {
            $titleItalic = false;
        } else {
            $titleItalic = true;
        }
        $titlecolor = ltrim($excelView["pfevTitleColor"], "#");
        $navcolor = ltrim($excelView["pfevNavColor"], "#");
        $concolor = ltrim($excelView["pfevCColor"], "#");
    
        $objPHPExcel->getActiveSheet()
        ->getStyle('A1')
        ->getFont()
        ->getColor()
        ->setARGB('FF' . $titlecolor);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A1')
        ->getFont()
        ->setName($excelView['pfevTitleFontF'])
        -> // 字体
        setSize($excelView["pfevTitleFontSize"])
        -> // 字体大小
        setBold($titleBold)
        -> // 字体加粗
        setItalic($titleItalic); // 字体倾斜
        if ($excelView["pfevNavFontBold"] == 1) {
            $NavBold = false;
        } else {
            $NavBold = true;
        }
        if ($excelView["pfevNavFontItalic"] == 1) {
            $NavItalic = false;
        } else {
            $NavItalic = true;
        }
        $objPHPExcel->getActiveSheet()
        ->getStyle('A2:Z2')
        ->getFont()
        ->getColor()
        ->setARGB('FF' . $navcolor);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A2:Z2')
        ->getFont()
        ->setName($excelView['pfevNavFontF'])
        -> // 字体
        setSize($excelView["pfevNavFontSize"])
        -> // 字体大小
        setBold($NavBold)
        -> // 字体加粗
        setItalic($NavItalic); // 字体倾斜
    
        if ($excelView["pfevCFontBold"] == 1) {
            $CBold = false;
        } else {
            $CBold = true;
        }
        if ($excelView["pfevNavFontItalic"] == 1) {
            $CItalic = false;
        } else {
            $CItalic = true;
        }
        // 程序设置EXCEL文字颜色
        $objPHPExcel->getActiveSheet()
        ->getStyle('A3:Z' . ($dataNum + 2))
        ->getFont()
        ->getColor()
        ->setARGB('FF' . $concolor);
        // $objPHPExcel->getActiveSheet()->getStyle( 'A3:V'.($dataNum+2))->getFont()->getColor()->setARGB(\PHPExcel_Style_Color::COLOR_RED);
    
        $objPHPExcel->getActiveSheet()
        ->getStyle('A3:Z' . ($dataNum + 2))
        ->getFont()
        ->setName($excelView['pfevCFontF'])
        -> // 字体
        setSize($excelView["pfevCFontSize"])
        -> // 字体大小
        setBold($CBold)
        -> // 字体加粗
        setItalic($CItalic); // 字体倾斜
        // 隐藏某列
        $yc = unserialize($excelView["pfevHiddleNav"]);
        foreach ($yc as $val) {
            $objPHPExcel->getActiveSheet()
            ->getColumnDimension($val)
            ->setVisible(false);
        }
        // 是否自动筛选
        if ($excelView["pfevShaixuan"] == 1) {
            $objPHPExcel->getActiveSheet()->setAutoFilter($objPHPExcel->getActiveSheet()
                ->calculateWorksheetDimension());
        }
    
        $objPHPExcel->getActiveSheet()
        ->getRowDimension('1')
        ->setRowHeight($excelView["pfevTitleLineHeight"]); // 行高
        $objPHPExcel->getActiveSheet()
        ->getRowDimension('2')
        ->setRowHeight($excelView["pfevNavLineHeight"]); // 行高
        for ($i = 3; $i < ($dataNum + 3); $i ++) {
            $objPHPExcel->getActiveSheet()
            ->getRowDimension($i)
            ->setRowHeight($excelView["pfevCLineHeight"]); // 行高
        }
        $objPHPExcel->getActiveSheet()
        ->getColumnDimension('A1')
        ->setAutoSize(true); // 列宽
        for ($j = 65; $j < 94; $j ++) {
            $newj = chr($j);
            $objPHPExcel->getActiveSheet()
            ->getColumnDimension($newj)
            ->setWidth($excelView["pfevLWidth"]); // 列宽
        }
        // 设置表格边框
        if ($excelView["pfevBorder"] == 1) {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A2:Z' . ($dataNum + 2))
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
        }
        // 设置填充颜色
        $titlebgcolor = ltrim($excelView["pfevTitleBgColor"], "#");
        $navbgcolor = ltrim($excelView["pfevNavBgColor"], "#");
        $conbgcolor = ltrim($excelView["pfevCBgColor"], "#");
        $objPHPExcel->getActiveSheet()
        ->getStyle('A1')
        ->getFill()
        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A1')
        ->getFill()
        ->getStartColor()
        ->setARGB('FF' . $titlebgcolor);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A2:Z2')
        ->getFill()
        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A2:Z2')
        ->getFill()
        ->getStartColor()
        ->setARGB('FF' . $navbgcolor);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A3:Z' . ($dataNum + 2))
        ->getFill()
        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A3:Z' . ($dataNum + 2))
        ->getFill()
        ->getStartColor()
        ->setARGB('FF' . $conbgcolor);
    
        // 设置工作表的名称
        $objPHPExcel->getActiveSheet()->setTitle($excelView["pfevNav"]);
        // 设置水平居中
        if ($excelView["pfevTitleAlign"] == "LEFT") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A1')
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        } elseif ($excelView["pfevTitleAlign"] == "CENTER") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A1')
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        } elseif ($excelView["pfevTitleAlign"] == "RIGHT") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A1')
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        }
        if ($excelView["pfevNavAlign"] == "LEFT") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A2:Z2')
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        } elseif ($excelView["pfevNavAlign"] == "CENTER") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A2:Z2')
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        } elseif ($excelView["pfevNavAlign"] == "RIGHT") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A2:Z2')
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        }
        if ($excelView["pfevCAlign"] == "LEFT") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A3:Z' . ($dataNum + 2))
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        } elseif ($excelView["pfevCAlign"] == "CENTER") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A3:Z' . ($dataNum + 2))
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        } elseif ($excelView["pfevCAlign"] == "RIGHT") {
            $objPHPExcel->getActiveSheet()
            ->getStyle('A3:Z' . ($dataNum + 2))
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        }
        // 垂直居中
        $objPHPExcel->getActiveSheet()
        ->getStyle('A1')
        ->getAlignment()
        ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A2:Z2')
        ->getAlignment()
        ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet()
        ->getStyle('A3:Z' . ($dataNum + 2))
        ->getAlignment()
        ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle . '  Export time:' . date('Y-m-d H:i:s'));
        for ($i = 0; $i < $cellNum; $i ++) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i] . '2', $expCellName[$i][1]);
        }
        // Miscellaneous glyphs, UTF-8
        for ($i = 0; $i < $dataNum; $i ++) {
            for ($j = 0; $j < $cellNum; $j ++) {
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j] . ($i + 3), $expTableData[$i][$expCellName[$j][0]]);
            }
        }
        header('pragma:public');
        header('Content-type:application/vnd.ms-excel;charset=utf-8;name="' . $xlsTitle . '.xls"');
        header("Content-Disposition:attachment;filename=$fileName.xls"); // attachment新窗口打印inline本窗口打印
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }
    
    function expstaff()
    { // 导出Excel
    $expType = $_POST["expType"];
    if ($expType == 2) {
        $expStartNum = $_POST["expStartNum"];
        $expNumber = $_POST["expNumber"];
    } elseif ($expType == 3) {
        $pmenu = $_POST["pmenu"];
        if ($pmenu > 0) {
            $product_menu = M("department");
            $rs_pmenu = $product_menu->field("dId,dName")
            ->where("dId=$pmenu")
            ->find();
            $rspmName = $rs_pmenu["dName"];
        }
    }
    $escType = $_POST["escType"];
    $excelView = $_POST["excelView"];
    $xlsCell = array(
        array(
            'stId',
            'ID'
        ),
        array(
            'stNum',
            '员工编号'
        ),
        array(
            'stName',
            '姓名'
        ),
        array(
            'stSex',
            '性别'
        ),
        array(
            'stBirthdateType',
            '公历/农历'
        ),
        array(
            'stBirthdate',
            '出生日期'
        ),
        array(
            'stMarital',
            '婚姻状况'
        ),
        array(
            'stTel',
            '电话'
        ),
        array(
            'stTelT',
            '手机'
        ),
        array(
            'stAddress',
            '现住址'
        ),
        array(
            'stDid',
            '所在部门ID'
        ),
        array(
            'stDegrees',
            '学历'
        ),
        array(
            'stHeight',
            '身高'
        ),
        array(
            'stHealth',
            '身体状况'
        ),
        array(
            'stIDCard',
            '身份证号码'
        ),
        array(
            'stNativePlace',
            '籍贯'
        ),
        array(
            'stCity',
            '家庭住址'
        ),
        array(
            'stMultiracial',
            '民族'
        ),
        array(
            'stPoliticalIandscape',
            '政治面貌'
        ),
        array(
            'stEntryDate',
            '入职日期'
        ),
        array(
            'stDuties',
            '职务'
        ),
        array(
            'stPositionalTitles',
            '职称'
        ),
        array(
            'stQQ',
            'QQ'
        ),
        array(
            'stEmail',
            '邮箱'
        ),
        array(
            'stJingyuan',
            '工作经验'
        ),
        array(
            'stJineng',
            '掌握技能'
        )
        /*
        array(
            'stJobState',
            '在职状态'
        ),
        array(
            'stDepartureDate',
            '离职日期'
        ),
        array(
            'stPhoto',
            '照片'
        )
        */
    );
    $xlsModel = M('staff');
    if ($expType == 1) {
        if ($escType == 1) {
            $esc = "stId asc";
        } else {
            $esc = "stId desc";
        }
        $xlsData = $xlsModel->order($esc)->select();
    } elseif ($expType == 2) {
        if ($escType == 1) {
            $esc = "stId asc";
        } else {
            $esc = "stId desc";
        }
        $xlsData = $xlsModel->limit($expStartNum, $expNumber)
        ->order($esc)
        ->select();
    } elseif ($expType == 3) {
        if ($escType == 1) {
            $esc = "stId asc";
        } else {
            $esc = "stId desc";
        }
        if ($pmenu == 0) {
            $xlsData = $xlsModel->order($esc)->select();
        } else {
            $xlsData = $xlsModel->where("stId=$pmenu")
            ->order($esc)
            ->select();
            if (count($xlsData) == 0) {
                $this->error("该类目暂无数据");
            }
        }
    }
    foreach ($xlsData as $k => $v) {}
    $this->exportExcel($excelView, $rspmName, $xlsCell, $xlsData);
    }
    
    // 导入
    public function import()
    {
        $this->LoginTrue();
        $department = M("department");
        $rs_department = $department->field("dId,dPid,dPsid,dName")->select();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }
    
    /**
     * 实现导入excel
     */
    function impstaff()
    {
        if (! empty($_FILES)) {
            $upload = new \Think\Upload(); // 实例化上传类
            $upload->maxSize = 3145728; // 设置附件上传大小
            $upload->exts = array(
                'xls',
                'xlsx'
            ); // 设置附件上传类型
            $rootPath = $upload->rootPath = './'; // 设置附件上传根目录
            $upload->savePath = 'Uploads/staff/files/'; // 设置附件上传（子）目录
            $upload->subName = array(
                'date',
                'Ymd'
            );
            // 上传文件
            $infoss = $upload->upload();
            if (! $infoss) { // 上传错误提示错误信息
                $this->error($upload->getError());
            }
            foreach ($infoss as $file) {
                $infomy = $file['savepath'] . $file['savename'];
            }
            import("Org.Util.PHPExcel");
            $PHPExcel = new \PHPExcel();
            // vendor("PHPExcel.PHPExcel");
            $file_name = $infomy;
            $objReader = \PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($file_name, $encode = 'utf-8');
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow(); // 取得总行数
            $highestColumn = $sheet->getHighestColumn(); // 取得总列数
            for ($i = 3; $i <= $highestRow; $i ++) {
                $data['stNum'] = $objPHPExcel->getActiveSheet()
                ->getCell("B" . $i)
                ->getValue();
                if ($data['stNum'] == "") {
                    $data['stNum'] = 100;
                }
                $data['stName'] = $objPHPExcel->getActiveSheet()
                ->getCell("C" . $i)
                ->getValue();
                if ($data['stName'] == "") {
                    $data['stName'] = "测试";
                }
               
                
                $data['stSex'] = $objPHPExcel->getActiveSheet()
                ->getCell("D" . $i)
                ->getValue();
                if ($data['stSex'] == "") {
                    $data['stSex'] = 1;
                }
                $data['stBirthdateType'] = $objPHPExcel->getActiveSheet()
                ->getCell("E" . $i)
                ->getValue();
                if ($data['stBirthdateType'] == "") {
                    $data['stBirthdateType'] = 1;
                }
                $data['stBirthdate'] = $objPHPExcel->getActiveSheet()
                ->getCell("F" . $i)
                ->getValue();
                
                
                $data['stMarital'] = $objPHPExcel->getActiveSheet()
                ->getCell("G" . $i)
                ->getValue();
                if ($data['stMarital'] == "") {
                    $data['stMarital'] = "0";
                }
                
                
                $data['stTel'] = $objPHPExcel->getActiveSheet()
                ->getCell("H" . $i)
                ->getValue();
                if ($data['stTel'] == "") {
                    $data['stTel'] = "暂未填写";
                }
                $data['stTelT'] = $objPHPExcel->getActiveSheet()
                ->getCell("I" . $i)
                ->getValue();
                if ($data['stTelT'] == "") {
                    $data['stTelT'] ="未填写";
                }
                
                $data['stAddress'] = $objPHPExcel->getActiveSheet()
                ->getCell("J" . $i)
                ->getValue();
                if ($data['stAddress'] == "") {
                    $data['stAddress'] = "未填写";
                }
                
                $data['stDid'] = $objPHPExcel->getActiveSheet()
                ->getCell("K" . $i)
                ->getValue();
                if ($data['stDid'] == "") {
                    $data['stDid'] = $_POST["stDid"];
                }
               
                $data['stDegrees'] = $objPHPExcel->getActiveSheet()
                ->getCell("L" . $i)
                ->getValue();
                if ($data['stDegrees'] == "") {
                    $data['stDegrees'] = 1;
                }
                $data['stHeight'] = $objPHPExcel->getActiveSheet()
                ->getCell("M" . $i)
                ->getValue();
                if ($data['stHeight'] == "") {
                    $data['stHeight'] = "170";
                }
                $data['stHealth'] = $objPHPExcel->getActiveSheet()
                ->getCell("N" . $i)
                ->getValue();
                if ($data['stHealth'] == "") {
                    $data['stHealth'] = "身体健康";
                }
                $data['stIDCard'] = $objPHPExcel->getActiveSheet()
                ->getCell("O" . $i)
                ->getValue();
                if ($data['stIDCard'] == "") {
                    $data['stIDCard'] = "暂未填写";
                }
    
                $data['stNativePlace'] = $objPHPExcel->getActiveSheet()
                ->getCell("P" . $i)
                ->getValue();
                if ($data['stNativePlace'] == "") {
                    $data['stNativePlace'] = "未填写";
                }
                $data['stCity'] = $objPHPExcel->getActiveSheet()
                ->getCell("Q" . $i)
                ->getValue();
                if ($data['stCity'] == "") {
                    $data['stCity'] = "未填写";
                }
                $data['stMultiracial'] = $objPHPExcel->getActiveSheet()
                ->getCell("R" . $i)
                ->getValue();
                if ($data['stMultiracial'] == "") {
                    $data['stMultiracial'] = "未填写";
                }
                $data['stPoliticalIandscape'] = $objPHPExcel->getActiveSheet()
                ->getCell("S" . $i)
                ->getValue();
                if ($data['stPoliticalIandscape'] == "") {
                    $data['stPoliticalIandscape'] = "未填写";
                }
                $data['stEntryDate'] = $objPHPExcel->getActiveSheet()
                ->getCell("T" . $i)
                ->getValue();
                if ($data['stEntryDate'] == "") {
                    $data['stEntryDate'] = date("Y-m-d");
                }
                $data['stDuties'] = $objPHPExcel->getActiveSheet()
                ->getCell("U" . $i)
                ->getValue();
                if ($data['stDuties'] == "") {
                    $data['stDuties'] = "1";
                }
                $data['stPositionalTitles'] = $objPHPExcel->getActiveSheet()
                ->getCell("V" . $i)
                ->getValue();
                if ($data['stPositionalTitles'] == "") {
                    $data['stPositionalTitles'] = "1";
                }
    
                $data['stQQ'] = $objPHPExcel->getActiveSheet()
                ->getCell("W" . $i)
                ->getValue();
                if ($data['stQQ'] == "") {
                    $data['stQQ'] = "0";
                }
                $data['stEmail'] = $objPHPExcel->getActiveSheet()
                ->getCell("X" . $i)
                ->getValue();
                if ($data['stEmail'] == "") {
                    $data['stEmail'] = "0";
                }
                $data['stJingyuan'] = $objPHPExcel->getActiveSheet()
                ->getCell("Y" . $i)
                ->getValue();
                if ($data['stJingyuan'] == "") {
                    $data['stJingyuan'] = "未填写";
                }
                $data['stJineng'] = $objPHPExcel->getActiveSheet()
                ->getCell("Z" . $i)
                ->getValue();
                if ($data['stJineng'] == "") {
                    $data['stJineng'] = "未填写";
                }
                
                $staff = M('staff');
                $result = $staff->add($data);
            }
            if ($result) {
                $this->success('导入成功！');
            } else {
                $this->error("填写完整后再导入吧！");
            }
        } else {
            $this->error("请选择上传的文件");
        }
    }
}