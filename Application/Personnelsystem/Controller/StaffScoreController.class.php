<?php
namespace Personnelsystem\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class StaffScoreController extends LoginTrueController
{
    public function index(){
        $this->LoginTrue();
        $staff=M("staff");
        $rs_staff=$staff->where("stJobState=1")->select();
        $this->assign("rs_staff",$rs_staff);
        $nowMonth=date("m");
        $this->assign("nowMonth",$nowMonth);
        $this->display();
    }
    public function add(){
        $this->LoginTrue();
        $stId=$_POST["id"];
        $score=$_POST["score"];
        $data["stMonthValue"]=date("m");
        $staff=M("staff");
        $data["stMScore"]=$score;
		$ym=date("Y-m");
		$datahistory["hirsStId"]=$stId;
		$datahistory["hirsScore"]=$score;
		$datahistory["hirsMonthScore"]=date("Y-m-d");
        $result=$staff->where("stId={$stId}")->save($data);
        $historyrecord_score=M("historyrecord_score");
        $map['hirsMonthScore'] =array('like',array($ym.'%'),'AND');
        $map['hirsStId']=$stId;
        $rs_history=$historyrecord_score->where($map)->field("hirsId")->find();
		$hirsId=$rs_history["hirsId"];
        if($hirsId>0){
            $resulthistory=$historyrecord_score->where("hirsId={$hirsId}")->save($datahistory);
        }else{
            $resulthistory=$historyrecord_score->add($datahistory);
        }
        if($result && $resulthistory){
            $this->success("成功",U("index"));
        }
        //$this->display();
    }
    public function setScore(){
        $this->LoginTrue();
        $stgId=$_GET["stgId"];
        $nowMonth=date("m");
        $this->assign("nowMonth",$nowMonth);
        $staff=M("staff");
        $department = M("department");
        $rs_department=$department->select();
        $this->assign("rs_department",$rs_department);
        $rs_staff=$staff->where("stId={$stgId}")->find();
        $this->assign("rs_staff",$rs_staff);
        // 学历的处理
        $variables = M("variables");
        $rs_xueli = $variables->where("vId=1")->find();
        $xueli = explode("|", $rs_xueli["vVariablesVal"]);
        $xueliId=$rs_staff["stDegrees"];
        $xueliInfo=$xueli[$xueliId];
        $this->assign("xueliInfo", $xueliInfo);
        //职称处理
        $rs_zhicheng = $variables->where("vId=2")->find();
        $zhicheng = explode("|", $rs_zhicheng["vVariablesVal"]);
        $zhichengId=$rs_staff["stPositionalTitles"];
        $zhichengInfo=$zhicheng[$zhichengId];
        $this->assign("zhichengInfo", $zhichengInfo);
        //职务处理
        $rs_zhiwu = $variables->where("vId=3")->find();
        $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]);
        $zhiwuId=$rs_staff["stDuties"];
        $zhiwuInfo=$zhiwu[$zhiwuId];
        $this->assign("zhiwuInfo", $zhiwuInfo);
        $this->display();
    }
    public function setScoreAction(){
        $this->LoginTrue();
        $stId=$_GET["stId"];
        $data["stMScore"]=$_POST["stMScore"];
        $data["stMonthValue"]=date("m");
        $staff=M("staff");
        $result=$staff->where("stId={$stId}")->save($data);
		$ym=date("Y-m");
		$datahistory["hirsStId"]=$stId;
		$datahistory["hirsScore"]=$data["stMScore"];
		$datahistory["hirsMonthScore"]=date("Y-m-d");
		$historyrecord_score=M("historyrecord_score");
        $map['hirsMonthScore'] =array('like',array($ym.'%'),'AND');
        $map['hirsStId']=$stId;
        $rs_history=$historyrecord_score->where($map)->field("hirsId")->find();
		$hirsId=$rs_history["hirsId"];
        if($hirsId>0){
            $resulthistory=$historyrecord_score->where("hirsId={$hirsId}")->save($datahistory);
        }else{
            $resulthistory=$historyrecord_score->add($datahistory);
        }
        if($result && $resulthistory){
            $this->success("评分成功",U("index"));
        }else{
            $this->error("评分失败");
        }
    }
    public function allScore(){
        $this->LoginTrue();
        $historyrecord_score=M("historyrecord_score");
        $rs_h=$historyrecord_score->select();
        $this->assign("rs_h",$rs_h);
        $this->display();
    }
}