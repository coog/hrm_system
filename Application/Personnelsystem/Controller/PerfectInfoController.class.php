<?php
namespace Personnelsystem\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class PerfectInfoController extends LoginTrueController
{
    public function UpdateCV()
    {
        $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_hrreserve=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        // 学历的处理
        $variables = M("variables");
        $rs_xueli = $variables->where("vId=1")->find();
        $xueli = explode("|", $rs_xueli["vVariablesVal"]);
        $this->assign("xueli", $xueli);
        
        //职务的处理【应聘岗位】
        $rs_zhiwu = $variables->where("vId=3")->find();
        $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]);
        $this->assign("zhiwu", $zhiwu);
        $this->display();
    }

    public function UpdateCVAction()
    {
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $data["hrName"]=$_POST["hrName"];
        $data["hrSex"]=$_POST["hrSex"];
        $data["hrDegrees"]=$_POST["hrDegrees"];
        $data["hrHeight"]=$_POST["hrHeight"];
        $data["hrBirthdateType"]=$_POST["hrBirthdateType"];
        $data["hrBirthdate"]=$_POST["hrBirthdate"];
        $data["hrTel"]=$_POST["hrTel"];
        $data["hrMultiracial"]=$_POST["hrMultiracial"];
        $data["hrNativePlace"]=$_POST["hrNativePlace"];
        $data["hrEmail"]=$_POST["hrEmail"];
        $data["hrDuties"]=$_POST["hrDuties"];
        $data["hrSalary"]=$_POST["hrSalary"];
        $data["hrGengxingDate"] = date("Y-m-d H:i:s");
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
            $this->success("保存成功",U("updatecvtwo"));
        }else{
            $this->error("你没有做任何修改",U("updatecvtwo"));
        }
    }
    public function UpdateCVTwo()
    {
        $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_hrreserve=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        $this->display();
    }
    
    public function UpdateCVTwoAction()
    {
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $data["hrWeight"]=$_POST["hrWeight"];
        $data["hrQQ"]=$_POST["hrQQ"];
        $data["hrCity"]=$_POST["hrCity"];
        $data["hrIDCard"]=$_POST["hrIDCard"];
        $data["hrPoliticalIandscape"]=$_POST["hrPoliticalIandscape"];
        $data["hrEdu"]=$_POST["hrEdu"];
        $data["hrJineng"]=$_POST["hrJineng"];
        $data["hrWorks"]=$_POST["hrWorks"];
        $data["hrPingjia"]=$_POST["hrPingjia"];
        $data["hrRemarks"]=$_POST["hrRemarks"];
        $data["hrGengxingDate"] = date("Y-m-d H:i:s");
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
            $this->success("保存成功",U("updatecvthree"));
        }else{
            $this->error("你没有做任何修改",U("updatecvthree"));
        }
    }
    
    public function UpdateCVThree()
    {
        $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_hrreserve=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        $this->display();
    }
    
    public function UpdateCVThreeAction()
    {
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $data["hrPhoto"]=$_POST["hrPhoto"];
        $data["hrEnclosure"]=$_POST["hrEnclosure"];
        $data["hrShenfenzhengImages"]=$_POST["hrShenfenzhengImages"];
        $data["hrGengxingDate"] = date("Y-m-d H:i:s");
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
            $this->success("保存成功");
        }else{
            $this->error("你没有做任何修改");
        }
    }

    public function up()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Perfect/photo/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }
    
    public function upfujian()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Perfect/fujian/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }
    
    public function upsfz()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Perfect/shenfenzheng/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }
    
    //修改账户中的密码
    public function userPwd(){
        $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_userInfo=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_userInfo",$rs_userInfo);
        $this->display();
    }
    public function userPwdAction(){
        $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $rs_userInfo=$hrreserve->where("hrId={$hrId}")->field("hrPwd")->find();
        $hrPwd=$_POST["hrPwd"];
        if(md5($hrPwd)!=$rs_userInfo["hrPwd"]){
           $this->error("原密码错误，无法修改"); 
        }
        $pwd=$_POST["pwd"];
        $pwd2=$_POST["pwd2"];
        if($pwd!=$pwd2){
            $this->error("两次输入的密码不一致，请重新输入");
        }
        if($hrPwd==$pwd){
            $this->error("新密码不能和原密码一致");
        }
        $data["hrPwd"]=md5($_POST["pwd"]);
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
           $this->success("修改密码成功"); 
        }else{
            $this->error("修改密码失败");
        }
    }


    
    /**---------------------------------------------------------------------------
     * 简历数据填写页面显示
     * @return [type] [description]
     */
    public function addmain(){
        // 学历的处理
        $variables = M("variables");
        $rs_xueli = $variables->where("vId=1")->find();
        $xueli = explode("|", $rs_xueli["vVariablesVal"]);
        $this->assign("xueli", $xueli);
        $this->display();
                
    }

    /**
     * [简历数据添加操作]
     */
    public function add_action(){
        if(IS_POST){
            $add_data = I('post.');
            $hrNum = date('Ymd',time()).rand(1,100);
            
            $data_info = [
                'hrName'                 => $add_data['hrName'],//姓名
                'hrSex'                  => $add_data['hrSex'],//性别
                'hrDegrees'              => $add_data['hrDegrees'],//学历
                'hrBirthdate'            => $add_data['hrBirthdate'],//出生日期
                'hrEntryDate'            => $add_data['hrEntryDate'],//到岗日期
                'hrTel'                  => $add_data['hrTel'],//联系电话
                'hrHeight'               => $add_data['hrHeight'],//身高
                'hrMultiracial'          => $add_data['hrMultiracial'],//民族
                'hrNativePlace'          => $add_data['hrNativePlace'],//籍贯
                'hrPosition'             => $add_data['hrPosition'],//应聘岗位
                'hrIsMarriage'           => $add_data['hrIsMarriage'],//婚否
                'hrSalary'               => $add_data['hrSalary'],//薪资
                'hrCity'                 => $add_data['hrCity'],//住址
                'hrEmail'                => $add_data['hrEmail'],//邮件
                'hrWeight'               => $add_data['hrWeight'],//体重
                'hrIDCard'               => $add_data['hrIDCard'],//身份证
                'hrPoliticalIandscape'   => $add_data['hrPoliticalIandscape'],//政治面貌
                'hrWorkArea'             => $add_data['hrWorkArea'],//工作地区
                'hrRecruitment'          => $add_data['hrRecruitment'],//应聘渠道
                'hrRecruitment6'         => $add_data['hrRecruitment6'],//应聘渠道
                'hrRecruitment7'         => $add_data['hrRecruitment7'],//应聘渠道
                'hrLanguage'             => $add_data['hrLanguage'],//语言技能
                'hrLicense'              => $add_data['hrLicense'],//驾照
                'hrSkillCertificate'     => $add_data['hrSkillCertificate'],//技能证书
                'hrJineng'               => $add_data['hrJineng'],//掌握技能
                'hrContactName'          => $add_data['hrContactName'],//紧急联系人
                'hrContactRelationship'  => $add_data['hrContactRelationship'],//关系
                'hrContactTel'           => $add_data['hrContactTel'],//联系方式
                'hrInputDate'            => date('Y-m-d H:i:s',time()),//简历创建时间 
                
                //家庭情况
                'hrFamilyMember'         => $add_data['hrFamilyMember'],
                'hrFamilyRelationship'   => $add_data['hrFamilyRelationship'],
                'hrFamilyUnit'           => $add_data['hrFamilyUnit'],
                'hrFamilyCareer'         => $add_data['hrFamilyCareer'],
                'hrFamilyTel'            => $add_data['hrFamilyTel'],
                //家庭情况二
                'hrFamilyMember1'        => isset($add_data['hrFamilyMember1']) ? $add_data['hrFamilyMember1']:'',
                'hrFamilyRelationship1'  => isset($add_data['hrFamilyRelationship1']) ? $add_data['hrFamilyRelationship1']:'',
                'hrFamilyUnit1'          => isset($add_data['hrFamilyUnit1']) ? $add_data['hrFamilyUnit1']:'',
                'hrFamilyCareer1'        => isset($add_data['hrFamilyCareer1']) ? $add_data['hrFamilyCareer1']:'',
                'hrFamilyTel1'           => isset($add_data['hrFamilyTel1']) ? $add_data['hrFamilyTel1']:'',
                //家庭情况三
                'hrFamilyMember2'        => isset($add_data['hrFamilyMember2']) ? $add_data['hrFamilyMember2']:'',
                'hrFamilyRelationship2'  => isset($add_data['hrFamilyRelationship2']) ? $add_data['hrFamilyRelationship2']:'',
                'hrFamilyUnit2'          => isset($add_data['hrFamilyUnit2']) ? $add_data['hrFamilyUnit2']:'',
                'hrFamilyCareer2'        => isset($add_data['hrFamilyCareer2']) ? $add_data['hrFamilyCareer2']:'',
                'hrFamilyTel2'           => isset($add_data['hrFamilyTel2']) ? $add_data['hrFamilyTel2']:'',
                
                //教育背景
                'hrEduDate'              => $add_data['hrEduDate'],
                'hrEduSchool'            => $add_data['hrEduSchool'],
                'hrEduProfession'        => $add_data['hrEduProfession'],
                'hrEduEducation'         => $add_data['hrEduEducation'],
                'hrEduCertificate'       => $add_data['hrEduCertificate'],
                //教育背景
                'hrEduDate1'             => isset($add_data['hrEduDate1']) ? $add_data['hrEduDate1']:'',
                'hrEduSchool1'           => isset($add_data['hrEduSchool1']) ? $add_data['hrEduSchool1']:'',
                'hrEduProfession1'       => isset($add_data['hrEduProfession1']) ? $add_data['hrEduProfession1']:'',
                'hrEduEducation1'        => isset($add_data['hrEduEducation1']) ? $add_data['hrEduEducation1']:'',
                'hrEduCertificate1'      => isset($add_data['hrEduCertificate1']) ? $add_data['hrEduCertificate1']:'',
                //教育背景
                'hrEduDate2'             => isset($add_data['hrEduDate2']) ? $add_data['hrEduDate2']:'',
                'hrEduSchool2'           => isset($add_data['hrEduSchool2']) ? $add_data['hrEduSchool2']:'',
                'hrEduProfession2'       => isset($add_data['hrEduProfession2']) ? $add_data['hrEduProfession2']:'',
                'hrEduEducation2'        => isset($add_data['hrEduEducation2']) ? $add_data['hrEduEducation2']:'',
                'hrEduCertificate2'      => isset($add_data['hrEduCertificate2']) ? $add_data['hrEduCertificate2']:'',
                
                //培训
                'hrTrainingDate'         => $add_data['hrTrainingDate'],
                'hrTrainingMechanism'    => $add_data['hrTrainingMechanism'],
                'hrTrainingCourse'       => $add_data['hrTrainingCourse'],
                'hrTrainingCertificate'  => $add_data['hrTrainingCertificate'],
                //培训二
                'hrTrainingDate1'        => isset($add_data['hrTrainingDate1']) ? $add_data['hrTrainingDate1']:'',
                'hrTrainingMechanism1'   => isset($add_data['hrTrainingMechanism1']) ? $add_data['hrTrainingMechanism1']:'',
                'hrTrainingCourse1'      => isset($add_data['hrTrainingCourse1']) ? $add_data['hrTrainingCourse1']:'',
                'hrTrainingCertificate1' => isset($add_data['hrTrainingCertificate1']) ? $add_data['hrTrainingCertificate1']:'',

                //工作经历
                'hrWorksDate'            => $add_data['hrWorksDate'],
                'hrWorksUnit'            => $add_data['hrWorksUnit'],
                'hrWorksPosition'        => $add_data['hrWorksPosition'],
                'hrWorksSalary'          => $add_data['hrWorksSalary'],
                'hrWorksReason'          => $add_data['hrWorksReason'],
                'hrWorksWitness'         => $add_data['hrWorksWitness'],
                //工作经历二
                'hrWorksDate1'           => isset($add_data['hrWorksDate1']) ? $add_data['hrWorksDate1']:'',
                'hrWorksUnit1'           => isset($add_data['hrWorksUnit1']) ? $add_data['hrWorksUnit1']:'',
                'hrWorksPosition1'       => isset($add_data['hrWorksPosition1']) ? $add_data['hrWorksPosition1']:'',
                'hrWorksSalary1'         => isset($add_data['hrWorksSalary1']) ? $add_data['hrWorksSalary1']:'',
                'hrWorksReason1'         => isset($add_data['hrWorksReason1']) ? $add_data['hrWorksReason1']:'',
                'hrWorksWitness1'        => isset($add_data['hrWorksWitness1']) ? $add_data['hrWorksWitness1']:'',
                //工作经历三
                'hrWorksDate2'           => isset($add_data['hrWorksDate2']) ? $add_data['hrWorksDate2']:'',
                'hrWorksUnit2'           => isset($add_data['hrWorksUnit2']) ? $add_data['hrWorksUnit2']:'',
                'hrWorksPosition2'       => isset($add_data['hrWorksPosition2']) ? $add_data['hrWorksPosition2']:'',
                'hrWorksSalary2'         => isset($add_data['hrWorksSalary2']) ? $add_data['hrWorksSalary2']:'',
                'hrWorksReason2'         => isset($add_data['hrWorksReason2']) ? $add_data['hrWorksReason2']:'',
                'hrWorksWitness2'        => isset($add_data['hrWorksWitness2']) ? $add_data['hrWorksWitness2']:'',        
                
                'hrPhoto'                => $add_data['hrPhoto'],        
                'hrShenfenzhengImages'   => $add_data['hrShenfenzhengImages'],        

            ];
            
            $table_hrreserve = M('hrreserve');
            $res = $table_hrreserve->add($data_info);
            if($res){
                cookie('hrTel',$data_info['hrTel'],3600*12);
                $this->success('添加成功',U('Hrinfo/index'));
            }else{
                $this->error('添加失败');
                
            }
        }

    }

    /**
     * 验证输入的手机号码是否存在
     * @return [type] [description]
     */
    public function verifyTel(){
        $table_hrreserve = M('hrreserve');
        $tel = I('param.hrTel');
        $res = $table_hrreserve->where("hrTel='{$tel}'")->field('hrTel')->find();

        if($res){
            $this->ajaxReturn(['valid'=>1]);
        }else{
            $this->ajaxReturn(['valid'=>0]);
        }
    }
    

    /**
     * 修改输入的简历
     * @return [type] [description]
     */
    public function editmain(){
        $cid = I('get.cid');
        if($cid){
            $hrreserve = M("hrreserve");
            // 学历的处理
            $variables = M("variables");
            $rs_xueli = $variables->where("vId=1")->find();
            $xueli = explode("|", $rs_xueli["vVariablesVal"]);
            $this->assign("xueli", $xueli);
            $rs_index = $hrreserve->where("hrTel='{$cid}'")->find();
            $this->assign("rs_index", $rs_index);
            
            //算年龄
            $nowYear=date("Y");
            $age=explode("-",$rs_index["hrBirthdate"]);
            if($age[0]!=0000){
               $nowAge=$nowYear-$age[0];
               if($nowAge>120){
                   $nowAge=0;
               }
            }else{
               $nowAge=0;
            }
            $this->assign("nowAge",$nowAge);

            $this->display();
        }else{
            $this->display('Index/addmain');
        }
        
    }

    /**
     * 编辑提交操作
     */
    public function edit_action(){
        $edit_data = I('param.');
        $hrreserve = M("hrreserve");
        if(IS_POST){
            $edit_info = [
                'hrName'                 => $edit_data['hrName'],//姓名
                'hrSex'                  => $edit_data['hrSex'],//性别
                'hrDegrees'              => $edit_data['hrDegrees'],//学历
                'hrBirthdate'            => $edit_data['hrBirthdate'],//出生日期
                'hrEntryDate'            => $edit_data['hrEntryDate'],//到岗日期
                'hrTel'                  => $edit_data['hrTel'],//联系电话
                'hrHeight'               => $edit_data['hrHeight'],//身高
                'hrMultiracial'          => $edit_data['hrMultiracial'],//民族
                'hrNativePlace'          => $edit_data['hrNativePlace'],//籍贯
                'hrPosition'             => $edit_data['hrPosition'],//应聘岗位
                'hrIsMarriage'           => $edit_data['hrIsMarriage'],//婚否
                'hrSalary'               => isset($edit_data['hrSalary']) ? $edit_data['hrSalary']:'',//薪资
                'hrCity'                 => $edit_data['hrCity'],//住址
                'hrEmail'                => $edit_data['hrEmail'],//邮件
                'hrWeight'               => $edit_data['hrWeight'],//体重
                'hrIDCard'               => $edit_data['hrIDCard'],//身份证
                'hrPoliticalIandscape'   => $edit_data['hrPoliticalIandscape'],//政治面貌
                'hrWorkArea'             => $edit_data['hrWorkArea'],//工作地区
                'hrRecruitment'          => $edit_data['hrRecruitment'],//应聘渠道
                'hrRecruitment6'         => $edit_data['hrRecruitment6'],//应聘渠道
                'hrRecruitment7'         => $edit_data['hrRecruitment7'],//应聘渠道
                'hrLanguage'             => $edit_data['hrLanguage'],//语言技能
                'hrLicense'              => $edit_data['hrLicense'],//驾照
                'hrSkillCertificate'     => $edit_data['hrSkillCertificate'],//技能证书
                'hrJineng'               => $edit_data['hrJineng'],//掌握技能
                'hrContactName'          => $edit_data['hrContactName'],//紧急联系人
                'hrContactRelationship'  => $edit_data['hrContactRelationship'],//关系
                'hrContactTel'           => $edit_data['hrContactTel'],//联系方式
                'hrInputDate'            => date('Y-m-d H:i:s',time()),//简历创建时间               

                //家庭情况
                'hrFamilyMember'         => $edit_data['hrFamilyMember'],
                'hrFamilyRelationship'   => $edit_data['hrFamilyRelationship'],
                'hrFamilyUnit'           => $edit_data['hrFamilyUnit'],
                'hrFamilyCareer'         => $edit_data['hrFamilyCareer'],
                'hrFamilyTel'            => $edit_data['hrFamilyTel'],
                //家庭情况二
                'hrFamilyMember1'        => isset($edit_data['hrFamilyMember1']) ? $edit_data['hrFamilyMember1']:'',
                'hrFamilyRelationship1'  => isset($edit_data['hrFamilyRelationship1']) ? $edit_data['hrFamilyRelationship1']:'',
                'hrFamilyUnit1'          => isset($edit_data['hrFamilyUnit1']) ? $edit_data['hrFamilyUnit1']:'',
                'hrFamilyCareer1'        => isset($edit_data['hrFamilyCareer1']) ? $edit_data['hrFamilyCareer1']:'',
                'hrFamilyTel1'           => isset($edit_data['hrFamilyTel1']) ? $edit_data['hrFamilyTel1']:'',
                //家庭情况三
                'hrFamilyMember2'        => isset($edit_data['hrFamilyMember2']) ? $edit_data['hrFamilyMember2']:'',
                'hrFamilyRelationship2'  => isset($edit_data['hrFamilyRelationship2']) ? $edit_data['hrFamilyRelationship2']:'',
                'hrFamilyUnit2'          => isset($edit_data['hrFamilyUnit2']) ? $edit_data['hrFamilyUnit2']:'',
                'hrFamilyCareer2'        => isset($edit_data['hrFamilyCareer2']) ? $edit_data['hrFamilyCareer2']:'',
                'hrFamilyTel2'           => isset($edit_data['hrFamilyTel2']) ? $edit_data['hrFamilyTel2']:'',
                
                //教育背景
                'hrEduDate'              => $edit_data['hrEduDate'],
                'hrEduSchool'            => $edit_data['hrEduSchool'],
                'hrEduProfession'        => $edit_data['hrEduProfession'],
                'hrEduEducation'         => $edit_data['hrEduEducation'],
                'hrEduCertificate'       => $edit_data['hrEduCertificate'],
                //教育背景
                'hrEduDate1'             => isset($edit_data['hrEduDate1']) ? $edit_data['hrEduDate1']:'',
                'hrEduSchool1'           => isset($edit_data['hrEduSchool1']) ? $edit_data['hrEduSchool1']:'',
                'hrEduProfession1'       => isset($edit_data['hrEduProfession1']) ? $edit_data['hrEduProfession1']:'',
                'hrEduEducation1'        => isset($edit_data['hrEduEducation1']) ? $edit_data['hrEduEducation1']:'',
                'hrEduCertificate1'      => isset($edit_data['hrEduCertificate1']) ? $edit_data['hrEduCertificate1']:'',
                //教育背景
                'hrEduDate2'             => isset($edit_data['hrEduDate2']) ? $edit_data['hrEduDate2']:'',
                'hrEduSchool2'           => isset($edit_data['hrEduSchool2']) ? $edit_data['hrEduSchool2']:'',
                'hrEduProfession2'       => isset($edit_data['hrEduProfession2']) ? $edit_data['hrEduProfession2']:'',
                'hrEduEducation2'        => isset($edit_data['hrEduEducation2']) ? $edit_data['hrEduEducation2']:'',
                'hrEduCertificate2'      => isset($edit_data['hrEduCertificate2']) ? $edit_data['hrEduCertificate2']:'',
                
                //培训
                'hrTrainingDate'         => $edit_data['hrTrainingDate'],
                'hrTrainingMechanism'    => $edit_data['hrTrainingMechanism'],
                'hrTrainingCourse'       => $edit_data['hrTrainingCourse'],
                'hrTrainingCertificate'  => $edit_data['hrTrainingCertificate'],
                //培训二
                'hrTrainingDate1'        => isset($edit_data['hrTrainingDate1']) ? $edit_data['hrTrainingDate1']:'',
                'hrTrainingMechanism1'   => isset($edit_data['hrTrainingMechanism1']) ? $edit_data['hrTrainingMechanism1']:'',
                'hrTrainingCourse1'      => isset($edit_data['hrTrainingCourse1']) ? $edit_data['hrTrainingCourse1']:'',
                'hrTrainingCertificate1' => isset($edit_data['hrTrainingCertificate1']) ? $edit_data['hrTrainingCertificate1']:'',

                //工作经历
                'hrWorksDate'            => $edit_data['hrWorksDate'],
                'hrWorksUnit'            => $edit_data['hrWorksUnit'],
                'hrWorksPosition'        => $edit_data['hrWorksPosition'],
                'hrWorksSalary'          => $edit_data['hrWorksSalary'],
                'hrWorksReason'          => $edit_data['hrWorksReason'],
                'hrWorksWitness'         => $edit_data['hrWorksWitness'],
                //工作经历二
                'hrWorksDate1'           => isset($edit_data['hrWorksDate1']) ? $edit_data['hrWorksDate1']:'',
                'hrWorksUnit1'           => isset($edit_data['hrWorksUnit1']) ? $edit_data['hrWorksUnit1']:'',
                'hrWorksPosition1'       => isset($edit_data['hrWorksPosition1']) ? $edit_data['hrWorksPosition1']:'',
                'hrWorksSalary1'         => isset($edit_data['hrWorksSalary1']) ? $edit_data['hrWorksSalary1']:'',
                'hrWorksReason1'         => isset($edit_data['hrWorksReason1']) ? $edit_data['hrWorksReason1']:'',
                'hrWorksWitness1'        => isset($edit_data['hrWorksWitness1']) ? $edit_data['hrWorksWitness1']:'',
                //工作经历三
                'hrWorksDate2'           => isset($edit_data['hrWorksDate2']) ? $edit_data['hrWorksDate2']:'',
                'hrWorksUnit2'           => isset($edit_data['hrWorksUnit2']) ? $edit_data['hrWorksUnit2']:'',
                'hrWorksPosition2'       => isset($edit_data['hrWorksPosition2']) ? $edit_data['hrWorksPosition2']:'',
                'hrWorksSalary2'         => isset($edit_data['hrWorksSalary2']) ? $edit_data['hrWorksSalary2']:'',
                'hrWorksReason2'         => isset($edit_data['hrWorksReason2']) ? $edit_data['hrWorksReason2']:'',
                'hrWorksWitness2'        => isset($edit_data['hrWorksWitness2']) ? $edit_data['hrWorksWitness2']:'',        

                'hrPhoto'                => $edit_data['hrPhoto'], 
                'hrShenfenzhengImages'   => $edit_data['hrShenfenzhengImages'], 
            ];
            $cid = cookie('hrTel');
           
            if($cid){
                $res = $hrreserve->where("hrTel={$cid}")->save($edit_info);
                if($res){
                    $this->success('修改成功');
                }else{
                    $this->error('修改失败');
                }

            }
           
        }

    }


    
}

