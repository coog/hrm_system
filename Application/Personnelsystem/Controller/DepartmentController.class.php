<?php
namespace Personnelsystem\Controller;

use Think\Controller;
header("content-type:text/html;charset=utf-8");

class DepartmentController extends LoginTrueController
{

    public function Add()
    {
        $this->LoginTrue();
        $systemName = M("system");
        $rs_systemName = $systemName->field("sDepartment")
            ->where("sId=1")
            ->find();
        $this->assign("rs_systemName", $rs_systemName);
        $this->display();
    }

    public function AddAction()
    {
        $this->LoginTrue();
        $data["dName"] = $_POST["dName"];
        $data["dPid"] = $_POST["dPid"];
        if ($data["dPid"] == "") {
            $data["dPid"] = 0;
        }
        $data["dPsid"] = $_POST["dPsid"];
        if ($data["dPsid"] == "") {
            $data["dPsid"] = 0;
        }
        $data["dDirector"] = $_POST["dDirector"];
        if ($data["dDirector"] == "") {
            $data["dDirector"] = "暂未设定负责人";
        }
        $data["dDirectorTel"] = $_POST["dDirectorTel"];
        if ($data["dDirectorTel"] == "") {
            $data["dDirectorTel"] = "暂无完善";
        }
        $data["dDirectorQQ"] = $_POST["dDirectorQQ"];
        if ($data["dDirectorQQ"] == "") {
            $data["dDirectorQQ"] = "暂无完善";
        }
        $data["dDirectorEmail"] = $_POST["dDirectorEmail"];
        if ($data["dDirectorEmail"] == "") {
            $data["dDirectorEmail"] = "暂无完善";
        }
        $data["dInfo"] = $_POST["dInfo"];
        if ($data["dInfo"] == "") {
            $data["dInfo"] = "暂未填写";
        }
        $departmentAdd = M("department");
        $result = $departmentAdd->add($data);
        if ($result) {
            $this->success("添加成功", lists);
        } else {
            $this->error("添加失败");
        }
    }

    public function TwoAdd()
    {
        $this->LoginTrue();
        $departmentAdd = M("department");
        $rs_department = $departmentAdd->where("dPid=0 AND dPsid=0")->select();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function ThreeAdd()
    {
        $this->LoginTrue();
        $departmentAdd = M("department");
        $rs_department = $departmentAdd->where("dPid!=0 AND dPsid=0")->select();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    function random_color()
    {
        mt_srand((double) microtime() * 1000000);
        $c = '';
        while (strlen($c) < 6) {
            $c .= sprintf("%02X", mt_rand(0, 200));
        }
        return $c;
    }

    public function Lists()
    {
        $this->LoginTrue();
        $systemName = M("system");
        $departmentLists = M("department");
        $rs_departmentLists = $departmentLists->where("dPid=0 AND dPsid=0")->select();
        $this->assign("rs_departmentLists", $rs_departmentLists);
        
        $rs_departmentLists2 = $departmentLists->where("dPid!=0 AND dPsid=0")->select();
        $this->assign("rs_departmentLists2", $rs_departmentLists2);
        
        $rs_departmentLists3 = $departmentLists->where("dPid=0 AND dPsid!=0")->select();
        $this->assign("rs_departmentLists3", $rs_departmentLists3);
        // 颜色
        for ($i = 0; $i < 30; $i ++) {
            $colors[$i] = "#" . $this->random_color();
            $color = array(
                $colors
            );
        }
        $this->assign("color", $color);
        
        for ($i = 0; $i < 60; $i ++) {
            $colors1[$i] = "#" . $this->random_color();
            $color1 = array(
                $colors1
            );
        }
        $this->assign("color1", $color1);
        for ($i = 0; $i < 90; $i ++) {
            $colors2[$i] = "#" . $this->random_color();
            $color2 = array(
                $colors2
            );
        }
        $this->assign("color2", $color2);
        $this->display();
    }

    public function listsinfo()
    {
        $this->LoginTrue();
        $dId = $_GET["dId"];
        $staff = M("staff");
        $rs_staff = $staff->where("stDid={$dId}")
            ->field("stId,stName")
            ->select();
        $this->assign("rs_staff", $rs_staff);
        // 颜色
        for ($i = 0; $i < 800; $i ++) {
            $colors[$i] = "#" . $this->random_color();
            $color = array(
                $colors
            );
        }
        $this->assign("color", $color);
        $this->display();
    }

    public function listsdstaff()
    {
        $this->LoginTrue();
        $stId = $_GET["stId"];
        $staffLists = M("staff");
        $rs_staff = $staffLists->where("stId={$stId}")->find();
        $this->assign("rs_staff", $rs_staff);
        $year = date("Y");
        $this->assign("year", $year);
        $this->display();
    }

    public function ListsEdit()
    {
        $this->LoginTrue();
        $department = M("department");
        $rs_department = $department->select();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function ListsEditUpdate()
    {
        $this->LoginTrue();
        $dId = $_GET["dId"];
        $department = M("department");
        $rs_department = $department->where("dId={$dId}")->find();
        $this->assign("rs_department", $rs_department);
        
        $this->display();
    }

    public function Update()
    {
        $this->LoginTrue();
        $dId = $_GET["dId"];
        $department = M("department");
        $rs_department = $department->where("dId={$dId}")->find();
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function UpdateAction()
    {
        $this->LoginTrue();
        $dId = $_GET["dId"];
        $data["dPid"] = $_POST["dPid"];
        if ($data["dPid"] == "") {
            $data["dPid"] = 0;
        }
        $data["dPsid"] = $_POST["dPsid"];
        if ($data["dPsid"] == "") {
            $data["dPsid"] = 0;
        }
        $data["dName"] = $_POST["dName"];
        $data["dDirector"] = $_POST["dDirector"];
        if ($data["dDirector"] == "") {
            $data["dDirector"] = "暂未设定负责人";
        }
        $data["dDirectorTel"] = $_POST["dDirectorTel"];
        if ($data["dDirectorTel"] == "") {
            $data["dDirectorTel"] = "暂无完善";
        }
        $data["dDirectorQQ"] = $_POST["dDirectorQQ"];
        if ($data["dDirectorQQ"] == "") {
            $data["dDirectorQQ"] = "暂无完善";
        }
        $data["dDirectorEmail"] = $_POST["dDirectorEmail"];
        if ($data["dDirectorEmail"] == "") {
            $data["dDirectorEmail"] = "暂无完善";
        }
        $data["dInfo"] = $_POST["dInfo"];
        if ($data["dInfo"] == "") {
            $data["dInfo"] = "暂未填写";
        }
        $department = M("department");
        $result = $department->where("dId={$dId}")->save($data);
        if ($result) {
            $this->success("修改成功", U("lists"));
        } else {
            $this->error("修改失败");
        }
    }

    public function Del()
    {
        $this->LoginTrue();
        $dId = $_GET["dId"];
        $department = M("department");
        $rs_department = $department->where("dId={$dId}")->find();
        $staff = M("staff");
        $rs_staff = $staff->field("stId,stDid")->select();
        foreach ($rs_staff as $val_staff) {
            if ($val_staff["stDid"] == $rs_department["dId"]) {
                $static = 1;
            }
        }
        $this->assign("static", $static);
        $this->assign("rs_department", $rs_department);
        $this->display();
    }

    public function DelAction()
    {
        $this->LoginTrue();
        $dId = $_GET["dId"];
        $department = M("department");
        $rs = $department->where("dPid={$dId}")->count();
        if ($rs > 0) {
            $this->error("该部门下有二级部门，请删除二级部门在删除该部门");
        }
        $rs1 = $department->where("dPsid={$dId}")->count();
        if ($rs1 > 0) {
            $this->error("该部门下有三级部门，请删除三级部门在删除该部门");
        }
        $result = $department->where("dId={$dId}")->delete();
        if ($result) {
            $this->success("删除成功");
        } else {
            $this->error("删除失败");
        }
    }

    public function ShutDown()
    {
        $this->LoginTrue();
        $this->display();
    }
}