<?php
namespace Talentpool\Controller;

use Think\Controller;
header("content-type:text/html;charset=utf-8");

class LoginController extends Controller
{

    public function Index()
    {
        if (strlen(session("hrTel")) > 0) {
            $this->success("你已登陆，正在跳转", U('/Index/'));
        } else {
            $this->display();
        }
    }

    public function CheckLogin()
    {
        $hrreserve = M("hrreserve");
        $hrTel = $_POST["hrTel"];
        $hrPwd = md5($_POST["hrPwd"]);
        $rs_hrreserve = $hrreserve->where("hrTel='{$hrTel}'")
            ->field("hrId,hrState,hrTel,hrPwd")
            ->find();
        if (count($rs_hrreserve) > 0) {
			/*
            if ($rs_hrreserve["hrState"] == 1) {
                $this->error("该简历已被录取，不再允许登陆，可进行查询");
            }
			*/
            if ($hrPwd != $rs_hrreserve["hrPwd"]) {
                $this->error("密码错误，登陆失败");
            } else {
                session("hrTel", $hrTel);
                $this->success("登陆成功", U('/Index/'));
            }
        } else {
            $this->error("账户不存在，请登记后在登陆", "", 5);
        }
    }

    public function Reg()
    {
        if (strlen(session("hrTel")) > 0) {
            $this->success("已登陆，请注销账户后在登记", "Index");
        } else {
            $variables = M("variables");
            $rs_xueli = $variables->where("vId=1")->find();
            $xueli = explode("|", $rs_xueli["vVariablesVal"]);
            $this->assign("xueli", $xueli);
            $this->display();
        }
    }

    public function RegAction()
    {
        $hrreserve = M("hrreserve");
        $hrName = $_POST["hrName"];
        $hrSex = $_POST["hrSex"];
        $hrDegrees = $_POST["hrDegrees"];
        $hrHeight = $_POST["hrHeight"];
        $hrTel = $_POST["hrTel"];
        $hrPwd = $_POST["hrPwd"];
        $hrPwds = $_POST["hrPwds"];
        if (strlen($hrName) < 2) {
            $this->error("姓名不得少于2个字符");
        } else {
            $count = $hrreserve->where("hrName='{$hrName}'")->count();
            if ($count > 0) {
                $this->error("该姓名已经存在，无需重复登记");
            }
        }
        if (strlen($hrHeight) != 3) {
            $this->error("请输入三位数字，单位是厘米");
        }
        if (strlen($hrTel) != 11) {
            $this->error("手机号码必须是11位");
        } else {
            $count = $hrreserve->where("hrTel='{$hrTel}'")->count();
            if ($count > 0) {
                $this->error("该手机号码已经登记过，无需重复登记");
            }
        }
        $lengthPwd = strlen($hrPwd);
        if ($lengthPwd < 6 || $lengthPwd > 16) {
            $this->error("密码必须在6到16位之间");
        }
        if ($hrPwd != $hrPwds) {
            $this->error("两次密码输入不对");
        }
        $data["hrName"] = $hrName;
        $data["hrSex"] = $hrSex;
        $data["hrDegrees"] = $hrDegrees;
        $data["hrHeight"] = $hrHeight;
        $data["hrTel"] = $hrTel;
        $data["hrPwd"] = md5($hrPwd);
        // 创建时间
        $data["hrInputDate"] = date("Y-m-d H:i:s");
        $data["hrGengxingDate"]=$data["hrInputDate"];
        $result = $hrreserve->add($data);
        if ($result) {
            $this->success("登记成功，请登陆！", U("index"));
        }
    }
}
