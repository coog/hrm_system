<?php
namespace Talentpool\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class PerfectInfoController extends Controller
{
    public function UpdateCV()
    {   
        
        // $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_hrreserve=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        // 学历的处理
        $variables = M("variables");
        $rs_xueli = $variables->where("vId=1")->find();
        $xueli = explode("|", $rs_xueli["vVariablesVal"]);
        $this->assign("xueli", $xueli);
        
        //职务的处理【应聘岗位】
        $rs_zhiwu = $variables->where("vId=3")->find();
        $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]);
        $this->assign("zhiwu", $zhiwu);
        $this->display();
    }

    public function UpdateCVAction()
    {
        // $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $data["hrName"]=$_POST["hrName"];
        $data["hrSex"]=$_POST["hrSex"];
        $data["hrDegrees"]=$_POST["hrDegrees"];
        $data["hrHeight"]=$_POST["hrHeight"];
        $data["hrBirthdateType"]=$_POST["hrBirthdateType"];
        $data["hrBirthdate"]=$_POST["hrBirthdate"];
        $data["hrTel"]=$_POST["hrTel"];
        $data["hrMultiracial"]=$_POST["hrMultiracial"];
        $data["hrNativePlace"]=$_POST["hrNativePlace"];
        $data["hrEmail"]=$_POST["hrEmail"];
        $data["hrDuties"]=$_POST["hrDuties"];
        $data["hrSalary"]=$_POST["hrSalary"];
        $data["hrGengxingDate"] = date("Y-m-d H:i:s");
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
            $this->success("保存成功",U("updatecvtwo"));
        }else{
            $this->error("你没有做任何修改",U("updatecvtwo"));
        }
    }
    public function UpdateCVTwo()
    {
        // $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_hrreserve=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        $this->display();
    }
    
    public function UpdateCVTwoAction()
    {
        // $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $data["hrWeight"]=$_POST["hrWeight"];
        $data["hrQQ"]=$_POST["hrQQ"];
        $data["hrCity"]=$_POST["hrCity"];
        $data["hrIDCard"]=$_POST["hrIDCard"];
        $data["hrPoliticalIandscape"]=$_POST["hrPoliticalIandscape"];
        $data["hrEdu"]=$_POST["hrEdu"];
        $data["hrJineng"]=$_POST["hrJineng"];
        $data["hrWorks"]=$_POST["hrWorks"];
        $data["hrPingjia"]=$_POST["hrPingjia"];
        $data["hrRemarks"]=$_POST["hrRemarks"];
        $data["hrGengxingDate"] = date("Y-m-d H:i:s");
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
            $this->success("保存成功",U("updatecvthree"));
        }else{
            $this->error("你没有做任何修改",U("updatecvthree"));
        }
    }
    
    public function UpdateCVThree()
    {
        // $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_hrreserve=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_hrreserve",$rs_hrreserve);
        $this->display();
    }
    
    public function UpdateCVThreeAction()
    {
        // $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $data["hrPhoto"]=$_POST["hrPhoto"];
        $data["hrEnclosure"]=$_POST["hrEnclosure"];
        $data["hrShenfenzhengImages"]=$_POST["hrShenfenzhengImages"];
        $data["hrGengxingDate"] = date("Y-m-d H:i:s");
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
            $this->success("保存成功");
        }else{
            $this->error("你没有做任何修改");
        }
    }

    public function up()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Perfect/photo/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }
    
    public function upfujian()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Perfect/fujian/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }
    
    public function upsfz()
    {
        $config = array(
            'rootPath' => './Uploads/', // 保存根路径
            'savePath' => 'Perfect/shenfenzheng/', // 保存路径
            'subName' => array(
                'date',
                'Ymd'
            )
        );
        $up = new \Think\Upload($config);
        $rup = $up->upload($_FILES);
        $a = "";
        foreach ($rup as $v) {
            $name = './Uploads/' . $v['savepath'] . $v['savename'];
            $url = 'Uploads/' . $v['savepath'] . $v['savename'];
            list ($width, $height, $type, $attr) = getimagesize($name);
            if ($width > 0) {
                echo json_encode(array(
                    "error" => "0",
                    "pic" => $url
                ));
            } else {
                echo json_encode(array(
                    "error" => "上传有误，清检查服务器配置！"
                ));
            }
        }
    }
    
    //修改账户中的密码
    public function userPwd(){
        // $this->LoginTrue();
        $hrreserve=M("hrreserve");
        $hrTel=session("hrTel");
        $rs_userInfo=$hrreserve->where("hrTel='{$hrTel}'")->find();
        $this->assign("rs_userInfo",$rs_userInfo);
        $this->display();
    }
    public function userPwdAction(){
        // $this->LoginTrue();
        $hrId=$_GET["hrId"];
        $hrreserve=M("hrreserve");
        $rs_userInfo=$hrreserve->where("hrId={$hrId}")->field("hrPwd")->find();
        $hrPwd=$_POST["hrPwd"];
        if(md5($hrPwd)!=$rs_userInfo["hrPwd"]){
           $this->error("原密码错误，无法修改"); 
        }
        $pwd=$_POST["pwd"];
        $pwd2=$_POST["pwd2"];
        if($pwd!=$pwd2){
            $this->error("两次输入的密码不一致，请重新输入");
        }
        if($hrPwd==$pwd){
            $this->error("新密码不能和原密码一致");
        }
        $data["hrPwd"]=md5($_POST["pwd"]);
        $result=$hrreserve->where("hrId={$hrId}")->save($data);
        if($result){
           $this->success("修改密码成功"); 
        }else{
            $this->error("修改密码失败");
        }
    }
}

