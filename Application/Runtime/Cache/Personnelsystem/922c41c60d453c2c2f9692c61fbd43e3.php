<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>员工简历</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">
<link href="/Public/Theme1/static/plupload/upfiless.css"
	rel="stylesheet" type="text/css" />
<link href="/Public/Theme1/lib/webuploader/0.1.5/webuploader.css"
	rel="stylesheet" type="text/css" />
<link
	href="/Public/Theme1/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							<?php echo ($rs_staffShow["stName"]); ?>的个人档案 <small><a
								style="margin-left: 15px" href="/Staff/lists">返回列表</a>
                                <a style="margin-left: 15px; font-weight:bold;" href="/Staff/show/stId/<?php echo ($rs_staffShow["stId"]); ?>/print/1">打印简历</a></small>
						</h5>
						<div class="ibox-tools"></div>
					</div>
					<div class="ibox-content">
						<form method="post" action="/Staff/AddAction"
							class="form-horizontal" id="form-admin-add"
							enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-sm-0 control-label" style="font-size:18px; margin-left:45%">员工基本信息表</label><br>
							</div>
                            <?php $zhichengInfo=""; $zhichengId=$rs_staffShow["stPositionalTitles"]; $variables=M("variables"); $rs_variables=$variables->where("vId=2")->find(); $zhicheng=explode("|",$rs_variables["vVariablesVal"]); foreach($zhicheng as $key=>$valzhicheng){ if($key==$zhichengId){ $zhichengInfo=$valzhicheng; } } ?>
                            
                            <?php $zhiwuInfo=""; $zhiwuId=$rs_staffShow["stDuties"]; $variables=M("variables"); $rs_variables=$variables->where("vId=3")->find(); $zhiwu=explode("|",$rs_variables["vVariablesVal"]); foreach($zhiwu as $key=>$valzhiwu){ if($key==$zhiwuId){ $zhiwuInfo=$valzhiwu; } } ?>
                            
                            <?php $xueliInfo=""; $xueliId=$rs_staffShow["stDegrees"]; $variables=M("variables"); $rs_variables=$variables->where("vId=1")->find(); $xueli=explode("|",$rs_variables["vVariablesVal"]); foreach($xueli as $key=>$valxueli){ if($key==$xueliId){ $xueliInfo=$valxueli; } } ?>
                            
                            <?php $xuexingInfo=""; $xuexingId=$rs_staffShow["stBloodType"]; $variables=M("variables"); $rs_variables=$variables->where("vId=5")->find(); $xuexing=explode("|",$rs_variables["vVariablesVal"]); foreach($xuexing as $key=>$valxuexing){ if($key==$xuexingId){ $xuexingInfo=$valxuexing; } } ?>
                            
                            <?php $leibieInfo=""; $leibieId=$rs_staffShow["stPersonnelCategory"]; $variables=M("variables"); $rs_variables=$variables->where("vId=6")->find(); $leibie=explode("|",$rs_variables["vVariablesVal"]); foreach($leibie as $key=>$valleibie){ if($key==$leibieId){ $leibieInfo=$valleibie; } } ?>
                            
                            <?php $minzuInfo=""; $minzuId=$rs_staffShow["stMultiracial"]; $variables=M("variables"); $rs_variables=$variables->where("vId=7")->find(); $minzu=explode("|",$rs_variables["vVariablesVal"]); foreach($minzu as $key=>$valminzu){ if($key==$minzuId){ $minzuInfo=$valminzu; } } ?>
                            
                            
                            <?php  $stDegrees=$rs_staffShow["stDegrees"]; $xueliYear=0; if($stDegrees==4){ $xueliYear=1; }elseif($stDegrees==5){ $xueliYear=2; }elseif($stDegrees==6){ $xueliYear=3; }elseif($stDegrees==7){ $xueliYear=4; } $stId=$rs_staffShow["stId"]; $historyrecord_score=M("historyrecord_score"); $youxiuScoreNumber=$historyrecord_score->where("(hirsStId=$stId) AND hirsScore>=90")->count(); $youxiuScoreYear=0.1*$youxiuScoreNumber; $jiaozhengGongling=$nowWorkingYear+$xueliYear+$youxiuScoreYear; $dengji=""; if($jiaozhengGongling < 1 ){ $dengji="暂无"; }elseif($jiaozhengGongling>=1 && $jiaozhengGongling < 4 ){ $dengji="A"; }elseif($jiaozhengGongling>=4 && $jiaozhengGongling < 7 ){ $dengji="2 A"; }elseif($jiaozhengGongling>=7 && $jiaozhengGongling < 10 ){ $dengji="3 A"; }elseif($jiaozhengGongling>=10 && $jiaozhengGongling < 13 ){ $dengji="4 A"; }elseif($jiaozhengGongling>=13 && $jiaozhengGongling < 17 ){ $dengji="5 A"; }elseif($jiaozhengGongling>=17 && $jiaozhengGongling < 21 ){ $dengji="6 A"; }elseif($jiaozhengGongling>=21){ $stPositionalTitles=$rs_staffShow["stPositionalTitles"]; if($stPositionalTitles!=3){ $dengji="7 A"; }else{ $dengji="6 A"; } } ?>
<table border="1" align="center" cellpadding="0" cellspacing="0">
    <tbody >
        <tr class="firstRow" >
            <td width="90" height="50" valign="middle" style="word-break: break-all;" align="center">
                <strong>姓名&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($rs_staffShow["stName"]); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>性别&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php if($rs_staffShow["stSex"] == 1): ?>&nbsp;&nbsp;&nbsp;&nbsp;男&nbsp;&nbsp;&nbsp;&nbsp; <?php else: ?>
										&nbsp;&nbsp;&nbsp;&nbsp;女 &nbsp;&nbsp;&nbsp;&nbsp;<?php endif; ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>出生日期&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($rs_staffShow["stBirthdate"]); ?></td>
            <td colspan="2" rowspan="4" align="center" valign="middle"><?php $stPhoto=strlen($rs_staffShow["stPhoto"]); if($stPhoto > 12): ?><img src="/<?php echo ($rs_staffShow["stPhoto"]); ?>" width="190" height="190"><?php else: ?><img src="/Public/Theme1/img/not.png" width="190" height="190"><?php endif; ?></td>
        </tr>
        <tr>
            <td width="90" height="50" valign="middle" style="word-break: break-all;" align="center">
                <strong>民族&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($minzuInfo); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>血型&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($xuexingInfo); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>政治面貌&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($rs_staffShow["stPoliticalIandscape"]); ?></td>
        </tr>
        <tr>
            <td width="90" height="50" valign="middle" style="word-break: break-all;" align="center">
                <strong>身份证&nbsp;</strong>
            </td>
            <td colspan="3" rowspan="1" align="center" valign="middle"><?php echo ($rs_staffShow["stIDCard"]); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>籍贯&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($rs_staffShow["stNativePlace"]); ?></td>
        </tr>
        <tr>
            <td width="90" height="50" valign="middle" style="word-break: break-all;" align="center">
                <strong>所在部门&nbsp;</strong>
            </td>
            <td colspan="3" rowspan="1" align="center" valign="middle"><?php echo ($rs_department["dName"]); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>职务&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($zhiwuInfo); ?></td>
        </tr>
        <tr>
            <td width="90" height="50" valign="middle" style="word-break: break-all;" align="center">
                <strong>住址&nbsp;</strong>
            </td>
            <td colspan="3" rowspan="1" align="center" valign="middle"><?php echo ($rs_staffShow["stAddress"]); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>职称&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($zhichengInfo); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>人员类别&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($leibieInfo); ?></td>
        </tr>
        <tr>
            <td width="90" height="50" valign="middle" style="word-break: break-all;" align="center">
                <strong>入职日期&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($rs_staffShow["stEntryDate"]); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>学历&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($xueliInfo); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>状态&nbsp;</strong>
            </td>
            
            <td width="110" align="center" valign="middle"><?php if($rs_staffShow["stJobState"] == 1): ?>在职<?php else: ?>离职<?php endif; ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>级别&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($dengji); ?></td>
        </tr>
        <tr>
            <td width="90" height="50" valign="middle" style="word-break: break-all;" align="center">
                <strong>健康状况&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($rs_staffShow["stHealth"]); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
                <strong>掌握技能&nbsp;</strong>
            </td>
            <td colspan="3" align="center" valign="middle"><?php echo ($rs_staffShow["stJineng"]); ?></td>
            <td width="90" valign="middle" style="word-break: break-all;" align="center">
              <strong>电话&nbsp;</strong>
            </td>
            <td width="110" align="center" valign="middle"><?php echo ($rs_staffShow["stTel"]); ?></td>
        </tr>
        <tr>
            <td width="90" height="100" valign="middle" style="word-break: break-all;" align="center">
                <p>
                    <br/>
                </p>
                <p>
                    <strong>工作经历&nbsp;</strong>
                </p>
                <p>
                    <br/>
                </p>
            </td>
            <td colspan="7" rowspan="1" align="center" valign="middle"><?php echo ($rs_staffShow["stJingyuan"]); ?></td>
        </tr>
        <tr>
            <td width="90" height="100" valign="middle" style="word-break: break-all;" align="center">
                <p>
                    <br/>
                </p>
                <p>
                    <strong>获奖经历&nbsp;</strong>
                </p>
                <p>
                    <br/>
                </p>
            </td>
            <td colspan="7" rowspan="1" align="center" valign="middle"><?php echo ($rs_staffShow["stWinning"]); ?></td>
        </tr>
        
        <tr>
            <td width="90" height="100" valign="middle" style="word-break: break-all;" align="center">
                <p>
                    <br/>
                </p>
                <p>
                    <strong>年度考核<br>情况&nbsp;</strong>
                </p>
                <p>
                    <br/>
                </p>
            </td>
            <td colspan="7" rowspan="1" align="center" valign="middle"><?php echo ($rs_staffShow["stAssessment"]); ?></td>
        </tr>
        
        
        
        <tr>
            <td valign="middle" height="70" colspan="1" rowspan="1" style="word-break: break-all;" align="center" width="90">
                <strong>人员备注&nbsp;</strong>
            </td>
            <td colspan="7" rowspan="1" align="center" valign="middle" style="word-break: break-all;"><?php echo ($rs_staffShow["stRemarks"]); ?></td>
        </tr>
    </tbody>
</table>

<?php $print=$_GET["print"]; ?>
<?php if($print == 1): ?><script type="text/javascript">
		window.print() ;
</script><?php endif; ?>

					  </form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/messages_zh.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/validate-methods.js"></script>
	<script type="text/javascript"
		src="/Public/Theme1/lib/webuploader/0.1.5/webuploader.min.js"></script>

	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>



	<script type="text/javascript">
	$(function(){
	$("#form-admin-add").validate({
		rules:{
            stNum:{
                required:true,
                minlength:1,
                maxlength:8
            },
            stName:{
                required:true,
                minlength:2,
                maxlength:16
            },
            stSex:{
                required:true,
            },
            stBirthdate:{
                required:true,
            },
            sCompanyIntroduce:{
                required:true,
            },
            stTel:{
                required:true,
                minlength:7,
                maxlength:11
            },
            stDegrees:{
                required:true,
            },
            stEntryDate:{
                required:true,
            },
            stDid:{
                required:true,
            },
            stPositionalTitles:{
                required:true,
            },
            stDuties:{
                required:true,
            },
            stMultiracial:{
                required:true,
            },
            stNativePlace:{
                required:true,
            },
            stIDCard:{
                //required:true,
                minlength:15,
                maxlength:18
            }
            
        },
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit();
			var index = parent.layer.getFrameIndex(window.name);
			parent.$('.btn-refresh').click();
			parent.layer.close(index);
		}
	});
});
</script>

	<script type="text/javascript"
		src="/Public/Theme1/static/plupload/plupload.full.min.js"></script>
	<script type="text/javascript">
            var uploader = new plupload.Uploader(
                  {
                    runtimes: 'html5,flash,silverlight,html4', //上传插件初始化选用那种方式的优先级顺序
                    browse_button: 'btn', // 上传按钮
                    url: "/Staff/up", //远程上传地址
                    flash_swf_url: '/Public/plupload/Moxie.swf', //flash文件地址
                    silverlight_xap_url: '/Public/plupload/Moxie.xap', //silverlight文件地址
                    filters: {
                        max_file_size: '2mb', //最大上传文件大小（格式100b, 10kb, 10mb, 1gb）
                        mime_types: [//允许文件上传类型
                            {title: "files", extensions: "jpg,png,gif"}
                        ]
                 },
                multi_selection: true, //true:ctrl多文件上传, false 单文件上传
                init: {
                    FilesAdded: function(up, files) { //文件上传前
                        if ($("#ul_pics").children("li").length >7) {
                            alert("您上传的图片太多了！");
                            uploader.destroy();
                        } else {
                            var dd = '';
                            plupload.each(files, function(file) { //遍历文件
                                dd += "<dd id='" + file['id'] + "'><div class='progress'><span class='bar'></span><span class='percent'>0%</span></div></dd>";
                            });
                            $("#ul_pics").append(dd);
                            uploader.start();
                        }
                    },
                    UploadProgress: function(up, file) { //上传中，显示进度条
                 var percent = file.percent;
                        $("#ul_pics" + file.id).find('.bar').css({"width": percent + "%"});
                        $("#ul_pics" + file.id).find(".percent").text(percent + "%");
                    },
                    FileUploaded: function(up, file, info) { //文件上传成功的时候触发
                       var data = eval("(" + info.response + ")");
                        $("#" + file.id).html("<img src='/" + data.pic + "'/>");
                        var old=$("#pPic").val();
                         $("#pPic").val(old + data.pic+'###');
                    },
                    Error: function(up, err) { //上传出错的时候触发
                        alert(err.message);
                    }
                }
            });
            uploader.init();
        </script>
        
</body>

</html>