<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>员工相关信息</title>

<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">

<!-- Data Tables -->
<link
	href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							员工相关信息 <a style="color: #ff0000; margin-left: 15px;"
								href="/Personnelsystem/Department/listsinfo/dId/<?php echo ($rs_staff["stDid"]); ?>">返回</a>
						</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">

						<table class="table table-striped table-bordered table-hover ">
							<thead>
								<tr>
									<th>编号</th>
									<th>姓名</th>
									<th>性别</th>
									<th>电话</th>
									<th class="dropdown hidden-xs">身高</th>
									<th class="dropdown hidden-xs">体重</th>
								</tr>
							</thead>
							<tbody>

								<tr class="text-c">
									<td><?php echo ($rs_staff["stNum"]); ?></td>
									<td><?php echo ($rs_staff["stName"]); ?></td>
									<?php if($rs_staff["stSex"] == 1): ?><td class="dropdown hidden-xs">男</td>
									<?php else: ?>
									<td class="dropdown hidden-xs">女</td><?php endif; ?>
									<td class="dropdown hidden-xs"><?php echo ($rs_staff["stTel"]); ?></td>
									<td class="dropdown hidden-xs"><?php echo ($rs_staff["stHeight"]); ?> CM</td>
									<td class="dropdown hidden-xs"><?php echo ($rs_staff["stWeight"]); ?> Kg</td>
								</tr>

							</tbody>

						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script
		src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
	<script
		src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script
		src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script>
        $(document).ready(function(){$(".dataTables-example").dataTable();var oTable=$("#editable").dataTable();oTable.$("td").editable("../example_ajax.php",{"callback":function(sValue,y){var aPos=oTable.fnGetPosition(this);oTable.fnUpdate(sValue,aPos[0],aPos[1])},"submitdata":function(value,settings){return{"row_id":this.parentNode.getAttribute("id"),"column":oTable.fnGetPosition(this)[2]}},"width":"90%","height":"100%"})});function fnClickAddRow(){$("#editable").dataTable().fnAddData(["Custom row","New row","New row","New row","New row"])};
    </script>

</body>

</html>