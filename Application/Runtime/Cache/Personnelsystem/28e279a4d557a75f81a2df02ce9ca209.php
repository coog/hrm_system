<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title><?php echo ($year); ?>年度员工入职离职数据统计</title>


<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">

<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">


</head>
<script src="/Public/Theme1/js/echarts.js"></script>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">

			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5><?php echo ($year); ?>年度员工入职离职数据统计</h5>
						<div class="ibox-tools"></div>
					</div>
					<div class="ibox-content" style="height: 700px;">
						<div class="flot-chart">
							<div id="container" style="min-width: 700px; height: 500px"></div>
						</div>
						<script type="text/javascript"
							src="/Public/Theme1/lib/jquery/1.9.1/jquery.min.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/layer/2.1/layer.js"></script>
						<script type="text/javascript"
							src="/Public/AdminStyle/static/h-ui/js/H-ui.js"></script>
						<script type="text/javascript"
							src="/Public/AdminStyle/static/h-ui.admin/js/H-ui.admin.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/Highcharts/4.1.7/js/highcharts.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/Highcharts/4.1.7/js/modules/exporting.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/Highcharts/4.1.7/js/highcharts-3d.js"></script>
						<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo ($year); ?>年度月份员工入职与离职数据统计'
        },
        subtitle: {
            text: '数据实时更新中'
        },
        xAxis: {
            categories: [
                '一月',
                '二月',
                '三月',
                '四月',
                '五月',
                '六月',
                '七月',
                '八月',
                '九月',
                '十月',
                '十一月',
                '十二月'
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: '人数 '
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0"> {series.name}: </td>' +
                '<td style="padding:0"><b> {point.y:.0f} 人</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: '入职人数',
            data: [<?php echo ($working_01); ?>, <?php echo ($working_02); ?>, <?php echo ($working_03); ?>, <?php echo ($working_04); ?>, <?php echo ($working_05); ?>, <?php echo ($working_06); ?>, <?php echo ($working_07); ?>, <?php echo ($working_08); ?>, <?php echo ($working_09); ?>, <?php echo ($working_10); ?>, <?php echo ($working_11); ?>, <?php echo ($working_12); ?>]

        }, {
            name: '离职人数',
            data: [<?php echo ($worked_01); ?>, <?php echo ($worked_02); ?>, <?php echo ($worked_03); ?>, <?php echo ($worked_04); ?>, <?php echo ($worked_05); ?>, <?php echo ($worked_06); ?>, <?php echo ($worked_07); ?>, <?php echo ($worked_08); ?>, <?php echo ($worked_09); ?>, <?php echo ($worked_10); ?>, <?php echo ($worked_11); ?>, <?php echo ($worked_12); ?>]

        }]
    });
});             
</script>
</body>

</html>