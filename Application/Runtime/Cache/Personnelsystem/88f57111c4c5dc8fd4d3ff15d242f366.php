<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>系统参数设置</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">
<link
	href="/Public/Theme1/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							修改我的资料 <small></small>
						</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="dropdown-toggle" data-toggle="dropdown"
								href="form_basic.html#"> <i class="fa fa-wrench"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<form method="post"
							action="/Manage/ManageUpdateAction/aId/<?php echo ($rs_admin["aId"]); ?>"
							class="form-horizontal" id="form-admin-add">
							<div class="form-group">
								<label class="col-sm-2 control-label">登陆账号</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										value="<?php echo ($rs_admin["aUser"]); ?>" disabled>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">真实姓名</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										value="<?php echo ($rs_admin["aName"]); ?>" placeholder="2到5个字符" id="aName"
										name="aName">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">原密码</label>
								<div class="col-sm-10">
									<input type="password" class="form-control" autocomplete="off"
										value="" placeholder="请输入原密码，不修改请留空" id="aOldPwd"
										name="aOldPwd">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">新密码</label>
								<div class="col-sm-10">
									<input type="password" class="form-control" autocomplete="off"
										value="" placeholder="6到16位字符，不修改请留空" id="aNewPwd"
										name="aNewPwd">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">确认新密码</label>
								<div class="col-sm-10">
									<input type="password" class="form-control" autocomplete="off"
										placeholder="确认新密码,如果没有填写新密码请勿填写" id="aNewPwd2"
										name="aNewPwd2">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">性别</label>

								<div class="col-sm-10">
									<?php if($rs_admin["aSex"] == 1): ?><label
										class="checkbox-inline"> <input type="radio" value="1"
										name="aSex" checked> 男
									</label> <label class="checkbox-inline"> <input type="radio"
										value="2" name="aSex"> 女
									</label> <?php else: ?> <label class="checkbox-inline"> <input
										type="radio" value="1" name="aSex"> 男
									</label> <label class="checkbox-inline"> <input type="radio"
										value="2" name="aSex" checked> 女
									</label><?php endif; ?>

								</div>
							</div>

							<?php if($rs_admin["aPowers"] != 0): ?><div class="form-group">
								<label class="col-sm-2 control-label">角色名</label>
								<div class="col-sm-10">
									<select data-placeholder="请选择权限组" name="aPowers"
										class="chosen-select form-control" required>
										<?php if(is_array($rs_role)): foreach($rs_role as $key=>$val_role): $selected=""; if($rs_admin["aPowers"]==$val_role["arId"]){ $selected="selected"; } ?>
										<option value="<?php echo ($val_role["arId"]); ?>"<?php echo ($selected); ?>><?php echo ($val_role["arName"]); ?></option><?php endforeach; endif; ?>

									</select>
								</div>
							</div><?php endif; ?>
							<div class="form-group">
								<label class="col-sm-2 control-label">手机</label>
								<div class="col-sm-10">
									<input type="text" class="form-control"
										value="<?php echo ($rs_admin["aTel"]); ?>" placeholder="" id="aTel"
										name="aTel">
								</div>
							</div>


							<div class="form-group">
								<label class="col-sm-2 control-label">邮箱</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" placeholder="@"
										value="<?php echo ($rs_admin["aEmail"]); ?>" name="aEmail" id="aEmail">
								</div>
							</div>


							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-2">
									<button class="btn btn-primary" type="submit">保存</button>
									<button class="btn btn-white" type="submit">取消</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>
	<script type="text/javascript"
		src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/messages_zh.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/validate-methods.js"></script>



	<script type="text/javascript">
    $(function(){
    $("#form-admin-add").validate({
        rules:{
            aUser:{
                required:true,
                minlength:4,
                maxlength:16
            },
            aName:{
                required:true,
                minlength:2,
                maxlength:14
            },
            aPwd:{
                required:true,
                minlength:6,
                maxlength:16
            },
            aPwd2:{
                required:true,
                equalTo: "#aPwd"
            },
            aSex:{
                required:true,
            },
            aTel:{
                required:true,
                isPhone:true,
            },
            aPowers:{
                required:true,
            },
            
        },
        onkeyup:false,
        focusCleanup:true,
        success:"valid",
        submitHandler:function(form){
            $(form).ajaxSubmit();
            var index = parent.layer.getFrameIndex(window.name);
            parent.$('.btn-refresh').click();
            parent.layer.close(index);
        }
    });
});
</script>
</body>

</html>