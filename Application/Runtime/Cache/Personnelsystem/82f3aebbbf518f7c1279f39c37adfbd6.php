<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>本月评分</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5><?php echo ($rs_staff["stName"]); ?>本月得分</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-wrench"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<form method="post"
							action="/StaffScore/setScoreAction/stId/<?php echo ($rs_staff["stId"]); ?>"
							class="form-horizontal" id="form-admin-add">
							<div class="form-group">
								<label class="col-sm-1 control-label">姓名</label>
								<div class="col-sm-10">
									<input type="text" style="width: 100%"
										value="<?php echo ($rs_staff["stName"]); ?>"
										class="form-control" readyonly>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">性别</label>
								<div class="col-sm-10">
								<?php if($rs_staff["stSex"] == 1): ?><input type="text" value="男"  class="form-control" readyonly>
									<?php else: ?>
									<input type="text" value="女"  class="form-control" readyonly><?php endif; ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">学历</label>
								<div class="col-sm-10">
									<input type="text" 
										value="<?php echo ($xueliInfo); ?>" class="form-control" readyonly>
								</div>
							</div>


							<div class="form-group">
								<label class="col-sm-1 control-label">部门</label>
							<div class="col-sm-10">
								<select class="chosen-select form-control" size="1" name="stDid"
									id="stDid" readyonly>
									<?php if(is_array($rs_department)): foreach($rs_department as $key=>$val_department): $selected=""; if($rs_staff["stDid"]==$val_department["dId"]){ $selected="selected"; } ?> <?php if($val_department["dPid"] == 0 AND $val_department["dPsid"] == 0): ?><option style="color: #f87ca8" value="<?php echo ($val_department["dId"]); ?>"<?php echo ($selected); ?>><?php echo ($val_department["dName"]); ?></option>
									<?php elseif($val_department["dPid"] != 0 AND $val_department["dPsid"] == 0): ?>
									<?php $dPid=$val_department["dPid"]; $department=M("department"); $rsp=$department->where("dId={$dPid}")->find(); ?>
									<option style="color: #44a5e4" value="<?php echo ($val_department["dId"]); ?>"<?php echo ($selected); ?>><?php echo ($rsp["dName"]); ?>
										-> <?php echo ($val_department["dName"]); ?></option>

									<?php elseif($val_department["dPid"] == 0 AND $val_department["dPsid"] != 0): ?>
									<?php $dPsid=$val_department["dPsid"]; $department=M("department"); $rsps=$department->where("dId={$dPsid}")->find(); $rsPsPid=$rsps["dPid"]; $rsPspe=$department->where("dId={$rsPsPid}")->find(); ?>
									<option style="color: #067b14" value="<?php echo ($val_department["dId"]); ?>"<?php echo ($selected); ?>><?php echo ($rsPspe["dName"]); ?>
										-> <?php echo ($rsps["dName"]); ?> -> <?php echo ($val_department["dName"]); ?></option><?php endif; endforeach; endif; ?>

								</select>
							</div>
							</div>

							

							<div class="form-group">
								<label class="col-sm-1 control-label">职称</label>
								<div class="col-sm-10">
									<input type="text" value="<?php echo ($zhichengInfo); ?>" class="form-control" readyonly>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">职位</label>
								<div class="col-sm-10">
									<input type="text" value="<?php echo ($zhiwuInfo); ?>" class="form-control" readyonly>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">入职时间</label>
								<div class="col-sm-10">
									<input type="text" value="<?php echo ($rs_staff["stEntryDate"]); ?>"
										class="form-control" readyonly>
								</div>
							</div>

							<?php $a=-1; if($nowMonth != $rs_staff["stMonthValue"]){ $a=0; }else{ $a=1; } ?>

							<div class="form-group">
								<label class="col-sm-1 control-label">本月得分</label>
								<div class="col-sm-10">
									<?php if($a == 1): ?><input type="text" name="stMScore" id="stMScore" value="<?php echo ($rs_staff["stMScore"]); ?>" class="form-control" required>
									<?php else: ?>
									<input type="text" name="stMScore" id="stMScore" placeholder="本月还未评分" class="form-control" required><?php endif; ?>
								</div>
							</div>

							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-2">
									<button class="btn btn-primary" type="submit">保存</button>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/messages_zh.min.js"></script>



	<script type="text/javascript"
		src="/Public/Theme1/check/js/validate-methods.js"></script>




	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>

	<script type="text/javascript">
	$(function(){
	$("#form-admin-add").validate({
		rules:{
			vNum:{
                required:true,
            },
           vVariablesExplain:{
                required:true,
            },
            vVariables:{
                required:true,
            },
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit();
			var index = parent.layer.getFrameIndex(window.name);
			parent.$('.btn-refresh').click();
			parent.layer.close(index);
		}
	});
});
</script>


</body>

</html>