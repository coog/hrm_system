<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title><?php echo ($rs_systemName["sName"]); ?>-系统中心</title>

<!--[if lt IE 9]>

    <meta http-equiv="refresh" content="0;ie.html" />

    <![endif]-->

<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style1.min.css?v=4.1.0" rel="stylesheet">
<script src="/Public/Theme1/js/echarts.js" type="text/javascript" charset="utf-8"></script>
</head>

<body class="fixed-sidebar full-height-layout gray-bg"

	style="overflow: hidden;">
<div style="max-width: : 60%;max-height: 80%;margin: 0 auto;display: none;background: rgba(0,0,0,0.3);position: fixed;padding: 10% 20%" class="zzc">11111</div>
<script>

	function  cs(){

		var zzc=document.getElementsByClassName("zzc")[0];

		zzc.style.display="block";

	}

	</script>
<div id="wrapper"> 
  
  <!--左侧导航开始-->
  
  <nav class="navbar-default navbar-static-side" role="navigation"

			style="background-color: #4c8ec0; border-right: 0px solid #ffffff">
    <div class="nav-close"> <i class="fa fa-times-circle"></i> </div>
    <div class="sidebar-collapse">
      <ul class="nav" id="side-menu">
        <li class="nav-header">
          <div class="dropdown profile-element">
            <?php if($rs_users["uImages"] == null): if($rs_admin["aSex"] == 1): ?><span><img

								alt="image" class="img-circle"

								src="/Public/Theme1/img/default_1.jpg" width="80" height="80" /></span>
                <?php else: ?>
                <span><img alt="image" class="img-circle"

								src="/Public/Theme1/img/default_2.jpg" width="80" height="80" /></span><?php endif; ?>
              <?php else: ?>
              <span><img alt="image" class="img-circle"

								src="/<?php echo ($rs_users["uImages"]); ?>" width="80" height="80" /></span><?php endif; ?>
            <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span

								class="clear"> <span class="block m-t-xs"><strong

										class="font-bold"><?php echo ($aUser); ?></strong></span> <span

									class="text-muted text-xs block"><?php echo ($aName); ?><b

										class="caret"></b></span> </span> </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs"

								style="z-index: 2000">
              <li><a href="/LoginTrue/ExitLogin">安全退出</a></li>
            </ul>
          </div>
          <div class="logo-element">栏目</div>
        </li>
        <?php if(($powersValue == 0) OR $powersValue[1] == 'A1'): ?><li><a href="#"> <span

							class="nav-label" style="background: url(/Public/Theme1/menu/ico/setsystem.png)no-repeat 2px center;">系统管理</span><span class="fa arrow"></span> </a>
            <ul class="nav nav-second-level">
              <li><a class="J_menuItem" href="/System/system"

								data-index="0">系统设置</a></li>
              <li><a class="J_menuItem" href="/Bak/index"

								data-index="0">数据库管理</a></li>
              <li><a class="J_menuItem" href="/Variables/add"

								data-index="0">添加变量</a></li>
              <li><a class="J_menuItem" href="/Variables/lists"

								data-index="0">配置变量</a></li>
              <li><a class="J_menuItem" href="/Manage/role">角色权限</a> </li>
              <li><a class="J_menuItem" href="/Manage/managelists">管理员列表</a> </li>
            </ul>
          </li><?php endif; ?>
        <?php if(($powersValue == 0) OR $powersValue[3] == 'A3'): ?><li><a href="#"><span

							class="nav-label" style="background: url(/Public/Theme1/menu/ico/setdept.png)no-repeat 2px center;">部门管理</span><span class="fa arrow"></span> </a>
            <ul class="nav nav-second-level">
              <li><a class="J_menuItem" href="/Department/lists">部门组织架构</a></li>
              <li><a class="J_menuItem"

								href="/Department/listsedit">部门信息列表</a></li>
              <li><a class="J_menuItem" href="/Department/add">添加一级部门</a></li>
              <li><a class="J_menuItem" href="/Department/twoadd">添加二级部门</a></li>
              <li><a class="J_menuItem" href="/Department/threeadd">添加三级部门</a></li>
            </ul>
          </li><?php endif; ?>
        <?php if(($powersValue == 0) OR $powersValue[2] == 'A2'): ?><li><a href="#"><span

							class="nav-label" style="background: url(/Public/Theme1/menu/ico/setstaff.png)no-repeat 2px center;">员工管理</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a class="J_menuItem" href="/Staff/lists">集团员工花名册</a>
              <li><a class="J_menuItem" href="/Staff/lists">全部员工列表</a> </li>
              <li><a class="J_menuItem" href="/Staff/joblists">在职员工列表</a> </li>
              <li><a class="J_menuItem" href="/Staff/nojoblists">离职员工列表</a> </li>
              <li><a class="J_menuItem" href="/Staff/backlists">离职员工黑名单</a> </li>
              <div class="hr-line-dashed"></div>
              <li><a class="J_menuItem" href="/Staff/add">录入员工信息</a> </li>
              <li><a class="J_menuItem" href="/Staff/import">导入员工信息</a> </li>
              <li><a class="J_menuItem" href="/Staff/export">导出员工资料</a> </li>
            </ul>
          </li><?php endif; ?>
        <?php if($morepowersValue == '0-0-0-0-B'): ?><li><a href="#"><span

							class="nav-label" style="background: url(/Public/Theme1/menu/ico/setstaff.png)no-repeat 2px center;">员工管理</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a class="J_menuItem"

								href="/Staff/lists/depId/<?php echo ($aDid); ?>"><?php echo ($rs_department["dName"]); ?>员工列表</a> </li>
              <li><a class="J_menuItem"

								href="/Staff/lists/depId/<?php echo ($aDid); ?>"><?php echo ($rs_department["dName"]); ?>在职员工列表</a> </li>
              <li><a class="J_menuItem"

								href="/Staff/lists/depId/<?php echo ($aDid); ?>"><?php echo ($rs_department["dName"]); ?>离职员工列表</a> </li>
              <li><a class="J_menuItem"

								href="/Staff/lists/depId/<?php echo ($aDid); ?>"><?php echo ($rs_department["dName"]); ?>黑名单列表</a> </li>
              <li><a class="J_menuItem"

								href="/Staff/add/depId/<?php echo ($aDid); ?>">添加<?php echo ($rs_department["dName"]); ?>员工</a> </li>
              <li><a class="J_menuItem" href="/Staff/import">导入<?php echo ($rs_department["dName"]); ?>信息</a> </li>
              <li><a class="J_menuItem" href="/Staff/export">导出<?php echo ($rs_department["dName"]); ?>资料</a> </li>
            </ul>
          </li><?php endif; ?>
        <?php if($morepowersValue != '0-0-0-0-B'): ?><li><a href="#"><span class="nav-label" style="background: url(/Public/Theme1/menu/ico/good.png)no-repeat 2px center;">绩效评分<!--<small style="color:#fce339"> <sup>new</sup></small>--></span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a class="J_menuItem" href="/StaffScore/index">员工绩效评分</a> </li>
              <li><a class="J_menuItem" href="/StaffScore/allscore">绩效评分记录</a> </li>
            </ul>
          </li><?php endif; ?>
        <li><a href="#"> <span class="nav-label" style="background: url(/Public/Theme1/menu/ico/hrku.png)no-repeat 2px center;">人才储备<!--<small style="color:#fce339"><sup>new</sup></small>--></span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a class="J_menuItem" href="/Hrinfo/index/statesId/0">人才库列表</a></li>
            <li><a class="J_menuItem" href="/PerfectInfo/addmain">人才简历录入</a></li>
            <li><a class="J_menuItem" href="/Hrinfo/index/statesId/1">未查看的简历<small style="color:#fce339"> <sup>Hot</sup></small></a> </li>
            <li><a class="J_menuItem" href="/Hrinfo/index/statesId/2">未录用的简历</a></li>
            <li><a class="J_menuItem" href="/Hrinfo/index/statesId/3">已录用的简历</a> </li>
            <li><a class="J_menuItem" href="/Hrinfo/index/statesId/4">已入职的简历</a> </li>
          </ul>
        </li>
        <li> <a href="#"> <span class="nav-label" style="background: url(/Public/Theme1/menu/ico/settarget.png)no-repeat 2px center;">月度目标</span><span class="fa arrow"></span> </a>
          <ul class="nav nav-second-level">
            <li><a class="J_menuItem" href="/Targets/models">模版编辑</a></li>
            <li><a class="J_menuItem" href="/Targets/monthadd">填写月度目标</a></li>
            <li><a class="J_menuItem" href="/Targets/monthlists">月度目标信息</a></li>
          </ul>
        </li>
        <li> <a href="#"> <span class="nav-label" style="background: url(/Public/Theme1/menu/ico/setactivity.png)no-repeat 2px center;">相关活动</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a class="J_menuItem" href="/Hdmenu/add">添加菜单</a> </li>
            <li><a class="J_menuItem" href="/Hdmenu/lists">编辑菜单</a> </li>
            <?php if(is_array($rs_hdmenu)): foreach($rs_hdmenu as $key=>$val_hdmenu): ?><li><a class="J_menuItem" href="/Huodong/lists/hdPid/<?php echo ($val_hdmenu["hdId"]); ?>"><?php echo ($val_hdmenu["hdName"]); ?></a> </li><?php endforeach; endif; ?>
          </ul>
        </li>
        <li><a href="#"> <span

							class="nav-label" style="background: url(/Public/Theme1/menu/ico/setstatistics.png)no-repeat 2px center;">数据统计</span><span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a class="J_menuItem" href="/Statistics/charts">综合数据统计</a> </li>
            <li><a class="J_menuItem" href="/Statistics/chartshr">人才储备统计</a> </li>
            <li><a class="J_menuItem"

								href="/Statistics/chartsdepartmentnum">部门员工统计</a></li>
            <li><a class="J_menuItem"

								href="/Statistics/chartsbasic/aDid/<?php echo ($aDid); ?>">员工基本统计</a> </li>
            <li><a class="J_menuItem"

								href="/Statistics/chartsstaffmonth/aDid/<?php echo ($aDid); ?>">员工月份统计</a> </li>
            <li><a class="J_menuItem"

								href="/Statistics/chartssex/aDid/<?php echo ($aDid); ?>">员工性别统计</a></li>
            <li><a class="J_menuItem"

								href="/Statistics/chartswork/aDid/<?php echo ($aDid); ?>">在职状态统计</a></li>
            <li><a class="J_menuItem"

								href="/Statistics/chartsblack/aDid/<?php echo ($aDid); ?>">黑白名单统计</a> </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
  
  <!--左侧导航结束--> 
  
  <!--右侧部分开始-->
  
  <div id="page-wrapper" class="gray-bg dashbard-1">
    <div class="row border-bottom">
      <nav class="navbar navbar-static-top" role="navigation"

					style="margin-bottom: 0">
        <div class="navbar-header"> <a class="navbar-minimalize btn" href="#"

							style="width: 190%; height: 50px; font-size: 18px; line-height: 36px; background: #4c8ec0; margin-left: 0; color: #ffffff; border-radius: 0;""><i

							class="fa fa-bars"></i><span

							style="margin-left: 20px; margin-right: 100px; color: #ffffff; font-weight: bold;"><?php echo ($rs_systemName["sName"]); ?></span> </a> </div>
        
        <!--<ul class="nav navbar-top-links navbar-right">

                        <li class="dropdown hidden-xs">

                            <a class="right-sidebar-toggle" aria-expanded="false">

                                <i class="fa fa-tasks"></i> 主题

                            </a>

                        </li>

                    </ul>--> 
        
      </nav>
    </div>
    <div class="row content-tabs">
      <button class="roll-nav roll-left J_tabLeft"></button>
      <nav class="page-tabs J_menuTabs">
        <div> <a style="color: #4c8ec0; " href="/Index/">系统主页</a> </div>
        <div> <a href="http://hr.qc7s.com" target="_bank" style="color:#ff0000; font-weight: bold">人才库储备</a> </div>
      </nav>
      <a href="/LoginTrue/ExitLogin"

					class="roll-nav roll-right J_tabExit"><i

					class="fa fa fa-sign-out"></i> 退出</a> </div>
    <div class="row J_mainContent" id="content-main">
      <iframe class="J_iframe" name="iframe0" width="100%" height="100%"

					src="/Index/welcome" frameborder="0"

					data-id="index_v1.html" seamless></iframe>
    </div>
    <div class="footer">
      <div class="pull-right J_iframe dropdown hidden-xs" name="iframe0"

					frameborder="0" data-id="index.html" seamless> <span style="color: #1690d8"> 
        
        <!-- 	<?php if($rs_glBNum > 0 OR $rs_nlBNum > 0): ?>--> 
        
        <?php echo ($ResultNowDay); ?> 
        
        <!-- 今天还是员工： <?php if($rs_glBNum > 0): if(is_array($rs_glB)): foreach($rs_glB as $key=>$val_glB): ?>[ <?php echo ($val_glB["stName"]); ?> ]<?php endforeach; endif; endif; ?> <?php if($rs_nlBNum > 0): if(is_array($rs_nlB)): foreach($rs_nlB as $key=>$val_nlB): ?>[ <?php echo ($val_nlB["stName"]); ?> ]<?php endforeach; endif; endif; ?> 的生日 <?php else: ?> --> 
        
        <?php echo ($ResultNowDay); ?>
        </iframe>
        
        <!--<?php endif; ?> --> 
        
      </div>
    </div>
  </div>
  
  <!--右侧部分结束--> 
  
  <!--右侧边栏开始-->
  
  <div id="right-sidebar">
    <div class="sidebar-container">
      <ul class="nav nav-tabs navs-3">
        <li class="active"><a data-toggle="tab" href="#tab-1"> <i

							class="fa fa-gear"></i> 主题 </a></li>
      </ul>
      <div class="tab-content">
        <div id="tab-1" class="tab-pane active">
          <div class="sidebar-title">
            <h3> <i class="fa fa-comments-o"></i> 主题设置 </h3>
            <small><i class="fa fa-tim"></i> 你可以从这里选择和预览主题的布局和样式，这些设置会被保存在本地，下次打开的时候会直接应用这些设置。</small> </div>
          <div class="skin-setttings">
            <div class="title">主题设置</div>
            <div class="setings-item"> <span>收起左侧菜单</span>
              <div class="switch">
                <div class="onoffswitch">
                  <input type="checkbox" name="collapsemenu"

											class="onoffswitch-checkbox" id="collapsemenu">
                  <label

											class="onoffswitch-label" for="collapsemenu"> <span

											class="onoffswitch-inner"></span> <span

											class="onoffswitch-switch"></span> </label>
                </div>
              </div>
            </div>
            <div class="setings-item"> <span>固定顶部</span>
              <div class="switch">
                <div class="onoffswitch">
                  <input type="checkbox" name="fixednavbar"

											class="onoffswitch-checkbox" id="fixednavbar">
                  <label

											class="onoffswitch-label" for="fixednavbar"> <span

											class="onoffswitch-inner"></span> <span

											class="onoffswitch-switch"></span> </label>
                </div>
              </div>
            </div>
            <div class="setings-item"> <span> 固定宽度 </span>
              <div class="switch">
                <div class="onoffswitch">
                  <input type="checkbox" name="boxedlayout"

											class="onoffswitch-checkbox" id="boxedlayout">
                  <label

											class="onoffswitch-label" for="boxedlayout"> <span

											class="onoffswitch-inner"></span> <span

											class="onoffswitch-switch"></span> </label>
                </div>
              </div>
            </div>
            <div class="title">皮肤选择</div>
            <div class="setings-item default-skin nb"> <span class="skin-name "> <a href="#" class="s-skin-0"> 默认皮肤 </a> </span> </div>
            <div class="setings-item blue-skin nb"> <span class="skin-name "> <a href="#" class="s-skin-1"> 蓝色主题 </a> </span> </div>
            <div class="setings-item yellow-skin nb"> <span class="skin-name "> <a href="#" class="s-skin-3"> 黄色/紫色主题 </a> </span> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!--右侧边栏结束--> 
  
</div>
<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script> 
<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script> 
<script src="/Public/Theme1/js/plugins/metisMenu/jquery.metisMenu.js"></script> 
<script src="/Public/Theme1/js/plugins/slimscroll/jquery.slimscroll.min.js"></script> 
<script src="/Public/Theme1/js/plugins/layer/layer.min.js"></script> 
<script src="/Public/Theme1/js/hplus.min.js?v=4.1.0"></script> 
<script type="text/javascript" src="/Public/Theme1/js/contabs.min.js"></script> 
<script src="/Public/Theme1/js/plugins/pace/pace.min.js"></script>
</body>
</html>