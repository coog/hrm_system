<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>详细资料</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">
<style>
	.btj,.btj1,.btj2{
	    font-size: 14px;
	    color: inherit;
	    display: block;
	    padding: 6px 12px;
	    border: 0;
	    position: absolute;
	    right: 0;
	    top: 0
	}
	@media screen and (max-width:640px){
		.btj,.btj1,.btj2{
	    font-size: 14px;
	    color: inherit;
	    display: block;
	    padding: 6px 12px;
	    border: 0;
	    position: absolute;
	    right: 30px;
	    top: -10px
	}
	}
	.p1{
		font-size: 14px;
	    color: inherit;
	    display: inline-block;
	    padding: 6px 12px;

	}
	.p0{
		padding: 0
	}
	.fam,.edus,.worker{
		position:relative;
	}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							<a href="/PerfectInfo/updatecv" style="color:#c8c9c9">基本资料 </a>
							<a href="/PerfectInfo/updatecvtwo" style="color:#3390cd; margin-left:25px;">详细资料</a>
							<a href="/PerfectInfo/updatecvthree" style="color:#c8c9c9; margin-left:25px;">相关附件</a>
						</h5>

					</div>

					<div class="ibox-content"">
						<form method="post" action="/PerfectInfo/UpdateCVTwoAction/hrId/<?php echo ($rs_hrreserve["hrId"]); ?>"
							class="form-horizontal" enctype="multipart/form-data">

							<div class="form-group">
								<label class="col-sm-1 control-label">体重</label>
								<div class="col-sm-6">
									<input type="text" name="hrWeight" id="hrWeight"
										placeholder="请输入体重，单位KG" value="<?php echo ($rs_hrreserve["hrWeight"]); ?>"
										class="form-control" >
								</div>
							</div>

							

							<div class="form-group">
								<label class="col-sm-1 control-label">QQ号码</label>
								<div class="col-sm-6">
									<input type="tel" name="hrQQ" id="hrQQ"
										placeholder="请输入QQ号码" value="<?php echo ($rs_hrreserve["hrQQ"]); ?>"
										class="form-control" >
								</div>
							</div>

							

							<div class="form-group">
								<label class="col-sm-1 control-label">身份证号码</label>
								<div class="col-sm-6">
									<input type="text" name="hrIDCard" id="hrIDCard"
										placeholder="请输入身份证号码" value="<?php echo ($rs_hrreserve["hrIDCard"]); ?>"
										class="form-control" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">政治面貌</label>
								<div class="col-sm-6">
									<input type="text" name="hrPoliticalIandscape" id="hrPoliticalIandscape"
										placeholder="请输入政治面貌，比如团员" value="<?php echo ($rs_hrreserve["hrPoliticalIandscape"]); ?>"
										class="form-control" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">所在城市</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="hrCity" id="hrCity"
										placeholder="请输入省/市/县" value="<?php echo ($rs_hrreserve["hrCity"]); ?>" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">家庭情况</label>
								<div class="col-sm-6 fam">
									<div class="section col-md-11 p0">
									<div class="col-sm-1 p1 ">Part1</div>
										<div id="input_box" class="col-sm-11">
											<input type="text" name="family" placeholder="家庭成员"   class="form-control"/>
											<input type="text" name="family2" placeholder="关系"   class="form-control"/>
											<input type="text" name="family3" placeholder="职业"   class="form-control"/>
											<input type="text" name="family4" placeholder="联系方式"   class="form-control"/>
										</div>
									</div>
										<input type="button" value="+" onclick="add_input('family','input_box','btj1',4,'section','fam')" class="btj1 col-sm-1"/>
									
							</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">教育背景</label>
								<div class="col-sm-6 edus">
										<div class="sedus col-md-11 p0">
									<div class="col-sm-1 p1 ">Part1</div>
										<div id="input_tbox" class="col-sm-11">
											<input type="text" name="edu" placeholder="日期"   class="form-control col-xs-12"/>
											<input type="text" name="edu2" placeholder="学校"   class="form-control col-xs-12"/>
											<input type="text" name="edu3" placeholder="专业"   class="form-control col-xs-12"/>
											<input type="text" name="edu4" placeholder="学历"   class="form-control col-xs-12"/>
											<input type="text" name="edu5" placeholder="所获证书" " class="form-control col-xs-12"/>
										</div>
									</div>
										<input type="button" value="+" onclick="add_input('edu','input_tbox','btj',5,'sedus','edus')" class="btj col-sm-1"/>
									
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-1 control-label">工作经历</label>
								<div class="col-sm-6 worker">
										<div class="swork col-md-11 p0">
											<div class="col-sm-1 p1">Part1</div>
											<div id="input_sbox" class="col-sm-11">
												<input type="text" name="family" placeholder="日期"   class="form-control"/>
												<input type="text" name="family2" placeholder="工作单位"   class="form-control"/>
												<input type="text" name="family3" placeholder="职位"   class="form-control"/>
												<input type="text" name="family4" placeholder="薪资"   class="form-control"/>
												<input type="text" name="family5" placeholder="离职原因"   class="form-control"/>
												<input type="text" name="family6" placeholder="证明人"   class="form-control"/>
												<input type="text" name="family7" placeholder="联系方式"   class="form-control"/>
											</div>
										</div>
										<input type="button" value="+" onclick="add_input('work1','input_sbox','btj2',7,'swork','worker')" class="btj2 col-sm-1"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-1 control-label">掌握技能</label>
								<div class="col-sm-6">
									<textarea class="input-text form-control" id="hrJineng"
								name="hrJineng" placeholder="列举你擅长的技能，比如会开车，写程序" rows="1"><?php echo ($rs_hrreserve["hrJineng"]); ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">项目经历</label>
								<div class="col-sm-6">
									<textarea class="input-text form-control" id="hrWorks"
								name="hrWorks" placeholder="列举你的项目经历" rows="3"><?php echo ($rs_hrreserve["hrWorks"]); ?></textarea>
								</div>
							</div>

							

							<div class="form-group">
								<label class="col-sm-1 control-label">自我评价</label>
								<div class="col-sm-6">
									<textarea class="input-text form-control" id="hrPingjia"
								name="hrPingjia" placeholder="对自己的综合评价" rows="3"><?php echo ($rs_hrreserve["hrPingjia"]); ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">相关备注</label>
								<div class="col-sm-6">
									<textarea class="input-text form-control" id="hrRemarks"
								name="hrRemarks" placeholder="比如何时能到岗" rows="3"><?php echo ($rs_hrreserve["hrRemarks"]); ?></textarea>
								</div>
							</div>
						
							
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									<a href="/PerfectInfo/updatecv" style="margin-left:15px; color:#1270ae; font-weight:bold">上一步</button>
									<button class="btn btn-primary" type="submit" style="margin-left:15px;">保 存</button>
									<a href="/PerfectInfo/updatecvthree" style="margin-left:15px; color:#1270ae; font-weight:bold">下一步</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>
	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
    <script type="text/javascript">
										function add_input(name1,idname,cname,dil,secname,pjn) 
										{
										var seclen=document.getElementsByClassName(secname).length;
										var pj=document.getElementsByClassName(pjn)[0];
										var ib = document.getElementById(idname); 
										var pn = document.getElementsByClassName("p1")[0]; 
										var btj =document.getElementsByClassName(cname)[0];
										var ibl = document.getElementById(idname).getElementsByTagName("input");
										var il = ib.childNodes.length;
										var arr1 =[];
										var p_arr=[];
										for(var i=0;i<dil;i++){
										var input_file = document.createElement("input");			
										file_name = name1 + (il+i-dil);
										input_file.setAttribute("type","text");
										input_file.setAttribute("class",'form-control');
										p_arr.push(ibl[i].getAttribute("placeholder"));
										arr1.push(input_file);
										}
										
										var ibc=document.createElement("div");
										var sec=document.createElement("div");
										var pnc=document.createElement("div");
										pnc.innerText = 'Part'+(seclen+1);
										pnc.setAttribute("class","col-sm-1 p1");
										ibc.setAttribute("id",(idname+seclen));
										ibc.setAttribute("class","col-sm-11");
										sec.setAttribute("class",(secname)+" "+"col-sm-11 p0");
										sec.append(pnc);
										for(var j=0;j<arr1.length;j++){
											ibc.append(arr1[j])
											arr1[j].setAttribute("placeholder",p_arr[j]);
										}
										sec.append(ibc);
										pj.append(sec);
										console.log(seclen);
										if(seclen==2){
											btj.style.display="none";
										}
										// var xz =3*dil;
										// if(ibl.length==xz){
										// 	btj.style.display="none";
										// }
										// for(var k=0;k<il;k++){
										// 	ibl[k].classList.add("form-control");	
										// }
										}		
										// function add_input(name1,idname,cname,dil) 
										// { 
										// var ib = document.getElementById(idname); 
										// var pn = document.getElementsByClassName("p1")[0]; 
										// var btj =document.getElementsByClassName(cname)[0];
										// var ibl = document.getElementById(idname).getElementsByTagName("input");
										// var il = ib.childNodes.length;
										// var arr1 =[];
										// for(var i=0;i<dil;i++){
										// var input_file = document.createElement("input");			
										// file_name = name1 + (il+i-dil);
										// input_file.setAttribute("type","text");
										// input_file.setAttribute("name",file_name);
										// arr1.push(input_file);
										// }
										// var pnc=document.createElement("")
										// for(var j=0;j<arr1.length;j++){
										// 	ib.append(arr1[j])
										// }
										// var xz =3*dil;
										// if(ibl.length==xz){
										// 	btj.style.display="none";
										// }
										// for(var k=0;k<il;k++){
										// 	ibl[k].classList.add("form-control");	
										// }
										// }		
	</script>
</body>

</html>