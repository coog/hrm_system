<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>会员中心首页</title>

    <link rel="shortcut icon" href="favicon.ico"> <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <!-- Morris -->
    <link href="/Public/Theme1/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/Public/Theme1/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/Public/Theme1/css/style1.min.css?v=4.1.0" rel="stylesheet">
    <style type="text/css">
    	/**{padding: 0 ;margin:0;};*/
    		li{
	    		list-style-type:none;
	    		padding: 0;
	    		margin: 0;
	    		transition: all 0.2s;
	    	}
	    	li:hover{
	    		background:rgb(76,142,192);
	    	}
	    	ul{padding: 0;}
	    	
			p.Notice{
				margin:15px 0 0 20px ;
				padding-bottom:15px ;
				font-size: 22px;
				font-family: "微软雅黑";
				color: black;
				font-weight: 600;
				border-bottom: 1px dotted #ccc;
			}
			ul>li>a>p{
				color: #000000;
				margin-left:20px;
				padding: 10px 0;
				border-bottom: 1px solid #ccc;
				transition: all 0.1s;
				margin-bottom: 0;
			}
			ul>li>a>p>span{
				float: right;
				margin-right: 10px;
			}
			ul>li>a>p:hover{
				color: rgba(255,255,255,0.9);
			}
			.Todo-list{
				margin: 20px 0;
	    	}

	    	.area_l{
	    		padding:20px 0;
	    		font-size: 18px;
	    		transition: all .5s; 
	    		margin: 10px 0;z-index:100;
	    	}
	    	.mainbox{
	    		height: 420px;margin: 20px 0 0 20px;background: #FFFFFF;
				box-shadow: 0 0 5px #ccc;overflow: hidden;
	    	}
	    	@media screen and (max-width: 640px){
	    		.mainbox{
	    		height: 420px;margin: 20px 0 0 0;background: #FFFFFF;
				box-shadow: 0 0 5px #ccc;
	    	}
	    	}
	    	.area_l:hover{
	    		
	    	}
	    	.area_gz{
	    		color: red;
	    	}
	    	.area_zs{
	    		color: blue;
	    	}
	    	.area_wh{
	    		color: deeppink;
	    	}
	    	.area_zz{
	    		color: darkviolet
	    	}
	    	.area_ts{
	    		color: #AA5500;
	    	}
	    	.area_cd{
	    		color: green;
	    	}
	    	/* Antiman */
.button--antiman {
	background: none;
	border: none;
	height: 60px;
	width: 100px
}
.button--antiman > span {
	padding-left: 0.35em;
}
.button--antiman::before,
.button--antiman::after {
	content: '';
	z-index: -1;
	border-radius: inherit;
	pointer-events: none;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	-webkit-backface-visibility: hidden;
	-webkit-transition: -webkit-transform 0.3s, opacity 0.3s;
	transition: transform 0.3s, opacity 0.3s;
	-webkit-transition-timing-function: cubic-bezier(0.75, 0, 0.125, 1);
	transition-timing-function: cubic-bezier(0.75, 0, 0.125, 1);
}
.button--antiman::before {
	border: 2px solid #37474f;
	opacity: 0;
	-webkit-transform: scale3d(1.2, 1.2, 1);
	transform: scale3d(1.2, 1.2, 1);
}
/*.button--antiman::after {
	border: 2px solid #37474f;
	opacity: 0;
	-webkit-transform: scale3d(1.2, 1.2, 1);
	transform: scale3d(1.2, 1.2, 1);
}*/
.button--antiman:hover::before {
	opacity: 1;
	-webkit-transform: scale3d(1, 1, 1);
	transform: scale3d(1, 1, 1);
}
/*.button--antiman:hover::after {
	opacity: 0;
	-webkit-transform: scale3d(0.8, 0.8, 1);
	transform: scale3d(0.8, 0.8, 1);
}*/


	    </style>
</head>

<body class="gray-bg"> <!--style="background-image:url(/Public/Theme1/img/bg.jpg);"-->
	<div style="width:100%;height: 100%;margin: 0 auto;display: none;background: rgba(0,0,0,0.3);position: fixed;padding: 0 10%;z-index: 5000" class="zzc">
		<div style="font-size: 25px;margin: 0 -30px 0 0;color: #ffffff;float: right;cursor:pointer" class="exit_zzc">
			<i class="fa fa-times" aria-hidden="true"></i>
		</div>
		 <iframe class="J_iframe" name="iframe0" width="100%" height="100%"
							src="/Hrinfo/main" frameborder="0"
							data-id="index_v1.html" scrolling="auto" seamless>
		 </iframe> 
		</div>
    <div class="wrapper wrapper-content">

    	<div class="row">
       <div id="pb_data" class="col-md-4 col-sm-12 col-xs-12 mainbox">
       	
       </div>
       <!--<div id="REP_sta" class="col-md-8 col-sm-6 col-xs-12 " style="height: 420px;">
       		
       </div>-->
       <div class="col-md-3 col-sm-12 area_m col-xs-12 mainbox" style="text-align: center;">
       	<p class="Notice" style="margin-left: 0;"><a href="#">员工地区划分</a></p>
       	<!--<div class="row">-->
		<a href="" class="area_gz"><div class="col-md-6 col-xs-6 area_l"><button class="button button--antiman button--round-l button--text-medium"><span>广州</span></button></div></a>
       	<!--<a href="" class="area_gz"><div class="col-md-6  area_l">广州</div></a>-->
       	<a href="" class="area_zs"><div class="col-md-6 col-xs-6 area_l"><button class="button button--antiman button--round-l button--text-medium"><span>中山</span></button></div></a>
       	<a href="" class="area_wh"><div class="col-md-6 col-xs-6 area_l"><button class="button button--antiman button--round-l button--text-medium"><span>武汉</span></button></div></a>
       	<a href="" class="area_zz"><div class="col-md-6 col-xs-6 area_l"><button class="button button--antiman button--round-l button--text-medium"><span>郑州</span></button></div></a>
       	<a href="" class="area_ts"><div class="col-md-6 col-xs-6 area_l"><button class="button button--antiman button--round-l button--text-medium"><span>唐山</span></button></div></a>
       	<a href="" class="area_cd"><div class="col-md-6 col-xs-6 area_l"><button class="button button--antiman button--round-l button--text-medium"><span>成都</span></button></div></a>
       	<!--</div>-->
       </div><div id="data_dbsx" class="col-md-4 col-sm-12 col-xs-12 mainbox" >
			<div class="col-md-2 col-sm-2 col-xs-2" style="border-right:1px dotted #ccc ;">
				<p><a><span style="height: 100%;font-size: 22px;writing-mode: tb-rl;text-align: center;letter-spacing: 1em;margin-right:15px;font-weight: 600;">待办事项</span></a></p>
			</div>
			<div class="col-md-10 col-sm-10 col-xs-10 Todo-list">
				<ul>
					<li><a href="#"><p>xxx面试简历查看<span>2018-8-28</span></p></a></li>
					<li><a href="#"><p>xxx绩效评分<span>2018-8-28</span></p></a></li>
					<li><a href="#"><p>xxx面试简历查看<span>2018-8-28</span></p></a></li>
					<li><a href="#"><p>xxx面试简历查看<span>2018-8-28</span></p></a></li>
					<li><a href="#"><p>xxx面试简历查看<span>2018-8-28</span></p></a></li>
					<li><a href="#"><p>xxx面试简历查看<span>2018-8-28</span></p></a></li>
				</ul>
			</div>
		</div>
      
    	</div>
       <div class="row">
		 <div class="col-md-3 col-sm-12 col-xs-12 mainbox">
       	<p class="Notice" ><a href="#">员工发展培训</a></p>
			<ul>
				<li><a href="#"><p>人事晋升</p></a></li>
				<li><a href="#"><p>公司福利</p></a></li>
			</ul>
       </div>
		<div id="data_analysis" class="col-md-3 col-sm-12 col-xs-12 mainbox" >
			<p class="Notice"><a href="#">员工档案</a></p>
			<ul>
				<li><a href="#"><p>xxxxxxxxx</p></a></li>
				<li><a href="#"><p>aaaaaaaaa</p></a></li>
				<li><a href="#"><p>bbbbbbbbb</p></a></li>
				<li><a href="#"><p>ccccccccc</p></a></li>
				<li><a href="#"><p>ddddddddd</p></a></li>
				<li><a href="#"><p>eeeeeeeee</p></a></li>
			</ul>
			
		</div>

		<div id="file_group" class="col-md-5 col-sm-12 col-xs-12 mainbox">
			<p class="Notice"><a href="#">集团大事件</a></p>
			<ul>
				<li onclick="cs()"><a href="#"><p>关于xxx同志的人事变动任命<span>2018-8-28</span></p></a></li>
				<li><a href="#"><p>关于xxx同志的人事变动任命<span>2018-8-28</span></p></a></li>
				<li><a href="#"><p>关于xxx同志的人事变动任命<span>2018-8-28</span></p></a></li>
				<li><a href="#"><p>关于xxx同志的人事变动任命<span>2018-8-28</span></p></a></li>
				<li><a href="#"><p>关于xxx同志的人事变动任命<span>2018-8-28</span></p></a></li>
				<li><a href="#"><p>关于xxx同志的人事变动任命<span>2018-8-28</span></p></a></li>
			</ul>
			
		</div></div>
    </div>

    
    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="/Public/Theme1/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="/Public/Theme1/js/demo/peity-demo.min.js"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script src="/Public/Theme1/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="/Public/Theme1/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/Public/Theme1/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/Public/Theme1/js/plugins/easypiechart/jquery.easypiechart.js"></script>
    <script src="/Public/Theme1/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="/Public/Theme1/js/demo/sparkline-demo.min.js"></script>
    <script src="/Public/Theme1/js/echarts.js" type="text/javascript" charset="utf-8"></script>
    <script src="/Public/Theme1/js/charts_index.js" type="text/javascript" charset="utf-8"></script>
    <script>
	function  cs(){
		var zzc=document.getElementsByClassName("zzc")[0];
		zzc.style.display="block";
	}
	$(".exit_zzc").click(function(event){
		event.stopPropagation();
		$(".zzc").hide();
		// $(".exit_zzc").hide();
		});

	</script>
</body>

</html>