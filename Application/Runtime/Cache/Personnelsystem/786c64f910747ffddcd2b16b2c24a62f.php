<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>员工月度评分历史记录</title>

<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">

<!-- Data Tables -->
<link
	href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>员工月度评分历史记录</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">

						<table
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>ID</th>
									<th >员工姓名</th>
									<th >性别</th>
									<th>月度得分</th>
									<th >评分日期</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($rs_h)): foreach($rs_h as $key=>$val_h): ?><tr>
									<td><?php echo ($val_h["hirsId"]); ?></td>
									<?php $staff=M("staff"); $stId=$val_h["hirsStId"]; $stName=$staff->where("stId={$stId}")->field("stName,stSex")->find(); $ymhistory=explode("-",$val_h["hirsMonthScore"]); $ymH=$ymhistory[0]."-".$ymhistory[1]; $nowYm=date("Y-m"); $nowymState=-1; if($ymH==$nowYm){ $nowymState=1; }else{ $nowymState=0; } ?>
									<td><?php echo ($stName["stName"]); ?></td>
									<td><?php if($stName["stSex"] == 1): ?>男<?php else: ?>女<?php endif; ?></td>
									<?php if($nowymState == 1): ?><td style="color:#ff0000;"><?php echo ($val_h["hirsScore"]); ?></td>
									<?php else: ?>
									<td ><?php echo ($val_h["hirsScore"]); ?></td><?php endif; ?>
									<td ><?php echo ($val_h["hirsMonthScore"]); ?></td>
								</tr><?php endforeach; endif; ?>
							</tbody>

						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script
		src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
	<script
		src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script
		src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script>
        $(document).ready(function(){$(".dataTables-example").dataTable();var oTable=$("#editable").dataTable();oTable.$("td").editable("../example_ajax.php",{"callback":function(sValue,y){var aPos=oTable.fnGetPosition(this);oTable.fnUpdate(sValue,aPos[0],aPos[1])},"submitdata":function(value,settings){return{"row_id":this.parentNode.getAttribute("id"),"column":oTable.fnGetPosition(this)[2]}},"width":"90%","height":"100%"})});function fnClickAddRow(){$("#editable").dataTable().fnAddData(["Custom row","New row","New row","New row","New row"])};
        alert(typeof dataTable());
    </script>

</body>

</html>