<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>人才储备库</title>
     <link rel="shortcut icon" href="favicon.ico"> 
     <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
     <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style1.min.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
        <?php if(is_array($rs_hrreserves)): $i = 0; $__LIST__ = $rs_hrreserves;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val_hrreserves): $mod = ($i % 2 );++$i;?><!--年龄计算-->
        <?php $nowyear=date("Y"); $pyear=explode("-",$val_hrreserves["hrBirthdate"]); $age=$nowyear-$pyear[0]; ?>
        <!--学历的处理-->
        <?php $xueliInfo=""; $xueliId=$val_hrreserves["hrDegrees"]; $variables=M("variables"); $rs_variables=$variables->where("vId=1")->find(); $xueli=explode("|",$rs_variables["vVariablesVal"]); foreach($xueli as $key=>$valxueli){ if($key==$xueliId){ $xueliInfo=$valxueli; } } ?>

        <!--应聘职位的处理-->
            <?php $zhiwuInfo=""; $zhiwuId=$val_hrreserves["hrDuties"]; $variables=M("variables"); $rs_variables=$variables->where("vId=3")->find(); $zhiwu=explode("|",$rs_variables["vVariablesVal"]); foreach($zhiwu as $key=>$valzhiwu){ if($key==$zhiwuId){ $zhiwuInfo=$valzhiwu; } } ?>

        <!--待遇的处理-->
        <?php if($val_hrreserves["hrBirthdate"] != null AND $val_hrreserves["hrSalary"] != null): ?><div class="col-sm-4">
                <div class="contact-box">
                    <a href="/Hrinfo/main/hrId/<?php echo ($val_hrreserves["hrId"]); ?>">
                        <div class="col-sm-4">
                            <div class="text-center">
                            <?php if($val_hrreserves["hrPhoto"] == null): if($val_hrreserves["hrSex"] == 1): ?><img style="height:140px;" alt="image" class="img-circle m-t-xs img-responsive" src="/Public/Theme1/img/hrsex_1.png" />
                                <?php else: ?> 
                                   <img style="height:140px;" alt="image" class="img-circle m-t-xs img-responsive" src="/Public/Theme1/img/hrsex_2.png" /><?php endif; ?> 
                            <?php else: ?> 
                                <img style="height:140px;" alt="image" class="img-circle m-t-xs img-responsive" src="/<?php echo ($val_hrreserves["hrPhoto"]); ?>" /><?php endif; ?>

                                <div class="m-t-xs font-bold"></div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <h3><strong><?php echo ($val_hrreserves["hrName"]); ?></strong></h3>
                            <p><i class="fa fa-map-marker"></i> <?php if($val_hrreserves["hrSex"] == 1): ?>男 <?php else: ?>女<?php endif; ?> <?php echo ($val_hrreserves["hrHeight"]); ?>CM</p>
                            <address>
                            学历：<strong><?php echo ($xueliInfo); ?></strong><p></p>
                            电话：<?php echo ($val_hrreserves["hrTel"]); ?><p></p>
                            生日：<a href=""><?php echo ($val_hrreserves["hrBirthdate"]); ?></a>
                            <abbr title="年龄按虚岁计算">现年:</abbr>  <?php echo ($age); ?> 岁<p></p>
                            应聘岗位：<span style="color:#ff0000"><?php echo ($zhiwuInfo); ?></span> <?php if($val_hrreserves["hrSalary"] != 0): ?>期望月薪：<span style="color:#ff0000"><?php echo ($val_hrreserves["hrSalary"]); ?>元 </span><?php else: ?>月薪要求：<span style="color:#0000ff">不限/面议</span><?php endif; ?>
                        </address>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </div>
            </div><?php endif; endforeach; endif; else: echo "" ;endif; ?>

          
        </div>
    </div>

    <!-- 全局js -->
    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <!-- 自定义js -->
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>

    <script>
        $(document).ready(function () {
            $('.contact-box').each(function () {
                animationHover(this, 'pulse');
            });
        });
    </script>

</body>

</html>