<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>办理员工<?php echo ($rs_hrreserve["hrName"]); ?>的入职手续</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>办理 <?php echo ($rs_hrreserve["hrName"]); ?> 的入职手续</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-wrench"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<form method="post"
							action="/Hrinfo/successFeedBackAction/hrId/<?php echo ($rs_hrreserve["hrId"]); ?>"
							class="form-horizontal" id="form-admin-add">
							<div class="form-group">
								<label class="col-sm-2 control-label">入职人：</label>
								<div class="col-sm-10">
									<input type="text" style="width: 100%"
										value="<?php echo ($rs_hrreserve["hrName"]); ?>" 
										class="form-control" readonly />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">联系电话：</label>
								<div class="col-sm-10">
									<input type="text" 
										
										value="<?php echo ($rs_hrreserve["hrTel"]); ?>"
										 class="form-control" readonly />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">入职编号：</label>
								<div class="col-sm-10">
									<input type="number" value="<?php echo ($number); ?>" name="stNum" class="form-control"  />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">入职日期：</label>
								<div class="col-sm-10">
									<input type="date" value="<?php echo ($successDate); ?>" name="stEntryDate" class="form-control"  />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">入职部门</label>
								<div class="col-sm-10">
									<select class="chosen-select form-control" size="1"
										name="stDid" id="stDid" required>

										<option value="" selected>请选择员工所在部门</option>
										<?php if(is_array($rs_department)): foreach($rs_department as $key=>$val_department): if($val_department["dPid"] == 0 AND $val_department["dPsid"] == 0): ?><option style="color: #f87ca8" value="<?php echo ($val_department["dId"]); ?>"><?php echo ($val_department["dName"]); ?></option>
										<?php elseif($val_department["dPid"] != 0 AND $val_department["dPsid"] == 0): ?>
										<?php $dPid=$val_department["dPid"]; $department=M("department"); $rsp=$department->where("dId={$dPid}")->find(); ?>
										<option style="color: #44a5e4" value="<?php echo ($val_department["dId"]); ?>"><?php echo ($rsp["dName"]); ?>
											-> <?php echo ($val_department["dName"]); ?></option>

										<?php elseif($val_department["dPid"] == 0 AND $val_department["dPsid"] != 0): ?>
										<?php $dPsid=$val_department["dPsid"]; $department=M("department"); $rsps=$department->where("dId={$dPsid}")->find(); $rsPsPid=$rsps["dPid"]; $rsPspe=$department->where("dId={$rsPsPid}")->find(); ?>
										<option style="color: #067b14" value="<?php echo ($val_department["dId"]); ?>"><?php echo ($rsPspe["dName"]); ?>
											-> <?php echo ($rsps["dName"]); ?> -> <?php echo ($val_department["dName"]); ?></option><?php endif; endforeach; endif; ?>

									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">职称</label>
								<div class="col-sm-10">
									<select class="chosen-select form-control" size="1"
										name="stPositionalTitles" id="stPositionalTitles" required>

										<option value="" selected>请选择职称</option>
										<?php if(is_array($zhicheng)): foreach($zhicheng as $zhichengk=>$valzhicheng): ?><option value="<?php echo ($zhichengk); ?>"><?php echo ($valzhicheng); ?></option><?php endforeach; endif; ?>

									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">应聘岗位</label>
								<div class="col-sm-10">
								<select class="chosen-select form-control" size="1"
								name="stDuties" id="stDuties" required>
								<option value="" selected>请选择应聘的职务</option>
								<?php if(is_array($zhiwu)): foreach($zhiwu as $zhiwuk=>$valzhiwu): if($rs_hrreserve["hrDuties"]!=null){ $xzzhiwu=""; if($rs_hrreserve["hrDuties"]==$zhiwuk){ $xzzhiwu="selected"; }else{ $xzzhiwu=""; } } ?>
								<option value="<?php echo ($zhiwuk); ?>"<?php echo ($xzzhiwu); ?>><?php echo ($valzhiwu); ?></option><?php endforeach; endif; ?>

								</select>
								</div>
							</div>

						
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-2">
									<button class="btn btn-primary" type="submit">办理入职</button>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/messages_zh.min.js"></script>



	<script type="text/javascript"
		src="/Public/Theme1/check/js/validate-methods.js"></script>




	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>

	<script type="text/javascript">
	$(function(){
	$("#form-admin-add").validate({
		rules:{
			
            vVariables:{
                required:true,
            },
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit();
			var index = parent.layer.getFrameIndex(window.name);
			parent.$('.btn-refresh').click();
			parent.layer.close(index);
		}
	});
});
</script>


</body>

</html>