<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo ($rs_hrreserve["hrName"]); ?>的简历反馈</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5><?php echo ($rs_hrreserve["hrName"]); ?>的简历反馈</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-wrench"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<form method="post"
							action="/Hrinfo/feedBackAction/hrId/<?php echo ($rs_hrreserve["hrId"]); ?>"
							class="form-horizontal" id="form-admin-add">
							<div class="form-group">
								<label class="col-sm-2 control-label">求职人：</label>
								<div class="col-sm-10">
									<input type="text" style="width: 100%"
										value="<?php echo ($rs_hrreserve["hrName"]); ?>" 
										class="form-control" readonly />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">联系电话：</label>
								<div class="col-sm-10">
									<input type="text" 
										
										value="<?php echo ($rs_hrreserve["hrTel"]); ?>"
										 class="form-control" readonly />
								</div>
							</div>


							<div class="form-group">
								<label class="col-sm-2 control-label">是否录用</label>

								<div class="col-sm-10">
									<label
										class="checkbox-inline"> <input type="radio" value="1"
										name="hrStateSuccess" checked> 录用
									</label> <label class="checkbox-inline"> <input type="radio"
										value="0" name="hrStateSuccess"> 拒绝
									</label>

								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label">相关说明</label>
								<div class="col-sm-10">
									<textarea class="input-text form-control" name="hrStateInfo"
										id="hrStateInfo" rows="8" placeholder="比如：拒绝录用原因/何时来公司办理入职手续，乘车路线，人事联系电话等" required ></textarea>

								</div>
							</div>



							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-4 col-sm-offset-2">
									<button class="btn btn-primary" type="submit">提交</button>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

	<script type="text/javascript"
		src="/Public/Theme1/check/js/messages_zh.min.js"></script>



	<script type="text/javascript"
		src="/Public/Theme1/check/js/validate-methods.js"></script>




	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>

	<script type="text/javascript">
	$(function(){
	$("#form-admin-add").validate({
		rules:{
			
            vVariables:{
                required:true,
            },
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit();
			var index = parent.layer.getFrameIndex(window.name);
			parent.$('.btn-refresh').click();
			parent.layer.close(index);
		}
	});
});
</script>


</body>

</html>