<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>部门网格化</title>

<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">

<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content  animated fadeInRight">
		<div class="row">
			<div class="col-sm-4">
				<div class="ibox">
					<div>
						<h3 style="text-align: center;">一级部门</h3>
						<ul class="sortable-list connectList agile-list">
							<?php if(is_array($rs_departmentLists)): foreach($rs_departmentLists as $k=>$val_departmentLists): if($k%2==0){ $floattype="left"; }else{ $floattype="right"; } ?> <a
								href="/Department/listsinfo/dId/<?php echo ($val_departmentLists["dId"]); ?>">
								<li style="background: <?php echo ($color[0]["$k"]); ?>;color: #ffffff;
									width: 49%; height: 200px; float: <?php echo ($floattype); ?>"> <span
									style="font-size: 18px; font-weight: bold; line-height: 180px; margin: 0 auto; text-align: center; display: block; color: #ffffff;"><?php echo ($val_departmentLists["dName"]); ?></span>

							</li>
							</a><?php endforeach; endif; ?>

						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="ibox">
					<div>
						<h3 style="text-align: center;">二级部门</h3>
						<ul class="sortable-list connectList agile-list">
							<?php if(is_array($rs_departmentLists2)): foreach($rs_departmentLists2 as $k2=>$val_departmentLists2): if($k2%2==0){ $floattype="left"; }else{ $floattype="right"; } $departmentLists=M("department"); $dPid=$val_departmentLists2["dPid"]; $rs_d1=$departmentLists->where("dId={$dPid}")->find(); ?> <a
								href="/Department/listsinfo/dId/<?php echo ($val_departmentLists2["dId"]); ?>">
								<li style="background: <?php echo ($color1[0]["$k2"]); ?>;color:
									#ffffff; width: 49%; height: 140px; float: <?php echo ($floattype); ?>"> <span
									style="font-size: 16px; font-weight: bold; margin: 0 auto; text-align: center; display: block;"><br><?php echo ($rs_d1["dName"]); ?>

										<br> ↓ <br> <span
										style="font-size: 16px; color: #ffffff"><?php echo ($val_departmentLists2["dName"]); ?></span></span>
							</li>
							</a><?php endforeach; endif; ?>

						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="ibox">
					<div>

						<h3 style="text-align: center;">三级部门</h3>
						<ul class="sortable-list connectList agile-list">
							<?php if(is_array($rs_departmentLists3)): foreach($rs_departmentLists3 as $k3=>$val_departmentLists3): if($k3%2==0){ $floattype="left"; }else{ $floattype="right"; } $departmentLists=M("department"); $dPsid=$val_departmentLists3["dPsid"]; $rs_d2=$departmentLists->where("dId={$dPsid}")->find(); $dPid1=$rs_d2["dPid"]; $rs_d1=$departmentLists->where("dId={$dPid1}")->find(); ?> <a
								href="/Department/listsinfo/dId/<?php echo ($val_departmentLists3["dId"]); ?>">
								<li style="background: <?php echo ($color2[0]["$k3"]); ?>;color:
									#ffffff; width: 49%; height: 120px; float: <?php echo ($floattype); ?>"> <span
									style="font-size: 14px; font-weight: bold; line-height: 20px; margin: 0 auto; text-align: center; display: block;">
										<?php echo ($rs_d1["dName"]); ?> <br> ↓ <br> <?php echo ($rs_d2["dName"]); ?> <br>
										↓ <br> <?php echo ($val_departmentLists3["dName"]); ?>
								</span>
							</li>
							</a><?php endforeach; endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/jquery-ui-1.10.4.min.js"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script>
        $(document).ready(function(){$(".sortable-list").sortable({connectWith:".connectList"}).disableSelection()});
    </script>
</body>

</html>