<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>人才库列表</title>

<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">

<!-- Data Tables -->
<link
	href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet">

<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>人才库列表</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i>
							</a> <a class="close-link"> <i class="fa fa-times"></i>
							</a>
						</div>
					


						<div class="ibox-content">
							<table
								class="table table-striped table-bordered table-hover dataTables-example">

								<thead>
									<tr>
										
										<th>ID</th>
										<th>姓名</th>
										<th>性别</th>
										<th>年龄</th>
										<th class="dropdown hidden-xs">学历</th>
										<th class="dropdown hidden-xs">手机</th>
										
										<th class="dropdown hidden-xs">状态</th>
										<th class="dropdown hidden-xs">详细资料</th>
									</tr>
								</thead>
								<tbody>
									<?php if(is_array($rs_hrreserves)): foreach($rs_hrreserves as $key=>$val_hrreserves): ?><tr class="text-c">
										

										<td><?php echo ($val_hrreserves["hrId"]); ?></td>
										<td><?php echo ($val_hrreserves["hrName"]); ?></td>
										<?php if($val_hrreserves["hrSex"] == 1): ?><td>男</td>
										<?php else: ?>
										<td>女</td><?php endif; ?>
										<?php $nowTime=time(); $hrBirthdate=strtotime($val_hrreserves["hrBirthdate"]); $hrAge=floor((($nowTime-$hrBirthdate)/86400)/365); ?>
										<td><?php echo ($hrAge); ?> 岁</td>

										<?php $xueliInfo=""; $xueliId=$val_hrreserves["hrDegrees"]; $variables=M("variables"); $rs_variables=$variables->where("vId=1")->find(); $xueli=explode("|",$rs_variables["vVariablesVal"]); foreach($xueli as $key=>$valxueli){ if($key==$xueliId){ $xueliInfo=$valxueli; } } ?>
										<td class="dropdown hidden-xs"><?php echo ($xueliInfo); ?></td>
										<td class="dropdown hidden-xs"><?php echo ($val_hrreserves["hrTel"]); ?></td>
										
										<?php if($val_hrreserves["hrState"] == 0): ?><td class="dropdown hidden-xs"><span
											class="label label-error radius">未查看</span></td>

										<?php elseif($val_hrreserves["hrState"] == 1 AND $val_hrreserves["hrStateSuccess"] == 0): ?>
										<td class="dropdown hidden-xs"><span
											class="label label-error radius" style="background:#296efc; color:#ffffff">未通过</span></td>
										<?php elseif($val_hrreserves["hrState"] == 1 AND $val_hrreserves["hrStateSuccess"] == 1 AND $val_hrreserves["hrStateSuccessJob"] == 0): ?>
										<td class="dropdown hidden-xs"><span
											class="label label-success radius" style="background: #01ada7">已通过</span></td>
										<?php elseif($val_hrreserves["hrState"] == 1 AND $val_hrreserves["hrStateSuccess"] == 1 AND $val_hrreserves["hrStateSuccessJob"] == 1): ?>
										<td class="dropdown hidden-xs"><span
											class="label label-success radius" style="background: #ff0000">已入职</span></td><?php endif; ?>

										<td class="td-manage dropdown hidden-xs">
											<a href="/Hrinfo/main/hrId/<?php echo ($val_hrreserves['hrId']); ?>">查看</a>

										</td>
									</tr><?php endforeach; endif; ?>
								</tbody>

							</table>
							
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script
		src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
	<script
		src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
	<script
		src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script>
        $(document).ready(function(){$(".dataTables-example").dataTable();var oTable=$("#editable").dataTable();oTable.$("td").editable("../example_ajax.php",{"callback":function(sValue,y){var aPos=oTable.fnGetPosition(this);oTable.fnUpdate(sValue,aPos[0],aPos[1])},"submitdata":function(value,settings){return{"row_id":this.parentNode.getAttribute("id"),"column":oTable.fnGetPosition(this)[2]}},"width":"90%","height":"100%"})});function fnClickAddRow(){$("#editable").dataTable().fnAddData(["Custom row","New row","New row","New row","New row"])};
    </script>
</body>

</html>