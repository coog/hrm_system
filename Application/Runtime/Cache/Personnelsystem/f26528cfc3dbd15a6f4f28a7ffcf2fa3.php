<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>H+ 后台主题UI框架 - jqGird</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico"> 
    <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <!-- jqgrid-->
    <link href="/Public/Theme1/css/plugins/jqgrid/ui.jqgrid.css?0820" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style1.min.css?v=4.1.0" rel="stylesheet">

    <style>
        /* Additional style to fix warning dialog position */

        #alertmod_table_list_2 {
            top: 900px !important;
        }
    </style>

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content  animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>在职员工月度评分系统</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="jqGrid_wrapper">
                            <table id="table_list_2"></table>
                            <div id="pager_list_2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 全局js -->
    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>

    <!-- Peity -->
    <script src="/Public/Theme1/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- jqGrid -->
    <script src="/Public/Theme1/js/plugins/jqgrid/i18n/grid.locale-cn.js?0820"></script>
    <script src="/Public/Theme1/js/plugins/jqgrid/jquery.jqGrid.min.js?0820"></script>

    <!-- 自定义js -->
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {

            $.jgrid.defaults.styleUI = 'Bootstrap';
            // Examle data for jqGrid
            var mydata = [
            <?php if(is_array($rs_staff)): foreach($rs_staff as $key=>$val_staff): ?>{
                    <?php $a=-1; if($nowMonth != $val_staff["stMonthValue"]){ $a=0; }else{ $a=1; } ?>
                    <?php if($a == 1): ?>score: "<?php echo ($val_staff["stMScore"]); ?>",
                    <?php else: ?>
                    score: "",<?php endif; ?>
                    id: "<?php echo ($val_staff["stId"]); ?>",
                    name: "<?php echo ($val_staff["stName"]); ?>",
                    <?php if($val_staff["stSex"] == 1): ?>sex: "男",
                    <?php else: ?>
                    sex: "女",<?php endif; ?>
                    <?php $variables = M("variables"); $rs_xueli = $variables->where("vId=1")->find(); $xueli = explode("|", $rs_xueli["vVariablesVal"]); $xueliId=$val_staff["stDegrees"]; $xueliInfo=$xueli[$xueliId]; $rs_zhicheng = $variables->where("vId=2")->find(); $zhicheng = explode("|", $rs_zhicheng["vVariablesVal"]); $zhichengId=$val_staff["stPositionalTitles"]; $zhichengInfo=$zhicheng["$zhichengId"]; $rs_zhiwu = $variables->where("vId=3")->find(); $zhiwu = explode("|", $rs_zhiwu["vVariablesVal"]); $zhiwuId=$val_staff["stDuties"]; $zhiwuInfo=$zhiwu["$zhiwuId"]; ?>
                    edu: "<?php echo ($xueliInfo); ?>",    

                    <?php $departmentInfo=M("department"); $rs_departmentInfo=$departmentInfo->field("dId,dPid,dPsid,dName")->where("dId={$val_staff['stDid']}")->find(); ?>
                        <?php if($rs_departmentInfo["dPid"] == 0 AND $rs_departmentInfo["dPsid"] == 0): ?>dept: "<?php echo ($rs_departmentInfo['dName']); ?>",
                    <?php elseif($rs_departmentInfo["dPid"] != 0 AND $rs_departmentInfo["dPsid"] == 0): ?>
                         <?php $dPid=$rs_departmentInfo["dPid"]; $department=M("department"); $rsp=$department->where("dId={$dPid}")->find(); ?>
                           dept: "<?php echo ($rsp["dName"]); ?> -> <?php echo ($rs_departmentInfo['dName']); ?>",             
                         <?php elseif($rs_departmentInfo["dPid"] == 0 AND $rs_departmentInfo["dPsid"] != 0): ?>
                            <?php $dPsid=$rs_departmentInfo["dPsid"]; $department=M("department"); $rsps=$department->where("dId={$dPsid}")->find(); $rsPsPid=$rsps["dPid"]; $rsPspe=$department->where("dId={$rsPsPid}")->find(); ?>
                               dept: "<?php echo ($rsPspe["dName"]); ?> -> <?php echo ($rsps["dName"]); ?> -> <?php echo ($rs_departmentInfo['dName']); ?>",<?php endif; ?>
                    
                    zhicheng: "<?php echo ($zhichengInfo); ?>",
                    zhiwu: "<?php echo ($zhiwuInfo); ?>",
                    ruzhi: "<?php echo ($val_staff['stEntryDate']); ?>",
                    ziliao: "<a href='/Staff/show/stId/<?php echo ($val_staff["stId"]); ?>'>详细资料</a>",
                    <?php if($a == 1): ?>caozuo: "<span style='color:#0000ff;'>已评分</span>"
                    <?php else: ?>
                        caozuo: "<a href='/StaffScore/setscore/stgId/<?php echo ($val_staff["stId"]); ?>' style='color:#ff0000'><b>评分</b></a>"<?php endif; ?>
                },<?php endforeach; endif; ?>

            ];
    
            // Configuration for jqGrid Example 2
            $("#table_list_2").jqGrid({

                data: mydata,
                datatype: "local",
                height: 490,
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                editurl: '/StaffScore/add/',
                rowList: [10, 20, 30],
                colNames: ['本月得分','员工ID', '姓名', '性别','学历', '部门', '职称', '职位','入职日期','详细资料','操作'],
                colModel: [
                    {
                        name: 'score',
                        index: 'score',
                        editable: true,
                        width: 60,
                        sorttype: "float",
                        search: true
                    },
                    {
                        name: 'id',
                        index: 'id',
                        editable: false,
                        width: 60,
                        sorttype: "int",
                        search: true
                    },
                    {
                        name: 'name',
                        index: 'name',
                        editable: true,
                        width: 90,
                    },
                    {
                        name: 'sex',
                        index: 'sex',
                        editable: true,
                        width: 100
                    },
                    {
                        name: 'edu',
                        index: 'edu',
                        editable: true,
                        width: 100
                    },
                    {
                        name: 'dept',
                        index: 'dept',
                        editable: true,
                        width: 100,
                        align: "left",
                    },
                    {
                        name: 'zhicheng',
                        index: 'zhicheng',
                        editable: true,
                        width: 80,
                        align: "left",
                        
                    },
                    {
                        name: 'zhiwu',
                        index: 'zhiwu',
                        editable: true,
                        width: 80,
                        align: "left",
                        
                    },
                    {
                        name: 'ruzhi',
                        index: 'ruzhi',
                        editable: false,
                        width: 80,
                        align: "left",
                        
                    },
                    {
                        name: 'ziliao',
                        index: 'ziliao',
                        editable: false,
                        width: 80,
                        align: "left",
                        
                    },
                    {
                        name: 'caozuo',
                        index: 'caozuo',
                        editable: false,
                        width: 80,
                        align: "left",
                        
                    },
                ],
                pager: "#pager_list_2",
                viewrecords: true,
               // caption: "选中下面的员工单条信息并点击编辑工具即可评分",
                add: true,
                edit: true,
                addtext: 'Add',
                edittext: 'Edit',
                hidegrid: false
            });

            // Add selection
            $("#table_list_2").setSelection(1, true);


            // Setup buttons
            $("#table_list_2").jqGrid('navGrid', '#pager_list_2', {
                edit: true,
                add: false,
                del: false,
                search: true
            }, {
                height: 420,
                reloadAfterSubmit: true
            });

            // Add responsive to jqGrid
            $(window).bind('resize', function () {
                var width = $('.jqGrid_wrapper').width();
                $('#table_list_1').setGridWidth(width);
                $('#table_list_2').setGridWidth(width);
            });
        });
    </script>

</body>

</html>