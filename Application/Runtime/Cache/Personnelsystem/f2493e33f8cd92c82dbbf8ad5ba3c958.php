<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<title>综合统计</title>


<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">

<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">


</head>
<script src="/Public/Theme1/js/echarts.js"></script>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">

			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>部门员工数据统计</h5>
						<div class="ibox-tools"></div>
					</div>
					<div class="ibox-content" style="height: 800px;">
						<div class="flot-chart">
							<div id="container" style="min-width: 900px; height: 600px"></div>
						</div>
						<script type="text/javascript"
							src="/Public/Theme1/lib/jquery/1.9.1/jquery.min.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/layer/2.1/layer.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/Highcharts/4.1.7/js/highcharts.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/Highcharts/4.1.7/js/modules/exporting.js"></script>
						<script type="text/javascript"
							src="/Public/Theme1/lib/Highcharts/4.1.7/js/highcharts-3d.js"></script>
						<script type="text/javascript">
﻿﻿$(function () {
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: '各部门员工人数统计'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: '所占比例',
            data: [
            <?php if(is_array($rs_department)): foreach($rs_department as $key=>$val_department): $rs_staff=$staff->where("stDid={$val_department['dId']}")->count(); ?>

                ['<?php echo ($val_department["dName"]); ?> <?php echo ($rs_staff); ?> 人',       <?php echo ($rs_staff); ?>.0],<?php endforeach; endif; ?>
                
            ]
        }]
    });
});
</script>
</body>

</html>