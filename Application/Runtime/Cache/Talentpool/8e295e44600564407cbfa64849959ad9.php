<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<title><?php echo ($rs_index["hrName"]); ?>的简历</title>

<!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->

<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style2.min.css?v=4.1.0"
	rel="stylesheet">
</head>

<body class="fixed-sidebar full-height-layout gray-bg"
	style="overflow: hidden;">
	<div id="wrapper">
		<!--左侧导航开始-->
		<!-- <nav class="navbar-default navbar-static-side" role="navigation"
			style="background-color: #3390cd; border-right: 0px solid #ffffff;">
			<div class="nav-close">
				<i class="fa fa-times-circle"></i>
			</div>
			<div class="sidebar-collapse">
				<ul class="nav" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element" style="margin-top: 17px;">
							<?php if($rs_index["hrPhoto"] == null): if($rs_index["hrSex"] == 1): ?><span><img alt="image" style="border: 10px solid #ffffff" src="/Public/Theme1/img/default_1.png" width="170" height="215" /></span> 
								<?php else: ?> 
									<span><img alt="image" style="border: 10px solid #ffffff" src="/Public/Theme1/img/default_2.jpg" width="170" height="215" /></span><?php endif; ?> 
							<?php else: ?> 
								<span><img alt="image" style="border: 10px solid #ffffff" src="/<?php echo ($rs_index["hrPhoto"]); ?>" width="170" height="215" /></span><?php endif; ?>


							<ul class="dropdown-menu animated fadeInRight m-t-xs"
								style="z-index: 2000">
								<li><a href="/LoginTrue/ExitLogin">安全退出</a></li>
							</ul>
						</div>
						<div class="logo-element"></div>
					</li>
					<li><a href="#"> <i><img
								src="/Public/Theme1/ico/name.png" width="22" height="22"></i>
							<span class="nav-label">姓名：<?php echo ($rs_index["hrName"]); ?></span>
					</a></li>

					<?php if($nowAge != 0): ?><li><a href="#"> <i class="glyphicon"><img
								src="/Public/Theme1/ico/age.png" width="22" height="22"></i>
							<span class="nav-label">年龄：<?php echo ($nowAge); ?> 岁</span>
					</a></li><?php endif; ?>

					<li><a href="#"> <i class="glyphicon"><img
								src="/Public/Theme1/ico/edu.png" width="24" height="24"></i>
							<span class="nav-label">学历：<?php echo ($xueliInfo); ?></span>
					</a></li>

					<li><a href="#"> <i class="glyphicon"><img
								src="/Public/Theme1/ico/height.png" width="22" height="22"></i>
							<span class="nav-label">身高：<?php echo ($rs_index["hrHeight"]); ?> CM</span>
					</a></li>

					<li><a href="#"> <i class="glyphicon"><img
								src="/Public/Theme1/ico/tel.png" width="24" height="24"></i>
							<span class="nav-label">电话：<?php echo ($rs_index["hrTel"]); ?></span>
					</a></li>

					<div class="hr-line-dashed"></div>
					

					<?php if($rs_index["hrState"] == 0): ?><li><a class="J_menuItem" href="/PerfectInfo/updatecv"><i
							class="fa fa-columns"></i> <span class="nav-label">更新我的简历</span></a>
					</li>
					<?php if($fenzhi != 0): ?><li><a class="J_menuItem" href="/Index/main/printId/2016"><i
							class="glyphicon glyphicon-print"></i> <span class="nav-label">打印我的简历</span></a>
						</li><?php endif; ?>

					<li><a href="#"><i class="glyphicon glyphicon-cog"></i> <span
							class="nav-label">管理我的账户</span></a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="/PerfectInfo/userpwd">修改密码</a>
							</li>
						</ul>
					</li>
					<?php else: ?>
						<li><a class="J_menuItem" ><i
							class="fa fa-columns"></i> <span class="nav-label">该简历已审核</span></a>
						</li>
						<?php if($fenzhi != 0): ?><li><a class="J_menuItem" href="/Index/main/printId/2016"><i
							class="glyphicon glyphicon-print"></i> <span class="nav-label">打印我的简历</span></a>
						</li><?php endif; ?>
						<li><a href="#"><i class="glyphicon glyphicon-cog"></i> <span
							class="nav-label">管理我的账户</span></a>
						<ul class="nav nav-second-level">
							<li><a class="J_menuItem" href="/PerfectInfo/userpwd">修改密码</a>
							</li>
						</ul>
					</li><?php endif; ?>

				</ul>
			</div>
		</nav> -->
		<!--左侧导航结束-->
		<!--右侧部分开始-->
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<!-- <nav class="navbar navbar-static-top" role="navigation"
					style="margin-bottom: 0"> -->
					<div class="navbar-header">
						<a class="btn" href="#"
							style="width: 190%; height: 50px; font-size: 18px; line-height: 36px; background: #3390cd; margin-left: 0; color: #ffffff; border-radius: 0;"><!-- <i
							class="fa fa-bars"></i> --><span
							style="margin-left: 20px; margin-right: 100px; color: #ffffff; font-weight: bold;"><!-- <?php echo ($rs_index["hrName"]); ?>的 -->简历</span>
						</a>

					</div>
					
				<!-- </nav> -->
			</div>
			<div class="row content-tabs">
				<!-- <button class="roll-nav roll-left J_tabLeft"></button> -->
				<nav class="page-tabs J_menuTabs col-xs-12">
					<div >
						<a style="color: #4c8ec0; font-weight: bold" href="/Index/">刷新简历</a>
					</div >
					<?php if($rs_index["hrState"] == 0): ?><div>
						<a style="color: #4c8ec0; font-weight: bold" href="/PerfectInfo/updatecv" target="iframe0">填写/修改简历</a>
					</div><?php endif; ?>
					<?php if($fenzhi != 0): ?><div>
						<a style="color: #4c8ec0; font-weight: bold" href="/Index/main/printId/2016" target="iframe0">打印简历</a>
					</div><?php endif; ?>
<!-- 	
					<div>
                        <a href="http://hrv4.nlipin.com" target="_bank" style="color:#ff0000; font-weight: bold">人事系统V4.2.3</a>
                    </div> -->
				</nav>

				<!-- <a href="/LoginTrue/ExitLogin"
					class="roll-nav roll-right J_tabExit col-xs-3"><i
					class="fa fa fa-sign-out"></i>退 出</a> -->
			</div>
			<div class="row J_mainContent" id="content-main" style="-webkit-overflow-scrolling:touch;overflow:auto">
			
				<iframe class="J_iframe" name="iframe0" width="100%" height="100%"
					src="/Index/main" frameborder="0" data-id="index_v1.html"  scrolling="yes"
					seamless></iframe>

			</div>
			<!-- <div class="footer">
				<div class="pull-right J_iframe dropdown hidden-xs" name="iframe0"
					frameborder="0" data-id="index.html" seamless>
					<span style="color: #1690d8"> <?php echo ($ResultNowDay); ?> 
				</div>
			</div> -->
		</div>
		<!--右侧部分结束-->
		<!--右侧边栏开始-->
		<div id="right-sidebar">
			<div class="sidebar-container">

				<ul class="nav nav-tabs navs-3">

					<li class="active"><a data-toggle="tab" href="#tab-1"> <i
							class="fa fa-gear"></i> 主题
					</a></li>
				</ul>

				<div class="tab-content">
					<div id="tab-1" class="tab-pane active">
						<div class="sidebar-title">
							<h3>
								<i class="fa fa-comments-o"></i> 主题设置
							</h3>
							<small><i class="fa fa-tim"></i>
								你可以从这里选择和预览主题的布局和样式，这些设置会被保存在本地，下次打开的时候会直接应用这些设置。</small>
						</div>
						<div class="skin-setttings">
							<div class="title">主题设置</div>
							<div class="setings-item">
								<span>收起左侧菜单</span>
								<div class="switch">
									<div class="onoffswitch">
										<input type="checkbox" name="collapsemenu"
											class="onoffswitch-checkbox" id="collapsemenu"> <label
											class="onoffswitch-label" for="collapsemenu"> <span
											class="onoffswitch-inner"></span> <span
											class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
							<div class="setings-item">
								<span>固定顶部</span>

								<div class="switch">
									<div class="onoffswitch">
										<input type="checkbox" name="fixednavbar"
											class="onoffswitch-checkbox" id="fixednavbar"> <label
											class="onoffswitch-label" for="fixednavbar"> <span
											class="onoffswitch-inner"></span> <span
											class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
							<div class="setings-item">
								<span> 固定宽度 </span>

								<div class="switch">
									<div class="onoffswitch">
										<input type="checkbox" name="boxedlayout"
											class="onoffswitch-checkbox" id="boxedlayout"> <label
											class="onoffswitch-label" for="boxedlayout"> <span
											class="onoffswitch-inner"></span> <span
											class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
							<div class="title">皮肤选择</div>
							<div class="setings-item default-skin nb">
								<span class="skin-name "> <a href="#" class="s-skin-0">
										默认皮肤 </a>
								</span>
							</div>
							<div class="setings-item blue-skin nb">
								<span class="skin-name "> <a href="#" class="s-skin-1">
										蓝色主题 </a>
								</span>
							</div>
							<div class="setings-item yellow-skin nb">
								<span class="skin-name "> <a href="#" class="s-skin-3">
										黄色/紫色主题 </a>
								</span>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
		<!--右侧边栏结束-->
	</div>
	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script
		src="/Public/Theme1/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script
		src="/Public/Theme1/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/Public/Theme1/js/plugins/layer/layer.min.js"></script>
	<script src="/Public/Theme1/js/hplus.min.js?v=4.1.0"></script>
	<script type="text/javascript"
		src="/Public/Theme1/js/contabs.min.js"></script>
	<script src="/Public/Theme1/js/plugins/pace/pace.min.js"></script>
</body>

</html>