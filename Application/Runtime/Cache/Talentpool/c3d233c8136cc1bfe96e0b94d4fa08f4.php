<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html  style="overflow: hidden;">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>详细资料</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">
<style>
	.btj,.btj1,.btj2,.btj3{
	    font-size: 14px;
	    color: inherit;
	    display: block;
	    /*padding: 6px 12px;*/
	    padding: 0;
	    width: 32px;
	    height: 32px;
	    text-align: center;
	    border: 0;
	    position: absolute;
	    right: 15px;
	    top: 0
	}
	@media screen and (max-width:640px){
		.btj,.btj1,.btj2{
	    font-size: 14px;
	    color: inherit;
	    display: block;
	    text-align: center;
	    /*padding: 6px 12px;*/
	    padding: 0;
	    width: 32px;
	    height: 32px;
	    border: 0;
	    position: absolute;
	    right: 30px;
	    top: -10px
	}
	}
	.p1{
		font-size: 14px;
	    color: inherit;
	    display: inline-block;
	    padding: 6px 12px;

	}
	.p0{
		padding: 0
	}
	.fam,.edus,.worker{
		position:relative;
	}
	.mb3{
		margin-bottom: 3px
	}
	.pl0{
		padding-left: 0;
	}
</style>
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							<a href="/Index/addatecv" style="color:#c8c9c9">基本资料 </a>
							<a href="/Index/addatecvtwo" style="color:#3390cd; margin-left:25px;">详细资料</a>
							<a href="/Index/addatecvthree" style="color:#c8c9c9; margin-left:25px;">相关附件</a>
						</h5>

					</div>

					<div class="ibox-content"">
						<form method="post" action="/Index/addAtecvthree"
							class="form-horizontal" enctype="multipart/form-data">

							<div class="form-group">
								<label class="col-sm-1 control-label">体重</label>
								<div class="col-sm-6">
									<input type="text" name="hrWeight" id="hrWeight" placeholder="请输入体重,单位KG" class="form-control" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">QQ号码</label>
								<div class="col-sm-6">
									<input type="tel" name="hrQQ" id="hrQQ"
										placeholder="请输入QQ号码" class="form-control" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">身份证号码</label>
								<div class="col-sm-6">
									<input type="text" name="hrIDCard" id="hrIDCard" placeholder="请输入身份证号码" class="form-control" >
								</div>
							</div>
			
							<div class="form-group">
								<label class="col-sm-1 control-label">政治面貌</label>
								<div class="col-sm-6">
									<input type="text" name="hrPoliticalIandscape" id="hrPoliticalIandscape"
										placeholder="请输入政治面貌，比如团员" 
										class="form-control" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">工作地区</label>
								<div class="col-sm-6">
								<select name="hrWorkArea" id="hrWorkArea" class="form-control">
									<option>请选择</option>
									<option value="广州">广州</option>
									<option value="中山">中山</option>
									<option value="武汉">武汉</option>
									<option value="成都">成都</option>
									<option value="郑州">郑州</option>
									<option value="唐山">唐山</option>
								</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">应聘渠道</label>
								<div class="col-sm-3">
								<select name="hrRecruitment" id="sel_channel" class="form-control"  onchange="showText('sel_channel','ss_text','sale_other')">
									<option>请选择</option>
									<option value="前程无忧">前程无忧</option>
									<option value="智联招聘">智联招聘</option>
									<option value="58同城">58同城</option>
									<option value="boss直聘">boss直聘</option>
									<option value="赶集网">赶集网</option>
									<option value="推荐人">内部推荐</option>
									<option value="其他"  class="newEnt">其他</option>
								</select>
								</div>
								<div class="col-sm-3 ss_text" >
									<span class="sale_other"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">专业技能</label>
								<div class="col-sm-6">
									<div class="col-sm-12 mb3 p0">
										<div class="col-sm-2 pl0"><span class="form-control p0" style="border: 0">语言技能</span></div>
										<div class="col-sm-6 p0">
										<select name="hrLanguage1" id="hrLanguage1" class="form-control" onchange="showText('hrLanguage1','language_other','language_text')">
											<option>请选择</option>
											<option value="英语">英语</option>
											<option value="日语">日语</option>
											<option value="其他" class="newEnt">其他</option>
										</select>
										</div>
										<div class="col-sm-4 language_other" >
											<span class="language_text"></span>
										</div>
									</div>
									<div class="col-sm-12 mb3 p0">
										<div class="col-sm-2 p0"><span class="form-control p0" style="border: 0">是否有驾照</span></div>
										<div class="col-sm-6 p0">
										<select name="hrLicense" id="sel_driver" class="form-control" onchange="showText('sel_driver','driver_other','driver_type')">
											<option value="">请选择</option>
											<option value="1" class="newEnt">是</option>
											<option value="2">否</option>
										</select>
										</div>
										<div class="col-sm-4 driver_other" >
											<span class="driver_type"></span>
										</div>
									</div>
									<div class="col-sm-12 mb3 p0">
										<div class="col-sm-2 p0"><span class="form-control p0" style="border: 0">技能证书</span></div>
										<div class="col-sm-6 p0">
										<select name="hrTrainingCertificate1" id="hrTrainingCertificate1" class="form-control" onchange="showText('hrTrainingCertificate1','skill_type','skill_val')">
											<option>请选择</option>
											<option value="计算机" class="newEnt">计算机类</option>
											<option value="财会类" class="newEnt">财会类</option>
											<option value="其他" class="newEnt">其他</option>
										</select>
										</div>
										<div class="col-sm-4 skill_type" >
											<span class="skill_val"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">掌握技能</label>
								<div class="col-sm-6">
									<input type="text"  class="input-text form-control" id="hrJineng"
								name="hrJineng" placeholder="列举你擅长的技能，比如会开车，写程序" rows="1" value="<?php echo ($rs_hrreserve["hrJineng"]); ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">紧急联系人</label>
								<div class="col-sm-6">
										<div class="col-sm-12 p0">
											<input type="text" name="hrContactName" placeholder="姓名"   class="form-control mb3"/>
											<input type="text" name="hrContactRelationship" placeholder="关系"   class="form-control mb3"/>
											<input type="text" name="hrContactTel" placeholder="联系方式"   class="form-control mb3"/>
										</div>									
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">家庭情况</label>
								<div class="col-sm-6 fam">
									<div class="section col-md-11 p0">
									<div class="col-sm-1 p1 pl0">Part1</div>
										<div id="input_box" class="col-sm-11 p0">
											<input type="text" name="hrFamilyMember" placeholder="姓名"   class="form-control mb3"/>
											<input type="text" name="hrFamilyRelationship" placeholder="关系"   class="form-control mb3"/>
											<input type="text" name="hrFamilyUnit" placeholder="工作单位"   class="form-control mb3"/>
											<input type="text" name="hrFamilyCareer" placeholder="职业"   class="form-control mb3"/>
											<input type="text" name="hrFamilyTel" placeholder="联系方式"   class="form-control mb3"/>
										</div>
									</div>
										<input type="button" value="+" onclick="add_input('input_box','btj1',5,'section','fam')" class="btj1 col-sm-1"/>
									
							</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">教育背景</label>
								<div class="col-sm-6 edus">
										<div class="sedus col-md-11 p0">
									<div class="col-sm-1 p1 pl0">Part1</div>
										<div id="input_tbox" class="col-sm-11 p0">
											<input id='jysj' type="text" name="hrEduDate" placeholder="起止年月"   class="form-control col-xs-12 mb3" onblur="zzpd(this.id)"/>
											<input type="text" name="hrEduSchool" placeholder="学校"   class="form-control col-xs-12 mb3"/>
											<input type="text" name="hrEduProfession" placeholder="专业"   class="form-control col-xs-12 mb3"/>
											<input type="text" name="hrEduEducation" placeholder="学历"   class="form-control col-xs-12 mb3"/>
											<input type="text" name="hrEduCertificate" placeholder="所获证书" " class="form-control col-xs-12 mb3"/>
										</div>
									</div>
										<input type="button" value="+" onclick="add_input('input_tbox','btj',5,'sedus','edus')" class="btj col-sm-1"/>
									
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">培训经历</label>
								<div class="col-sm-6 pxjl">
										<div class="pxedus col-md-11 p0">
									<div class="col-sm-1 p1 pl0">Part1</div>
										<div id="input_fbox" class="col-sm-11 p0">
											<input type="text" name="hrTrainingDate" placeholder="起止年月"   class="form-control col-xs-12 mb3"/>
											<input type="text" name="hrTrainingMechanism" placeholder="培训机构/老师"   class="form-control col-xs-12 mb3"/>
											<input type="text" name="hrTrainingCourse" placeholder="课程名称"   class="form-control col-xs-12 mb3"/>
											<input type="text" name="hrTrainingCertificate" placeholder="所获证书" " class="form-control col-xs-12 mb3"/>
										</div>
									</div>
										<input type="button" value="+" onclick="add_input('input_fbox','btj3',4,'pxedus','pxjl')" class="btj3 col-sm-1"/>
									
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-1 control-label">工作经历</label>
								<div class="col-sm-6 worker">
										<div class="swork col-md-11 p0">
											<div class="col-sm-1 p1 pl0">Part1</div>
											<div id="input_sbox" class="col-sm-11 p0">
												<input type="text" name="hrWorksDate" placeholder="起止年月"   class="form-control mb3"/>
												<input type="text" name="hrWorksUnit" placeholder="工作单位"   class="form-control mb3"/>
												<input type="text" name="hrWorksPosition" placeholder="职位"   class="form-control mb3"/>
												<input type="text" name="hrWorksSalary" placeholder="薪资"   class="form-control mb3"/>
												<input type="text" name="hrWorksReason" placeholder="离职原因"   class="form-control mb3"/>
												<input type="text" name="hrWorksWitness" placeholder="证明人/联系方式"   class="form-control mb3"/>
											
											</div>
										</div>
										<input type="button" value="+" onclick="add_input('input_sbox','btj2',6,'swork','worker')" class="btj2 col-sm-1"/>
								</div>
							</div>
							
							

					<!-- 		<div class="form-group">
								<label class="col-sm-1 control-label">项目经历</label>
								<div class="col-sm-6">
									<textarea class="input-text form-control" id="hrWorks"
								name="hrWorks" placeholder="列举你的项目经历" rows="3"><?php echo ($rs_hrreserve["hrWorks"]); ?></textarea>
								</div>
							</div>

							

							<div class="form-group">
								<label class="col-sm-1 control-label">自我评价</label>
								<div class="col-sm-6">
									<textarea class="input-text form-control" id="hrPingjia"
								name="hrPingjia" placeholder="对自己的综合评价" rows="3"><?php echo ($rs_hrreserve["hrPingjia"]); ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">相关备注</label>
								<div class="col-sm-6">
									<textarea class="input-text form-control" id="hrRemarks"
								name="hrRemarks" placeholder="比如何时能到岗" rows="3"><?php echo ($rs_hrreserve["hrRemarks"]); ?></textarea>
								</div>
							</div> -->
						
							
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									
									<button class="btn btn-primary" type="submit" style="margin-left:15px;">下一步</button>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>
	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
    <script type="text/javascript">
						    /**
						 *
						 方法说明
						 *
						 @method add_input() 
						 *
						 @param {string} name1  添加的input的name属性
						 @param {string} idname  添加的input的父级ID
						 @param {string} cname  添加input按钮的class名
						 @param {number} dil  添加的input的个数
						 @param {string} secname  包着input父级盒子的盒子class名（input-->父级ID-->父级class）
						 @param {string} pjn  secname的父盒子class名
						 *
						 @return {Object}  返回一个对象，该对象包含所添加的input个数，input的外层盒子，与外层盒子平级的div，div的文本为part+（N+1） 							  N为添加input组的次数
						 */
										function add_input(idname,cname,dil,secname,pjn) 
										{
										var seclen=document.getElementsByClassName(secname).length;
										var pj=document.getElementsByClassName(pjn)[0];
										var ib = document.getElementById(idname); 
										var pn = document.getElementsByClassName("p1")[0]; 
										var btj =document.getElementsByClassName(cname)[0];
										var ibl = document.getElementById(idname).getElementsByTagName("input");
										var il = ib.childNodes.length;
										var arr1 =[];
										var p_arr=[];
										var n_arr=[];
										for(var i=0;i<dil;i++){
										var input_file = document.createElement("input");			
										input_file.setAttribute("type","text");
										input_file.setAttribute("class",'form-control mb3');
										var name1=ibl[i].getAttribute("name"+(idname+seclen));
										n_arr.push(ibl[i].getAttribute("name"));
										p_arr.push(ibl[i].getAttribute("placeholder"));
										arr1.push(input_file);
										}
										
										var ibc=document.createElement("div");
										var sec=document.createElement("div");
										var pnc=document.createElement("div");
										pnc.innerText = 'Part'+(seclen+1);
										pnc.setAttribute("class","col-sm-1 p1 pl0");
										ibc.setAttribute("id",(idname+seclen));
										ibc.setAttribute("class","col-sm-11 p0");
										sec.setAttribute("class",(secname)+" "+"col-sm-11 p0");
										sec.append(pnc);
										for(var j=0;j<arr1.length;j++){
											ibc.append(arr1[j])
											arr1[j].setAttribute("placeholder",p_arr[j]);
											arr1[j].setAttribute("name",(n_arr[j]+seclen));
										}
										sec.append(ibc);
										pj.append(sec);
										if(seclen==2){
											// btj.setAttribute("disabled","disabled")
											btj.style.display="none";
										}
										}	

						/**
						 *
						 方法说明
						 *
						 @method showText()
						 *
						 @param {string} idname  触发该点击事件的ID名
						 @param {string} changebox  需要显示input所在位置的盒子class名
						 @param {string} changeval  显示内容的元素class名

						 *
						 @return {string}   需要显示的内容
						 */	
								function showText(idname,changebox,changeval){
									var onch=document.getElementById(idname);
									var chbox=document.getElementsByClassName(changebox)[0];
									var chval=document.getElementsByClassName(changeval)[0];
								if(onch.options[onch.selectedIndex].className=="newEnt"){
									chbox.style.display="inline-block";
								chval.innerHTML="<input name='"+(onch.options[onch.selectedIndex].getAttribute("name")+1)+"'  placeholder='"+onch.options[onch.selectedIndex].value+"' type='text' class='form-control'/>";
									onch.value=onch.options[onch.selectedIndex].value;
								}else{
									chbox.style.display="none";
									
								}
								}

								
 								function zzpd(idname){
	 								var reg = /^[1-2]\d{3}\.(0[1-9]|1[0-2])\.(0[1-9]|[1-2][0-9]|3[0-1])-[1-2]\d{3}\.(0[1-9]|1[0-2])\.(0[1-9]|[1-2][0-9]|3[0-1])$/; 
	 								var regExp = new RegExp(reg);
	 								var ev=document.getElementById(idname).value;
									// console.log(regExp.test(val));
									 if(!regExp.test()){
										// alert("日期格式不正确，正确格式为：2015.09.01-2018.07.01");     
									 }else{
									    // alert(111);
									}
								}
								

	</script>
</body>

</html>