<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>基本资料</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">
</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							<a href="/Index/addAtecv" style="color:#3390cd">基本资料 </a>
							<a href="/Index/addAtecvtwo" style="color:#c8c9c9; margin-left:25px;">详细资料</a>
							<a href="/Index/addAtecvthree" style="color:#c8c9c9; margin-left:25px;">相关附件</a>
						</h5>

					</div>

					<div class="ibox-content"">
						<form method="post" action="/Index/addAtecvtwo"
							class="form-horizontal" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-sm-1 control-label">姓名</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="hrName" id="hrName"
										placeholder="" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">性别</label>

								<div class="col-sm-6">
									<label
										class="checkbox-inline"> <input type="radio" value="1"
										name="hrSex" checked> 男
									</label> 
									<label class="checkbox-inline"> <input type="radio"
										value="2" name="hrSex"> 女
									</label> 
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">学历</label>
								<div class="col-sm-6">
									<select class="chosen-select form-control" size="1"
										name="hrDegrees" id="hrDegrees" required>
										<?php if(is_array($xueli)): foreach($xueli as $xuelik=>$valxueli): ?><option value="<?php echo ($valxueli); ?>"><?php echo ($valxueli); ?></option><?php endforeach; endif; ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">出生日期</label>
								<div class="col-sm-2">
									<span data-placeholder="员工出生日期"
										class="chosen-select form-control">
										公历
									</span>
								</div>
								<div class="col-sm-4">
									<input type="date" name="hrBirthdate" id="hrBirthdate" class="form-control" required>
								</div>

							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">联系电话</label>
								<div class="col-sm-6">
									<input type="tel" name="hrTel" id="hrTel"
										placeholder="请输入联系电话"
										class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">身高</label>
								<div class="col-sm-6">
									<input type="text" name="hrHeight" id="hrHeight"
										placeholder="请输入身高，单位CM"
										class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">民族</label>
								<div class="col-sm-6">
									<input type="text" name="hrMultiracial" id="hrMultiracial"
										placeholder="请输入民族"
										class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">籍贯</label>
								<div class="col-sm-6">
									<input type="text" name="hrNativePlace" id="hrNativePlace"
										placeholder="请输入籍贯"
										class="form-control" required>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-1 control-label">应聘岗位</label>
								<div class="col-sm-6">
								<input type="text" name="hrPosition" id="hrPosition"
										placeholder="请输入应聘岗位" value=""
										class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-1 control-label">婚否</label>
								<div class="col-sm-6">
								<select class="chosen-select form-control" size="1"
								name="hrIsMarriage" id="" required>
								<option value="1" selected>已婚</option>
								<option value="0" selected>未婚</option>
								
								</select>
								</div>
							</div>
							

							<div class="form-group">
								<label class="col-sm-1 control-label">期望薪资</label>
								<div class="col-sm-6">
									<input type="number" maxlength="5" name="hrSalary" id="hrSalary"
										placeholder="选填内容，不填或者0代表不限"
										class="form-control" required />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-1 control-label">现住址</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" name="hrCity" id="hrCity" placeholder="请输入省/市/县" required />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-1 control-label">邮箱</label>
								<div class="col-sm-6">
									<input type="email" name="hrEmail" id="hrEmail"
										placeholder="以@为分隔，如：309091579@qq.com"
										class="form-control" required />
								</div>
							</div>
						
							
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									
									<button class="btn btn-primary" type="submit" style="margin-left:15px;">下一步</button>
									<!-- <a href="/Index/addAtecvtwo" style="margin-left:15px; color:#1270ae; font-weight:bold">下一步</a> -->
									
								</div>
							</div>
						</form>
					</div>
				</div>	
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>
	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
</body>

</html>