<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo ($rs_hrreserve["hrName"]); ?> 简历</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
        rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
        rel="stylesheet">

    <!-- Morris -->
    <link href="/Public/Theme1/css/plugins/morris/morris-0.4.3.min.css"
        rel="stylesheet">

    <!-- Gritter -->
    <link href="/Public/Theme1/js/plugins/gritter/jquery.gritter.css"
        rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0"
        rel="stylesheet">

</head>

<body style="background: #ffffff;overflow: auto;" >

  <div class="row animated fadeInRight col-xs-12">
    <!--<div class="row">
        <div class="col-sm-12">
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="text-center float-e-margins p-md">
                             <?php if($fenzhi != 0): ?><span>预览样式：</span>
                                <a href="#" class="btn btn-xs btn-primary" id="lightVersion">浅色</a>
                                <a href="#" class="btn btn-xs btn-primary" id="darkVersion">深色</a>
                                <a href="#" class="btn btn-xs btn-primary" id="leftVersion">布局切换</a>

                               
                            </div>
                            <div class="text-center float-e-margins">
                                 <a href="#" class="btn btn-sm btn-danger">简历完善度  <?php echo ($fenzhi); ?>%</a>
                                 <a href="#" class="btn btn-sm btn-danger" style="background:#fa3ca3; margin-left:15px;">简历综合得分  <?php echo ($hotscore); ?> 分</a>

                                 
                            </div>

                            <div class="" id="ibox-content">

                                <div id="vertical-timeline" class="vertical-container light-timeline">

                                <?php if($rs_hrreserve["hrState"] == 1 AND $rs_hrreserve["hrStateSuccess"] == 0): ?><div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon yellow-bg" style="background:#2e65d5">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </div>
                                       

                                        <div class="vertical-timeline-content">
                                            <h2>面试状态</h2>
                                            <p>是否通过：<span style="color:#0000ff">未通过</span></p>
                                            <if condition="$rs_hrreserve.hrStateInfo eq null">
                                            <p>具体原因：<?php echo ($rs_hrreserve["hrStateInfo"]); ?></p>
                                            <span class="vertical-date">
                                        
                                        <small>审核时间：<?php echo ($rs_hrreserve["hrStateDate"]); ?></small>
                                    </span>
                                            
                                        </div>
                                    </div>
                                <?php elseif($rs_hrreserve["hrState"] == 1 AND $rs_hrreserve["hrStateSuccess"] == 1): ?>
                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon yellow-bg" style="background:#26a36c">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </div>
                                       

                                        <div class="vertical-timeline-content">
                                            <h2>面试状态</h2>
                                            <p>是否通过：<span style="color:#ff0000">已通过</span></p>
                                            <p>相关提示：<?php echo ($rs_hrreserve["hrStateInfo"]); ?></p>
                                            <span class="vertical-date">
                                        
                                        <small>审核时间：<?php echo ($rs_hrreserve["hrStateDate"]); ?></small>
                                    </span>
                                        </div>
                                    </div>

                                <?php elseif($rs_hrreserve["hrState"] == 0): ?>
                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon yellow-bg" style="background:#0ac4cb">
                                            <i class="glyphicon glyphicon-eye-open"></i>
                                        </div>
                                       

                                        <div class="vertical-timeline-content">
                                            <h2>查看状态</h2>
                                            <p><span style="color:#3939f8">还未查看</span></p>
                                             <a href="#" class="btn btn-sm btn-primary"> 更新时间：<?php echo ($rs_hrreserve["hrGengxingDate"]); ?></a>
                                            <span class="vertical-date">
                                            <small style="font-weight:bold; font-size:14px;">创建时间：<?php echo ($rs_hrreserve["hrInputDate"]); ?></small>
                                            </span>
                                   
                                        </div>
                                    </div><?php endif; ?>

                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon lazur-bg" style="background:#f37852">
                                            <i class="fa fa-user-md"></i>
                                        </div>

                                        <div class="vertical-timeline-content">
                                            <h2>基本概要</h2>
                                            <p><span style="font-size:14px; font-weight:bold; margin-right:10px;"><?php echo ($rs_hrreserve["hrName"]); ?></span>  <?php echo ($sex); ?>  　<?php echo ($rs_hrreserve["hrMultiracial"]); ?>  　<?php echo ($xueliInfo); ?>学历  　身高 <?php echo ($rs_hrreserve["hrHeight"]); ?> CM  　
                                            <?php if($rs_hrreserve["hrWeight"] != null): ?>体重 <?php echo ($rs_hrreserve["hrWeight"]); ?> Kg<?php endif; ?>
                                            <p> 
                                           <?php if($rs_hrreserve["hrBirthdateType"] == 1): ?>公历 <?php else: ?>农历<?php endif; echo ($rs_hrreserve["hrBirthdate"]); ?> 出生于 <?php echo ($rs_hrreserve["hrNativePlace"]); ?> 　现年<?php echo ($nowAge); ?>岁 </p>
                                           
                                           应聘公司：<span style="color:#ff0000"><?php echo ($zhiwuInfo); ?></span> 岗位 　期望月薪：<span style="color:#ff0000"><?php echo ($rs_hrreserve["hrSalary"]); ?></span>元

                                          
                                            </p> 
                                           
                                        </div>
                                    </div>

                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon yellow-bg" style="background:#fb4eb0">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                       

                                        <div class="vertical-timeline-content">
                                            <h2>联系方式</h2>
                                            <p>电话：<?php echo ($rs_hrreserve["hrTel"]); ?> 
                                            <?php if($rs_hrreserve["hrQQ"] != null): ?>　
                                            <p>QQ：<?php echo ($rs_hrreserve["hrQQ"]); endif; ?>
                                            <?php if($rs_hrreserve["hrEmail"] != null): ?>　 　
                                            <p>邮箱：<?php echo ($rs_hrreserve["hrEmail"]); endif; ?>　
                                            
                                            
                                        </div>
                                    </div>

                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon blue-bg" style="background:#af7ef0">
                                            <i class="glyphicon glyphicon-education"></i>
                                        </div>

                                        <div class="vertical-timeline-content">
                                            <h2>教育背景</h2>
                                            <p>
                                            <?php if($rs_hrreserve["hrEdu"] != null ): ?>　<?php echo ($rs_hrreserve["hrEdu"]); ?>
                                            <?php else: ?>
                                            <a href="/PerfectInfo/updatecvtwo">请点击这里完善</a>
                                            <span class="vertical-date">
                                            <small style="font-weight:bold; font-size:14px;">完善此项：+15分</small>
                                            </span><?php endif; ?>
                                            </p>
                                            
                                           
                                        </div>
                                    </div>

                                    <div class="vertical-timeline-block">
                                        
                                    <div class="vertical-timeline-icon red-bg">
                                            <i class="glyphicon glyphicon-export"></i>
                                        </div>

                                        <div class="vertical-timeline-content">
                                            <h2>掌握技能</h2>
                                            <p>
                                            <?php if($rs_hrreserve["hrJineng"] != null ): ?>　<?php echo ($rs_hrreserve["hrJineng"]); ?>
                                            <?php else: ?>
                                            <a href="/PerfectInfo/updatecvtwo">请点击这里完善</a>
                                            <span class="vertical-date">
                                            <small style="font-weight:bold; font-size:14px;">完善此项：+10分</small>
                                            </span><?php endif; ?>
                                            </p>
                                            
                                        </div>
                                    </div>

                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon yellow-bg" style="background:#948823" >
                                            <i class="fa fa-file-text"></i>
                                        </div>

                                        <div class="vertical-timeline-content">
                                            <h2>工作经历</h2>
                                            <p>
                                            <?php if($rs_hrreserve["hrWorks"] != null ): ?>　<?php echo ($rs_hrreserve["hrWorks"]); ?>
                                            <?php else: ?>
                                            <a href="/PerfectInfo/updatecvtwo">请点击这里完善</a>
                                            <span class="vertical-date">
                                            <small style="font-weight:bold; font-size:14px;">完善此项：+15分</small>
                                            </span><?php endif; ?>
                                            </p>
                                           
                                        </div>
                                    </div>

                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon yellow-bg" style="background:#ef5b9c" >
                                            <i class="glyphicon glyphicon-hand-up"></i>
                                        </div>

                                        <div class="vertical-timeline-content">
                                            <h2>自我评价</h2>
                                            <p>
                                            <?php if($rs_hrreserve["hrPingjia"] != null ): ?>　<?php echo ($rs_hrreserve["hrPingjia"]); ?>
                                            <?php else: ?>
                                            <a href="/PerfectInfo/updatecvtwo">请点击这里完善</a>
                                            <span class="vertical-date">
                                            <small style="font-weight:bold; font-size:14px;">完善此项：+10分</small>
                                            </span><?php endif; ?>
                                            </p>
                                           
                                        </div>
                                    </div>
                                    
                                    <?php if($rs_hrreserve["hrRemarks"] != null ): ?><div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon blue-bg">
                                            <i class="glyphicon glyphicon-info-sign"></i>
                                        </div>

                                        <div class="vertical-timeline-content">
                                            <h2>相关备注</h2>
                                            <p>
                                            　<?php echo ($rs_hrreserve["hrRemarks"]); ?>
                                            </p>
                                           
                                        </div>
                                    </div><?php endif; ?>


                                </div>
                            
                            </div>


                            <?php else: ?>
                                <span>该简历还未完善</span>
                                <a href="/PerfectInfo/updatecv" class="btn btn-xs btn-primary" >点击此处去完善咯！</a><?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>-->
            
            <style type="text/css">
            body{
                font-family: "Microsoft YaHei";color: #000000;
            }
            label{
                font-weight: 400;
                

            }
                table{width:800px;text-align: center;margin: 0 auto;border-collapse:collapse;border: 2px solid #000000;color: #000000}
                td{
                    
                    width: 100px;
                    height: 30px;
                }
                .kong{
                     border-left:0 ;
                }
                #project{
                    width: 800px;
                    margin: 0 auto;
                }
                #project p{
                    margin-top: 15px;
                    margin-bottom:0;
                }
                .pj_experience,.pj_experience1{
                    width: 800px;
                    height:200px;
                    border:1px solid #000000;
                    font-size: 14px;
                    margin: 10px auto;
                    display: block;
                    overflow: hidden;
                }
                #table_head{
                    width: 800px;
                    height: 50px;
                    margin: 10px auto;
                    text-align: center;
                    position: relative;
                }
                #table_head .qclogo{
                    text-align: left;
                    position: absolute;
                    top: 0px;

                }
                #table_head .tb_title{
                    font-size: 22px;text-align: right;position: absolute;right: calc(100%/3);top: 50%;font-weight: 1000
                }
                #table_head .tb_cpname{
                    text-align: right;position: absolute; top:0px;right:0 ;
                }
                .bbb{
                    border-bottom: 1px solid #000000;
                }
                
                .pad_l1{
                    padding-left: 1em;
                    text-align: left;
                }
                .ta_l{
                    text-align: left;
                    padding: 0;
                }
                .tc{
                    text-align: center;height:20px;width: 800px;margin: 30px auto;
                }
                .fw600{
                    font-weight: 600;
                    color: #000000;
                }
                .input_channel{
                    padding:6px 0;margin-left: 15px;margin-bottom: 0;width: 150px;
                }
                .input_label{
                    padding:6px 0;margin-right: 10px;margin-bottom: 0;
                }
                .input_icon_pos{
                        vertical-align:text-bottom;margin-bottom:1px;
                }
            </style>
            <div id="table_head">
                <div class="col-md-2 qclogo"><!-- <img src="/Public/Theme1/img/QCLOGO.png" width="50px" height="50px"/> --></div>
                <div class="col-md-6 tb_title" >应聘申请表<span style="font-size: 14px;font-weight: 400">（编号：HR20180903）</span></div>
                <div class="col-md-4 tb_cpname" >广东齐创科技投资集团有限公司</div>
            </div>
            
            <table border="2" >
                <tr>
                    <td class="fw600">姓名</td>
                    <td ></td>
                    <td class="fw600">应聘岗位</td>
                    <td colspan="1"></td>
                    <td class="fw600">出生年月</td>
                    <td colspan="2"></td>
                    
                    <td rowspan="5" style="width: 120px;">
                    <!-- <input type="file"> -->
                    <img  alt="" width="100%" height="150px" src="/Public/Theme1/img/QCLOGO.png">
                    </td>
                </tr>
                <tr>
                    <td class="fw600">性别</td>
                    <td></td>
                    <td class="fw600">学历</td>
                    <td colspan="1"></td>
                    <td class="fw600">身份证号码</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td class="fw600">年龄</td>
                    <td></td>
                    
                    <td class="fw600">身高</td>
                    <td></td>
                    <td class="fw600">体重</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td class="fw600">政治面貌</td>
                    <td></td>
                    <td class="fw600">民族</td>
                    <td colspan="1"></td>
                    <td class="fw600">籍贯</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td class="fw600">工作地区</td>
                    <td colspan="2"></td>
                    <td class="fw600">婚否</td>
                    <td colspan="3">
                    <form action="">
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m1" value="未婚" class="input_label"><span>未婚</span>
                        </label>
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m2" value="已婚" class="input_icon_pos"><span>已婚</span>
                        </label>    
                       <!--  <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="离异" style="vertical-align: text-bottom; margin-bottom:1px;"><span>离异</span>
                        </label> -->
                        </form>
                    </td>
                </tr>
                
                <tr>
                    
                    <td class="fw600">移动电话</td>
                    <td colspan="2"></td>
                    <td rowspan="2" class="fw600">紧急联系人</td>
                    <td class="fw600">姓名</td>     
                    <td></td>     
                    <td class="fw600">关系</td>     
                    <td></td>      
                </tr>
                <tr>
                    <td class="fw600">电子邮箱</td>
                    <td colspan="2"></td>
                    <td class="fw600">联系方式</td>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td class="fw600">现住址</td>
                    <td colspan="7"></td>
                </tr>
                <tr style="border-bottom: 2px solid #000000;">
                    <td class="fw600">应聘渠道</td>
                    <td colspan="7" style="text-align: left;">
                         <form action="">
                        <label for="" class="input_channel">
                            <input type="checkbox" disabled="true" name="marriage" id="m1" value="" class="input_icon_pos"><span>前程无忧</span>
                        </label>
                        <label for="" class="input_channel">
                            <input type="checkbox" disabled="true" name="marriage" id="m2" value="" class="input_icon_pos"><span>智联招聘</span>
                        </label>    
                        <label for="" class="input_channel">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="" class="input_icon_pos"><span>58同城</span>
                        </label>
                        <label for="" class="input_channel">
                            <input type="checkbox" disabled="true" name="marriage" id="m1" value="" class="input_icon_pos"><span>boss直聘</span>
                        </label>
                        <label for="" class="input_channel">
                            <input type="checkbox" disabled="true" name="marriage" id="m2" value="" class="input_icon_pos"><span>赶集网</span>
                        </label>    
                        <label for="" style="padding:6px 0;margin-left: 15px;margin-bottom: 0;width: 315px;">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="" class="input_icon_pos">
                            <span>内部推荐（推荐人<span style="border-bottom:1px solid #000000;width: 120px;display: inline-block;margin-bottom: -2px;" ></span>）</span>
                        </label>
                         <label for="" class="input_channel">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="" class="input_icon_pos">
                            <span>其他<span style="border-bottom:1px solid #000000;width: 100px;display: inline-block;margin-bottom: -2px;" ></span></span>
                        </label>
                        </form>
                    </td>
                </tr>
                <tr>
                    <table border="2">
                 <tr  style="background: #cccccc;border:2px solid #000000" class="fw600">
                    <td rowspan="6" style="padding: 0;width: 40px;letter-spacing: 3px;border:2px solid #000000">
                    <span style="writing-mode: tb-rl;width: 20px;height: 120px">家庭情况</span>
                    </td>
                </tr>
                <tr class="fw600">
                    <td colspan="2">家庭成员</td>
                    <td colspan="1">关系</td>
                    <td colspan="2">工作单位</td>
                    <td colspan="1">职业</td>
                    <td colspan="2">联系方式</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                </tr>
                <tr style="border-bottom:2px solid #000000">
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                    <td colspan="1"></td>
                    <td colspan="2"></td>
                </tr>   
                <tr style="background: #cccccc;border:2px solid #000000" class="fw600">
                    <td rowspan="8" style="padding: 0;width: 40px;letter-spacing: 3px;border:2px solid #000000">
                    <span style="writing-mode: tb-rl;width: 20px;height: 150px">教育及培训状况</span>
                    </td>
                </tr>
                <tr class="fw600">
                    <td colspan="2">起止年月</td>
                    <td colspan="2">学校名称</td>
                    <td colspan="2">专业名称</td>
                    <td>学历</td>
                    <td>所获资格证书</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr class="fw600">
                    <td colspan="2">起止年月</td>
                    <td colspan="2">培训机构/老师</td>
                    <td colspan="3">课程名称</td>
                    <td>所获资格证书</td>
                </tr>
                 <tr>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td colspan="3"></td>
                    <td></td>
                </tr>
                 <tr style="border-bottom:2px solid #000000">
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td colspan="3"></td>
                    <td></td>
                </tr>
                <tr style="background: #cccccc;border:2px solid #000000" class="fw600">
                    <td rowspan="5" style="padding: 0;width: 40px;letter-spacing: 3px;border:2px solid #000000">
                    <span style="writing-mode: tb-rl;width: 20px;height: 100px">工作经历</span>
                    </td>
                </tr>
                <tr class="fw600">
                    <td colspan="1" style="width: 150px">日期</td>
                    <td>工作单位</td>
                    <td>职位</td>
                    <td>薪资</td>
                    <td colspan="2">离职原因</td>
                    <td colspan="2">证明人及电话</td>
                </tr>
                <tr>
                    <td colspan="1" style="width: 150px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                     <td colspan="2"></td>
                    <td colspan="2"></td>

                </tr>
                <tr>
                    <td colspan="1" style="width: 150px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                </tr>
                <tr style="border-bottom:2px solid #000000">
                    <td colspan="1" style="width: 150px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                     <td colspan="2"></td>
                    <td colspan="2"></td>
                </tr>
                <tr style="border-bottom:2px solid #000000">
                    <td colspan="2" class="fw600">期望月工资（税前）</td>
                    <td colspan="2"></td>
                    <td colspan="2" class="fw600">预计到岗日期</td>
                    <td colspan="3"></td>
                </tr>
                 <tr style="background: #cccccc;border:2px solid #000000" class="fw600">
                    <td rowspan="4" style="padding: 0;width: 40px;letter-spacing: 3px;border:2px solid #000000">
                    <span style="writing-mode: tb-rl;width: 20px;height: 80px">专业技能</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        
                           <form action="" style="text-align: left;padding-left: 10px;">
                            语言：
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m1" value="" class="input_icon_pos">
                            <span>日语</span>
                        </label>
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m2" value="" class="input_icon_pos">
                            <span>英语</span>
                        </label>    
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="" class="input_icon_pos">
                            <span>其他<span style="border-bottom:1px solid #000000;width: 100px;display: inline-block;margin-bottom: -2px;" ></span></span>
                        </label>
                        </form>
                    </td>
                    <td colspan="4">
                        
                           <form action="" style="text-align: left;padding-left: 10px;">
                         是否持有驾照：
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m1" value="" class="input_icon_pos">
                            <span>是，<span style="border-bottom:1px solid #000000;width: 100px;display: inline-block;margin-bottom: -2px;" ></span>照</span>
                        </label>
                        
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="" class="input_icon_pos">
                            <span>否</span>
                        </label>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" style="text-align: left;padding-left: 10px;">
                        技能证书：计算机<span style="border-bottom:1px solid #000000;width: 100px;display: inline-block;margin-bottom: -2px;" ></span>
                        财会<span style="border-bottom:1px solid #000000;width: 100px;display: inline-block;margin-bottom: -2px;" ></span>
                        其他<span style="border-bottom:1px solid #000000;width: 100px;display: inline-block;margin-bottom: -2px;" ></span>
                    </td>
                </tr>
                <tr style="border-bottom:2px solid #000000">
                    <td colspan="8" style="text-align: left;padding-left: 10px;">
                        业余爱好/其他特长：<span></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="9">
                        <p>承诺书</p>
                    <p style="text-align: left;padding-left: 10px;">1、我保证所填写的每一项内容正式，如有虚假，即使被贵公司录用，贵公司也可随时无条件解聘我；</p>
                    <p style="text-align: left;padding-left: 10px;">2、我愿意接受贵公司的背景调查、培训、试用，如达不到贵公司要求，不予录用我为公司正式员工；</p>
                    <p style="text-align: left;padding-left: 10px;">3、我保证到贵公司报道前，已与原工作单位解除劳动合同等关系，并不存在任何劳资纠纷。</p>
                    <p style="text-align: right;padding-right: 10px;">申请人签名：
                        <span style="width: 200px;margin-left: 150px;">年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日</span>
                    </p>
                    </td>
                </tr>
                <tr style="border: 2px solid #000000">
                    <td colspan="9" style="letter-spacing: 2em;background: #cccccc;color: #000000;height: 60px">
                        以下为公司填写
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" style="padding: 0;width: 40px;letter-spacing: 3px;border:2px solid #000000">
                    <span style="writing-mode: tb-rl;width: 20px;height: 80px">面谈结要</span>
                    </td>
                </tr>
                <tr style="border-bottom: 1px dotted #000000;">
                    <td colspan="4" >
                        
                        <form action="" style="text-align: left;padding-left: 10px;">
                            人事部意见：
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m1" value="" class="input_icon_pos">
                            <span>同意录用</span>
                        </label>
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m2" value="" class="input_icon_pos">
                            <span>存档储备</span>
                        </label>    
                        <label for="" style="padding:6px 0;margin-right: 15px;margin-bottom: 0;">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="" class="input_icon_pos">
                            <span>不予录用</span>
                        </label>
                        </form>
                        <p style="text-align: left;padding-left: 10px;">面试评价：<span></span><br /><span></span></p>
                        <p style="text-align: left;padding-left: 10px;">面试官签名：
                            <span style="width: 200px;margin-left: 150px;">年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日</span>
                        </p>
                    </td>
                    <td colspan="4">
                        
                        <form action="" style="text-align: left;padding-left: 10px;">
                            用人部门意见：
                        <label for="" class="input_label">
                            <input type="checkbox" disabled="true" name="marriage" id="m1" value="" class="input_icon_pos">
                            <span>同意录用</span>
                        </label>
                        <label for="" class="input_label">
                            <input type="checkbox" disabled="true" name="marriage" id="m2" value="" class="input_icon_pos">
                            <span>存档储备</span>
                        </label>    
                        <label for="" class="input_label">
                            <input type="checkbox" disabled="true" name="marriage" id="m3" value="" class="input_icon_pos">
                            <span>不予录用</span>
                        </label>
                        </form>
                        <p style="text-align:left;padding-left: 10px;">面试评价：<span></span><br /><span></span></p>
                        <p style="text-align: left;padding-left: 10px;">面试官签名：
                            <span style="width: 200px;margin-left: 150px;">年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>协商到岗日期</td>
                    <td></td>
                    <td>部门</td>
                    <td></td>
                    <td>职位</td>
                    <td></td>
                    <td>试用期薪资</td>
                    <td></td>
                </tr>
                    </table>
                </tr>
                
                
            </table>
            </div>
    <!-- 全局js -->
    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <!-- 自定义js -->
    <script src="/Public/Theme1/js/content.js?v=1.0.0"></script>
    <script>
        $(document).ready(function () {

            // Local script for demo purpose only
            $('#lightVersion').click(function (event) {
                event.preventDefault()
                $('#ibox-content').removeClass('ibox-content');
                $('#vertical-timeline').removeClass('dark-timeline');
                $('#vertical-timeline').addClass('light-timeline');
            });

            $('#darkVersion').click(function (event) {
                event.preventDefault()
                $('#ibox-content').addClass('ibox-content');
                $('#vertical-timeline').removeClass('light-timeline');
                $('#vertical-timeline').addClass('dark-timeline');
            });

            $('#leftVersion').click(function (event) {
                event.preventDefault()
                $('#vertical-timeline').toggleClass('center-orientation');
            });


        });
    </script>
    <?php if($printId == 2016): ?><script type="text/javascript">
        window.print();
    </script><?php endif; ?>
</body>

</html>