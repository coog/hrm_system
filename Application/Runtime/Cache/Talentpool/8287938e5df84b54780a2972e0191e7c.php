<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html style="overflow: hidden;">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>相关附件</title>
<link rel="shortcut icon" href="favicon.ico">
<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6"
	rel="stylesheet">
<link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0"
	rel="stylesheet">
<link href="/Public/Theme1/css/plugins/iCheck/custom.css"
	rel="stylesheet">
<link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
<link href="/Public/Theme1/css/style.min.css?v=4.1.0"
	rel="stylesheet">
<link href="/Public/Theme1/static/plupload/upfiless.css"
	rel="stylesheet" type="text/css" />

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>
							<a href="/Index/addatecv" style="color:#c8c9c9">基本资料 </a>
							<a href="/Index/addatecvtwo" style="color:#c8c9c9; margin-left:25px;">详细资料</a>
							<a href="/Index/addatecvthree" style="color:#3390cd; margin-left:25px;">相关附件</a>
						</h5>

					</div>

					<div class="ibox-content"">
						<form method="post" action="/Index/index"
							class="form-horizontal" enctype="multipart/form-data">
							<div class="form-group">
						<label class="col-sm-1 control-label" style="margin-top: 20px;">个人形象照片</label>
						<div class="col-sm-7">
							<dl id="ul_pics" class="ul_pics clearfix">
								<?php if($rs_hrreserve["hrPhoto"] != null): ?><img src="/<?php echo ($rs_hrreserve["hrPhoto"]); ?>" width="100" height="100" 		style="margin-bottom: 5px;"><?php endif; ?>
							</dl>
							<a class="upimgs col-sm-2" id="btn"
								style="background-color: #067fcb; width: 100px; margin-top: 2px;">浏览照片</a>
							<input class="form-control upload-url" name="hrPhoto"
								value="<?php echo ($rs_hrreserve["hrPhoto"]); ?>" placeholder="如果已经存在图片路径，请您清空后在上传" id="pPic" style="width: 88%; margin-left: 5px; margin-right: 5px;">
						</div>

					</div>

					<div class="form-group">
						<label class="col-sm-1 control-label" style="margin-top: 20px;">入职表</label>
						<div class="col-sm-7">
							<dl id="ul_btnthumbnail" class="ul_pics clearfix">

								<?php if($rs_hrreserve["hrEnclosure"] != null): ?><img
									src="/<?php echo ($rs_hrreserve["hrEnclosure"]); ?>" width="100"
									height="100" style="margin-bottom: 5px; margin-right: 5px;"><?php endif; ?>

							</dl>
							<a class="upimgs col-sm-2" id="btnthumbnail"
								style="background-color: #0C9; width: 100px;">浏览图片</a> 
								<input class="upload-url form-control" name="hrEnclosure"
								value="<?php echo ($rs_hrreserve["hrEnclosure"]); ?>" placeholder="如果已经存在图片路径，请您清空后在上传" id="stEnclosure"
								style="width: 88%; margin-left: 5px; margin-right: 5px;">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-1 control-label" style="margin-top: 20px;">身份证附件</label>
						<div class="col-sm-7">
							<dl id="ul_stShenfenzhengImages" class="ul_pics clearfix">

								<?php if($rs_hrreserve["hrShenfenzhengImages"] != null): ?><img src="/<?php echo ($rs_hrreserve["hrShenfenzhengImages"]); ?>"
									width="100" height="100"
									style="margin-bottom: 5px; margin-right: 5px;"><?php endif; ?>

							</dl>
							<a class="upimgs col-sm-2" id="btnimg"
								style="background-color: #cf4ef4; width: 100px;">浏览图片</a> 
								<input class="upload-url form-control" name="hrShenfenzhengImages"
								value="<?php echo ($rs_hrreserve["hrShenfenzhengImages"]); ?>" placeholder="如果已经存在图片路径，请您清空后在上传"
								id="stShenfenzhengImages"
								style="width: 88%; margin-left: 5px; margin-right: 5px;">
						</div>
					</div>

						
							
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									
									<button class="btn btn-primary" type="submit" style="margin-left:15px;">保 存</button>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
	<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
	<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
	<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>
	<script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>

    	<script type="text/javascript"
		src="/Public/Theme1/static/plupload/plupload.full.min.js"></script>
	<script type="text/javascript">
            var uploader = new plupload.Uploader(
                  {
                    runtimes: 'html5,flash,silverlight,html4', //上传插件初始化选用那种方式的优先级顺序
                    browse_button: 'btn', // 上传按钮
                    url: "/Index/up", //远程上传地址
                    flash_swf_url: '/Public/plupload/Moxie.swf', //flash文件地址
                    silverlight_xap_url: '/Public/plupload/Moxie.xap', //silverlight文件地址
                    filters: {
                        max_file_size: '2mb', //最大上传文件大小（格式100b, 10kb, 10mb, 1gb）
                        mime_types: [//允许文件上传类型
                            {title: "files", extensions: "jpg,png,gif"}
                        ]
                 },
                multi_selection: true, //true:ctrl多文件上传, false 单文件上传
                init: {
                    FilesAdded: function(up, files) { //文件上传前
                        if ($("#ul_pics").children("li").length >7) {
                            alert("您上传的图片太多了！");
                            uploader.destroy();
                        } else {
                            var dd = '';
                            plupload.each(files, function(file) { //遍历文件
                                dd += "<dd id='" + file['id'] + "'><div class='progress'><span class='bar'></span><span class='percent'>0%</span></div></dd>";
                            });
                            $("#ul_pics").append(dd);
                            uploader.start();
                        }
                    },
                    UploadProgress: function(up, file) { //上传中，显示进度条
                 var percent = file.percent;
                        $("#ul_pics" + file.id).find('.bar').css({"width": percent + "%"});
                        $("#ul_pics" + file.id).find(".percent").text(percent + "%");
                    },
                    FileUploaded: function(up, file, info) { //文件上传成功的时候触发
                       var data = eval("(" + info.response + ")");
                        $("#" + file.id).html("<img src='/" + data.pic + "'/>");
                        var old=$("#pPic").val();
                         $("#pPic").val(old + data.pic+' ');
                    },
                    Error: function(up, err) { //上传出错的时候触发
                        alert(err.message);
                    }
                }
            });
            uploader.init();
        </script>

	<script type="text/javascript">
            var icuploader = new plupload.Uploader(
                  {
                    runtimes: 'html5,flash,silverlight,html4', //上传插件初始化选用那种方式的优先级顺序
                    browse_button: 'btnthumbnail', // 上传按钮
                    url: "/Index/upfujian", //远程上传地址
                    flash_swf_url: '/Public/plupload/Moxie.swf', //flash文件地址
                    silverlight_xap_url: '/Public/plupload/Moxie.xap', //silverlight文件地址
                    filters: {
                        max_file_size: '2mb', //最大上传文件大小（格式100b, 10kb, 10mb, 1gb）
                        mime_types: [//允许文件上传类型

                            {title: "files", extensions: "jpg,png,gif"}
                        ]
                 },
                multi_selection: true, //true:ctrl多文件上传, false 单文件上传
                init: {
                    FilesAdded: function(up, files) { //文件上传前
                        if ($("#ul_btnthumbnail").children("li").length >7) {
                            alert("您上传的图片太多了！");
                            icuploader.destroy();
                        } else {
                            var dd = '';
                            plupload.each(files, function(file) { //遍历文件
                                dd += "<dd id='" + file['id'] + "'><div class='progress'><span class='bar'></span><span class='percent'>0%</span></div></dd>";
                            });
                            $("#ul_btnthumbnail").append(dd);
                            icuploader.start();
                        }
                    },
                    UploadProgress: function(up, file) { //上传中，显示进度条
                 var percent = file.percent;
                        $("#" + file.id).find('.bar').css({"width": percent + "%"});
                        $("#" + file.id).find(".percent").text(percent + "%");
                    },
                    FileUploaded: function(up, file, info) { //文件上传成功的时候触发
                       var data = eval("(" + info.response + ")");
                        $("#" + file.id).html("<img src='/" + data.pic + "'/>");
                        var old=$("#stEnclosure").val();
                         $("#stEnclosure").val(old + data.pic+' ');
                    },
                    Error: function(up, err) { //上传出错的时候触发
                        alert(err.message);
                    }
                }
            });
            icuploader.init();
        </script>


	<script type="text/javascript">
            var pimguploader = new plupload.Uploader(

                  {
                    runtimes: 'html5,flash,silverlight,html4', //上传插件初始化选用那种方式的优先级顺序
                    browse_button: 'btnimg', // 上传按钮
                    url: "/Index/upsfz", //远程上传地址
                    flash_swf_url: '/Public/plupload/Moxie.swf', //flash文件地址
                    silverlight_xap_url: '/Public/plupload/Moxie.xap', //silverlight文件地址
                    filters: {
                        max_file_size: '2mb', //最大上传文件大小（格式100b, 10kb, 10mb, 1gb）
                        mime_types: [//允许文件上传类型
                            {title: "files", extensions: "jpg,png,gif"}
                        ]
                 },
                multi_selection: false, //true:ctrl多文件上传, false 单文件上传
                init: {
                    FilesAdded: function(up, files) { //文件上传前
                        if ($("#ul_stShenfenzhengImages").children("li").length >7) {
                            alert("您上传的图片太多了！");
                            pimguploader.destroy();
                        } else {
                            var dd = '';
                            plupload.each(files, function(file) { //遍历文件
                                dd += "<dd id='" + file['id'] + "'><div class='progress'><span class='bar'></span><span class='percent'>0%</span></div></dd>";
                            });
                            $("#ul_stShenfenzhengImages").append(dd);
                            pimguploader.start();
                        }
                    },
                    UploadProgress: function(up, file) { //上传中，显示进度条
                 var percent = file.percent;
                        $("#" + file.id).find('.bar').css({"width": percent + "%"});
                        $("#" + file.id).find(".percent").text(percent + "%");
                    },
                    FileUploaded: function(up, file, info) { //文件上传成功的时候触发
                       var data = eval("(" + info.response + ")");
                        $("#" + file.id).html("<img src='/" + data.pic + "'/>");
                        var old=$("#stShenfenzhengImages").val();
                         $("#stShenfenzhengImages").val(old + data.pic+' ');
                    },
                    Error: function(up, err) { //上传出错的时候触发
                        alert(err.message);
                    }
                }
            });
            pimguploader.init();
        </script>

</body>

</html>