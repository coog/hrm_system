<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>创建账户信息</title>
	<link href="/Public/Theme1/HrStyle/MemberLogin/css/style.css"
		rel='stylesheet' type='text/css' />
</head>
<body>

	<div class="main">
		<div class="login-form">
			<div class="head" style="margin-top: -40px;">
				<img src="/Public/Theme1/HrStyle/MemberLogin/images/user.png"
					alt="" />
			</div>
			<form action="/Login/RegAction" method="POST">

				<input type="text" class="text"
					style="background: #ffffff; color: #a0a4a3;" name="hrName"
					placeholder="真实姓名：" required /> <select
					style="width: 98%; height: 44px; border: 1px solid #dbdbdb; border-radius: 6px; margin-bottom: 20px; padding-left: 10px; font-size: 16px; font-weight: bold; color: #a0a4a3"
					name="hrSex" required>
					<option value="">请选择性别</option>
					<option value="1">男</option>
					<option value="2">女</option>
				</select> <select
					style="width: 98%; height: 44px; border: 1px solid #dbdbdb; border-radius: 6px; margin-bottom: 20px; padding-left: 10px; font-size: 16px; font-weight: bold; color: #a0a4a3"
					name="hrDegrees" required>
					<option value=""><span> 请选择学历</span></option>
					<?php if(is_array($xueli)): foreach($xueli as $xuelik=>$valxueli): ?><option value="<?php echo ($xuelik); ?>"><?php echo ($valxueli); ?></option><?php endforeach; endif; ?>
				</select> <input type="text" class="text"
					style="background: #ffffff; color: #a0a4a3;" name="hrHeight"
					placeholder="身高 CM：" required /> <input type="text" class="text"
					style="background: #ffffff; color: #a0a4a3;" name="hrTel"
					placeholder="手机号码：" required /> <input type="password"
					name="hrPwd" style="background: #ffffff; color: #a0a4a3;"
					placeholder="登陆密码：" required /> <input type="password"
					name="hrPwds" style="background: #ffffff; color: #a0a4a3;"
					placeholder="确认密码：" required />

				<div class="submit">
					<input type="submit" onClick="myFunction()" value="信息登记">
				</div>
				<p>
					<a href="/Login/"> 已有账户，点击登陆</a>
				</p>

			</form>
		</div>
		<div class="copy-right">
			<p></p>
		</div>
	</div>

</body>
</html>